<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package aisai
 */
@session_start();
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon-16x16.png" sizes="16x16" />
<script type='text/javascript'>var aiAjax = "<?php echo admin_url('admin-ajax.php');?>"</script>
<?php wp_head(); ?>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '945342705495492');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=945342705495492&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code  -->


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-74375581-1', 'auto');
  ga('send', 'pageview');
</script>


</head>

<body class="<?php if (aisai::user_logged_in() != false) { echo ' logged-in '; }?>"<?php body_class(); ?>  ng-app='aisaiApp'>
<div id="page" class="hfeed site">

<?php if (!aisai::user_logged_in()) { ?>

	<header id="masthead" class="site-header container" role="banner">
    <div class="middle-large-section clearfix">
  		<div class="site-branding">
        <a href="/" class="logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt=""></a>
  		</div><!-- .site-branding -->

  		<nav id="site-navigation" class="main-navigation desktop-navigation" role="navigation">
        <ul class="main-nav">
          <li><a href="/pricing">Pricing</a></li>
          <li><a href="/get-started">Sign In</a></li>
          <li><a class='bordered' href="/get-started">Sign Up</a></li>
        </ul>
  		</nav><!-- #site-navigation -->

      <nav id="site-navigation" class="main-navigation mobile-navigation" role="navigation">

        <a class="nav-item nav-dropdown"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="item-label">menu</span><i class="fa fa-bars"></i></a>
          <ul class="dropdown-menu">
            <li><a href="/pricing">Pricing</a></li>
            <li><a href="/get-started">Sign In</a></li>
            <li><a href="/get-started">Sign Up</a></li>
          </ul>

      </nav><!-- #site-navigation -->

    </div>
	</header><!-- #masthead -->

<?php } else { ?>


  <header id="masthead" class="site-header logged-in container" role="banner">
    <div class="middle-large-section clearfix">
      <div class="site-branding">
        <a href="/" class="logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt=""></a>
      </div><!-- .site-branding -->

      <nav id="site-navigation" class="main-navigation main-menu" role="navigation">

          <a href="<?php echo get_home_url();?>/inbox?show=inbox" class="nav-item messages"><span class="item-label">inbox</span><i class="fa fa-envelope-o"></i>
            <?php $new = aisai::get_number_of_new_messages();
            if($new > 0) { ?>
              <span class="badge header-badge"><?php echo $new; ?></span>
            <?php } else { ?>
              <span class="badge header-badge grey">0</span>
            <?php } ?>
          </a>


        <?php
          $accounts = get_accounts_i_m_in();
          if($accounts !== false) {

            if(isset($_SESSION['nsauth']['account']['id'])) {
              $current_account_id = $_SESSION['nsauth']['account']['id'];
            }


            $current_account_title = $_SESSION['nsauth']['account']['account_title'];

          ?>

        <span class="dropdown-wrap account-dropdown-wrap">
          <a class="nav-item nav-dropdown"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="item-label"><?php echo $current_account_title; ?></span><i class="fa fa-caret-down"></i></a>
          <ul class="dropdown-account dropdown-menu">
            <li class="<?php if($current_account_id == 0) {echo 'active';}?>"><a href="<?php echo get_home_url(); ?>/switch-account/?switch_to=0"><i class="fa fa-male"></i>My Account</a></li>
           <?php foreach($accounts as $acct) { ?>
            <li class='<?php if($current_account_id == $acct->id) {echo 'active';}?>'><a href="<?php echo get_home_url(); ?>/switch-account/?switch_to=<?php echo $acct->id; ?>"><i class="fa fa-share-alt"></i><?php echo $acct->account_title; ?></a></li>
           <?php } ?>
          </ul>
        </span>
        <?php } ?>


        <span class="dropdown-wrap menu-dropdown">
          <a class="nav-item nav-dropdown"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="item-label">menu</span><i class="fa fa-bars"></i></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo get_home_url(); ?>/dashboard"><i class="fa fa-briefcase"></i>Dashboard</a></li>
              <li><a href="<?php echo get_home_url(); ?>/inbox?show=inbox"><i class="fa fa-envelope"></i>Inbox</a></li>
              <?php if(can_use_talent_pool()) { ?>
              <li><a href="<?php echo get_home_url(); ?>/talent-pool"><i class="fa fa-users"></i>Talent Pool</a></li>
              <?php } ?>

              <li><a href="<?php echo get_home_url(); ?>/settings"><i class="fa fa-cog"></i>Settings</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="<?php echo do_shortcode('[nsauth-logout-url]'); ?>"><i class="fa fa-lock"></i>Logout</a></li>
            </ul>
          </span>

      </nav><!-- #site-navigation -->

    </div>
  </header><!-- #masthead -->

<?php } ?>

	<div id="content" class="site-content">

