<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package aisai
 */

?>

	</div><!-- #content -->





<?php if (!aisai::user_logged_in()) { ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
      <div class="button-wrapper">
        <div class="middle-large-section clearfix">
          <a href="/get-started" class="btn btn-success">SIGN UP FOR AISAI</a>
        </div>
      </div>
      <div class="nav-wrapper">
        <div class="middle-large-section clearfix">
          <div class="fade-logo">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/aisai-symbol.png" alt="AISAI LOGO">
          </div>
          <?php dynamic_sidebar( 'footer-loggedout' ); ?>
        </div>
      </div>
      <div class="copyright-wrapper">
        <div class="middle-large-section clearfix">
         Copyright <?php echo date("Y"); ?> AISAI. All rights reserved.
        </div>
      </div>
	</footer><!-- #colophon -->


<?php } else { ?>

  <footer id="colophon" class="site-footer logged-in" role="contentinfo">
      <div class="nav-wrapper">
        <div class="middle-large-section clearfix">
          <?php dynamic_sidebar( 'footer-loggedin' ); ?>
        </div>
      </div>
      <div class="copyright-wrapper">
        <div class="middle-large-section clearfix">
         Copyright <?php echo date("Y"); ?> AISAI. All rights reserved.
        </div>
      </div>
  </footer><!-- #colophon -->






<?php

// read added_to_account_notification meta and if it exists show popup.

global $api;

$current_user_id = aisai::user_logged_in();

$result = $api->sendRequest(array(
  'action'       => 'read',
  'controller'   => 'meta',
  'data_type'    => 'user', // can be job, profile or user
  'data_id'      => $current_user_id, // read by profile id -> output all meta as an array
  'meta_key'     => 'added_to_account_notification', // read by meta id
));

$accounts_i_m_in = get_accounts_i_m_in();

if(count($accounts_i_m_in) > 0) {
  $account_ids = array();
  foreach((array) $accounts_i_m_in as $acct) {
    array_push($account_ids, $acct->id);
  }

  if (($result > 0) && in_array($result, $account_ids)) { ?>

    <!-- Modal Begin -->
      <div class="modal fade added-to-account-notification"  tabindex="-1" role="dialog" aria-labelledby="added-to-account-notification">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="added-to-account-notification">New Shared Account</h4>
            </div>
            <div class="modal-body">

              <div class="new-shared-account">
                Hi. New account has been shared with you. You can switch between your account and shared account from the account menu in the header or by pressing the button below.
              </div>
              <div class="account-switch-btn">
                <button  class="btn btn-success switch-to-account">Switch To Shared Account</button>
              </div>

            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- Modal END -->

      <script>
        jQuery( document ).ready(function() {
             jQuery('.added-to-account-notification').modal('show');

             jQuery('.added-to-account-notification').on('hidden.bs.modal', function () {
                var data = {};
                jQuery.ajax({
                  data: ({
                  action : 'ai_remove_added_to_account_notification',
                  params: data,
                  }),
                  type: 'POST',
                  async: true,
                  url: aiAjax,
                  })
              });

             jQuery('.switch-to-account').click(function() {
                var data = {};
                jQuery.ajax({
                  data: ({
                  action : 'ai_remove_added_to_account_notification',
                  params: data,
                  }),
                  type: 'POST',
                  async: true,
                  url: aiAjax,
                  });

                location.href = '<?php site_url(); ?>/switch-account/?switch_to=<?php echo $result; ?>';

             });


        });
      </script>

  <?php }
}
?>


<?php
// pricing modal

if(!can_indicate_capabilities()) { ?>

    <!-- Modal Begin -->
      <div class="modal fade pricing-large-modal"  tabindex="-1" role="dialog" aria-labelledby="pricing-large-modal">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="pricing-large-modal">Membership Plans</h4>
            </div>
            <div class="modal-body clearfix">

              <div class="full-pricing-table-wrapper">
                <div class="full-table">

                  <div class="line greyline">
                    <div class="col-25 titlerow">Plan</div>
                    <div class="col-25 smart">Smart</div>
                    <div class="col-25 genius">Genius</div>
                    <div class="col-25 genius-plus">Genius +</div>
                  </div>

                  <div class="line">
                    <div class="col-25 titlerow">Active Talent Searches</div>
                    <div class="col-25 smart">1</div>
                    <div class="col-25 genius">1</div>
                    <div class="col-25 genius-plus">10</div>
                  </div>

                  <div class="line greyline">
                    <div class="col-25 titlerow">Interest matching</div>
                    <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                    <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                    <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
                  </div>

                  <div class="line">
                    <div class="col-25 titlerow">Role experience matching</div>
                    <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                    <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                    <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
                  </div>

                  <div class="line greyline">
                    <div class="col-25 titlerow">Qualifications matching</div>
                    <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                    <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                    <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
                  </div>

                  <div class="line">
                    <div class="col-25 titlerow">Industry matching</div>
                    <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                    <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                    <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
                  </div>

                  <div class="line greyline">
                    <div class="col-25 titlerow">Technical skills matching</div>
                    <div class="col-25 smart"><i class="fa fa-times"></i></div>
                    <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                    <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
                  </div>

                  <div class="line">
                    <div class="col-25 titlerow">Talent pool</div>
                    <div class="col-25 smart"><i class="fa fa-times"></i></div>
                    <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                    <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
                  </div>

                  <div class="line greyline">
                    <div class="col-25 titlerow">Multi user account</div>
                    <div class="col-25 smart"><i class="fa fa-times"></i></div>
                    <div class="col-25 genius"><i class="fa fa-times"></i></div>
                    <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
                  </div>

                </div>
              </div>


            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- Modal END -->


<script>

jQuery('.show-pricing-modal').click(function() {

  jQuery('.pricing-large-modal').modal('show');

});

jQuery('.show-pricing-modal a').click(function(e) {

  e.stopPropagation();

});



</script>



<?php } ?>


<?php
// pricing modal

if(is_page('create-search')) { ?>

    <!-- Modal Begin -->
      <div class="modal fade job-was-created-modal"  tabindex="-1" role="dialog" aria-labelledby="job-was-created-modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="job-was-created-modal">Job was created</h4>
            </div>
            <div class="modal-body clearfix">

              <div class="new-shared-account">
                Job was created successfully. It will go live after moderation process. You can view it on your dashboard.
              </div>
              <div class="account-switch-btn">
                <a href="/dashboard/" class="btn btn-success switch-to-account">Go to Dashboard</a>
              </div>

            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- Modal END -->


    <!-- Modal Begin -->
      <div class="modal fade job-was-not-created-modal"  tabindex="-1" role="dialog" aria-labelledby="job-was-not-created-modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="job-was-not-created-modal">Job was not created</h4>
            </div>
            <div class="modal-body clearfix">
              <div class="new-shared-account">
                Job was not created, because you are out of live job slots. Consider archiving one of your other jobs or upgrading your plan.
              </div>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- Modal END -->

<?php } ?>





<?php
// if user was downgraded and has more live jobs than he can, show modal.
if(has_extra_jobs_live()) {

  // get current user's jobs
  $user_id = aisai::user_logged_in();


  @session_start();

  $user_id = aisai::user_logged_in();

  global $api;

  $live_jobs = $api->sendRequest(array(

    'action'       => 'read',
    'controller'   => 'job',
    'id'           => '',
    'recruiter_id' => $user_id, // indicate 0 for showing all jobs and disregard the author
    'multiple'     => true, //****
    'query_string' => '', // search in title ******
    'limit'        => 100, // ***
    'offset'       => 0, // ****
    'sort_by'      => 'date', // date, title
    'order'        => 'desc',
    'status'       => 'live', //all, live, pending, archive
  ));

  $pending_jobs = $api->sendRequest(array(

    'action'       => 'read',
    'controller'   => 'job',
    'id'           => '',
    'recruiter_id' => $user_id, // indicate 0 for showing all jobs and disregard the author
    'multiple'     => true, //****
    'query_string' => '', // search in title ******
    'limit'        => 100, // ***
    'offset'       => 0, // ****
    'sort_by'      => 'date', // date, title
    'order'        => 'desc',
    'status'       => 'pending', //all, live, pending, archive
  ));

  $active_jobs_array = array_merge($live_jobs->jobs, $pending_jobs->jobs);

  $select_options = '';

  foreach($active_jobs_array as $active_job) {
    $select_options .= '<option value="'.$active_job->id.'">'.$active_job->job_title.'</option>';
  }


  ?>


    <!-- Modal Begin -->
      <div class="modal fade out-of-live-jobs-modal"  tabindex="-1" role="dialog" aria-labelledby="out-of-live-jobs-modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="out-of-live-jobs-modal">Deactivate Jobs</h4>
            </div>
            <div class="modal-body clearfix">

              <div class="modal-text-padding">
                You have more live jobs than it is allowed according to your current Membership plan.
                Please select a job you would like to keep active. Other jobs will be moved to archive.
                Closing this window without selecting one job will automatically archive all your active jobs.
              </div>

              <div class="form-group clearfix">
                <div class="col-sm-12 multiselect-wrapper">
                  <select class="multiselect-single" id="active-job-selection" style="visibility:hidden;" title="Job Title">
                    <option value="0">Nothing Selected</option>
                    <?php echo $select_options; ?>
                  </select>
                </div>
              </div>

              <div class="btn-wrapper">
                <button  class="btn btn-success keep-selected-job-active">Keep Selected Active</button>
              </div>

            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- Modal END -->


      <script>
        jQuery( document ).ready(function() {

          jQuery('.out-of-live-jobs-modal').modal('show');

        });

        jQuery('.keep-selected-job-active').click(function() {
          var data = {};
          data.keep_job = jQuery('#active-job-selection').val();
          console.log(data);
          jQuery.ajax({
            data: ({
            action : 'ai_deactivate_extra_jobs',
            params: data,
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            }).done(function( msg ) { console.log(msg); });
          jQuery('.out-of-live-jobs-modal').attr('data-jobs-deactivated', '1');
          jQuery('.out-of-live-jobs-modal').modal('hide');
          setTimeout(function(){ location.reload(); }, 1000);
       });


        jQuery('.out-of-live-jobs-modal').on('hidden.bs.modal', function () {
          if(jQuery('.out-of-live-jobs-modal').attr('data-jobs-deactivated') == 1) {
            // jobs already deactivated
          } else {
          var data = {};
          data.keep_job = 0;
          console.log(data);
          jQuery.ajax({
            data: ({
            action : 'ai_deactivate_extra_jobs',
            params: data,
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            }).done(function( msg ) { console.log(msg); });
          setTimeout(function(){ location.reload(); }, 1000);
          }

        });
      </script>

<?php } ?>
<?php } ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
