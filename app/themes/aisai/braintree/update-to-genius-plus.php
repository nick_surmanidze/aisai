<?php


function update_to_genius_plus() {

###############################################################################################
###############################################################################################

// this equalizes all previous subscriptions to one-off
cancel_subscription_remove_card();

// update last payment to one-off
update_last_active_payment_to_one_off();
###############################################################################################
###############################################################################################

$customer_email = $_SESSION['nsauth']['user_login'];
$genius_plus_plan_id = get_option("nspay_genius_plus_plan_id");
$genius_plus_plan_price = get_option("nspay_genius_plus_single_price");
$genius_plan_price = get_option("nspay_genius_single_price");

$form =  '
<form method="POST" action="">
  <div class="form-group clearfix">
    <div class="col-sm-6">
      <label for="">First Name</label>
      <input type="text" class="form-control" name="first_name" placeholder="Name">
    </div>
    <div class="col-sm-6">
      <label for="">Last Name</label>
      <input type="text" class="form-control" name="last_name" placeholder="Surname">
    </div>
  </div>

  <div class="form-group clearfix">
    <div class="col-sm-12">
      <label for="">Cardholder Name</label>
      <input type="text" class="form-control" name="full_name" placeholder="Name Surname">
    </div>
  </div>

  <div class="form-group clearfix">
    <div class="col-sm-12">
      <label for="">Card Number</label>
      <input type="text" class="form-control" name="card_number" placeholder="1111222233334444">
    </div>
  </div>

  <div class="form-group clearfix">
    <div class="col-sm-6">
      <label for="">Expiration Month</label>
      <input type="text" class="form-control" name="expiry_month" placeholder="MM">
    </div>
    <div class="col-sm-6">
      <label for="">Expiration Year</label>
      <input type="text" class="form-control" name="expiry_year" placeholder="YYYY">
    </div>
  </div>

  <div class="form-group clearfix">
    <div class="col-sm-12">
      <label for="">Card CVV</label>
      <input type="text" class="form-control" name="card_cvv" placeholder="123">
    </div>
  </div>

  <div class="button-wrapper clearfix">
    <div class="col-sm-12">
      <input type="hidden" class="form-control" name="user_email" value="'.$customer_email.'">
      <input type="submit" class="btn btn-success complete-payment-btn" value="Complete Payment"/>
    </div>
  </div>
  </form>
  ';



if($_SESSION['update_membership']['type_after'] == 'recurring') {





if($_SESSION['update_membership']['plan_before'] == 'genius') {


  // then we need to upgrade
  $upgrading = true;

  $payment_form = $form;

  # calculate upgrade fee

  $upgrade_price = get_upgrade_price($genius_plan_price, $genius_plus_plan_price);

  # next billing date

  // identify next payment date (DEFAULT NOW)
  $next_payment_date = strtotime('now');
  // this is needed for checking if we are making payment today and need to create payment record.
  $create_payment_on_core = true;

  // if this is true than we are formatting next plan and so on differently
  $payment_result = false;

  global $api;
  $payments = $api->sendRequest(array(
    'action'         => 'read',
    'controller'     => 'payment',
    'account_id'     => '',
    'user_id'        => aisai::user_logged_in()
  ));

  if(count($payments->payments) > 0) {
    $last_payment = $payments->payments[0];
    // check if last payment is active
    if(strtotime('now') < strtotime($last_payment->payment_date . '+1 month')) {
      // BUT IF WE HAVE LAST PAYMENT WHICH IS ACTIVE
      // THEN:
      $next_payment_date = strtotime($last_payment->payment_date . '+1 month');
      $create_payment_on_core = false;
    }
  }




  #create customer

  // check if we returned to this page from the same page and have $post vars
  if(isset($_POST['first_name']) &&
    isset($_POST['last_name']) &&
    isset($_POST['user_email']) &&
    isset($_POST['full_name']) &&
    isset($_POST['card_number']) &&
    isset($_POST['expiry_month']) &&
    isset($_POST['expiry_year']) &&
    isset($_POST['card_cvv']) ) {



    // if all of that is set then we need to create a customer
    $creditCardToken = create_customer();



    // if credit card token is valid
    if(strlen($creditCardToken) > 0) {

      try {

        // make current transaction to upgrade from genius
        $single_pay = Braintree_Transaction::sale([
          'amount' => $upgrade_price,
          'paymentMethodToken' => $creditCardToken,
          'options' => [
            'submitForSettlement' => True
          ]
        ]);
      }
      catch(Exception $e) {
        $payment_form = 'Error occured:' .$e->getMessage();
      }




      try {

        // make subscription from next date
        $date_object = date("m/d/Y h:i:s A T", $next_payment_date);
        $result = create_transaction($creditCardToken, $genius_plus_plan_id, $date_object);

      }
      catch(Exception $e) {
        $payment_form = 'Error occured:' .$e->getMessage();
      }



      if($result->success && $single_pay->success) {

        ####################################################
        #########            SUCCESS               #########

        // parsing response
        $subscription_object = $result->subscription;
        $subscription_status = $result->subscription->status;
        $subscription_first_billing_date = $result->subscription->firstBillingDate;
        $subscription_id = $result->subscription->id;

        if($create_payment_on_core == true) {
          //payment  successful
          $payment_form = "<div class='info-area'>Transaction was successful. Subscription # ".$subscription_id."</div>";
        } else {
          $payment_form = "<div class='info-area'>Transaction was successful. Now you are subscribed to Genius Plus plan. Your first payment will be made on ".$subscription_first_billing_date->format('Y-m-d').", once your current plan expires.</div>";
        }

        $payment_result = true;

        // update user's meta

        set_user_subscription(1, $genius_plus_plan_id, $creditCardToken, $subscription_id, 'genius_plus');



          // create payment input (if date is not later)
          $new_payments = $api->sendRequest(array(
            'action'         => 'update',
            'controller'     => 'payment',
            'id'             => $last_payment->id,
            'payment_plan'   => 'genius_plus',
            'type'           => 'subscription',
          ));



        // unset $_POST and $_SESSION['update_membership']

        unset($_POST['first_name']);
        unset($_POST['last_name']);
        unset($_POST['user_email']);
        unset($_POST['full_name']);
        unset($_POST['card_number']);
        unset($_POST['expiry_month']);
        unset($_POST['expiry_year']);
        unset($_POST['card_cvv']);

        unset($_SESSION['update_membership']['plan_before']);
        unset($_SESSION['update_membership']['plan_after']);
        unset($_SESSION['update_membership']['type_before']);
        unset($_SESSION['update_membership']['type_after']);
        unset($_SESSION['update_membership']['price']);


        #########            SUCCESS               #########
        ####################################################


      } else {


        ####################################################
        #########             FAILED               #########

        // payment was not successful and need to start over
        $payment_form = "<div class='info-area'>Payment failed. " . $result . "</div>" . $form;


        unset($_POST['first_name']);
        unset($_POST['last_name']);
        unset($_POST['user_email']);
        unset($_POST['full_name']);
        unset($_POST['card_number']);
        unset($_POST['expiry_month']);
        unset($_POST['expiry_year']);
        unset($_POST['card_cvv']);


        #########             FAILED               #########
        ####################################################

      }

    } else {

      $payment_form = "<div class='info-area'>Could not create a customer.</div>";
    }


} else {

  $payment_form = $form;

}



  # make a single payment

  # make subscription and indicate subscription first billing date

  # update last payment "payment_plan"












} else {

  // then regular plan upgrade or downgrade procedure


  // create recurring subscription to genius plus plan
  // we have cancelled the subscriptions so we equalized all previous plans to ###one-off###
  // identify next payment date (DEFAULT NOW)
  $next_payment_date = strtotime('now');
  // this is needed for checking if we are making payment today and need to create payment record.
  $create_payment_on_core = true;

  // if this is true than we are formatting next plan and so on differently
  $payment_result = false;

  global $api;
  $payments = $api->sendRequest(array(
    'action'         => 'read',
    'controller'     => 'payment',
    'account_id'     => '',
    'user_id'        => aisai::user_logged_in()
  ));

  if(count($payments->payments) > 0) {
    $last_payment = $payments->payments[0];
    // check if last payment is active
    if(strtotime('now') < strtotime($last_payment->payment_date . '+1 month')) {
      // BUT IF WE HAVE LAST PAYMENT WHICH IS ACTIVE
      // THEN:
      $next_payment_date = strtotime($last_payment->payment_date . '+1 month');
      $create_payment_on_core = false;
    }
  }


  // check if we returned to this page from the same page and have $post vars
  if(isset($_POST['first_name']) &&
    isset($_POST['last_name']) &&
    isset($_POST['user_email']) &&
    isset($_POST['full_name']) &&
    isset($_POST['card_number']) &&
    isset($_POST['expiry_month']) &&
    isset($_POST['expiry_year']) &&
    isset($_POST['card_cvv']) ) {



    // if all of that is set then we need to create a customer
    $creditCardToken = create_customer();



    // if credit card token is valid
    if(strlen($creditCardToken) > 0) {

      try {
        $date_object = date("m/d/Y h:i:s A T", $next_payment_date);
        $result = create_transaction($creditCardToken, $genius_plus_plan_id, $date_object);
      }
      catch(Exception $e) {
        $payment_form = 'Error occured:' .$e->getMessage();
      }

      if($result->success) {

        ####################################################
        #########            SUCCESS               #########

        // parsing response
        $subscription_object = $result->subscription;
        $subscription_status = $result->subscription->status;
        $subscription_first_billing_date = $result->subscription->firstBillingDate;
        $subscription_id = $result->subscription->id;

        if($create_payment_on_core == true) {
          //payment  successful
          $payment_form = "<div class='info-area'>Transaction was successful. Subscription # ".$subscription_id."</div>";
        } else {
          $payment_form = "<div class='info-area'>Transaction was successful. Now you are subscribed to Genius Plus plan. Your first payment will be made on ".$subscription_first_billing_date->format('Y-m-d').", once your current plan expires.</div>";
        }

        $payment_result = true;

        // update user's meta

        set_user_subscription(1, $genius_plus_plan_id, $creditCardToken, $subscription_id, 'genius_plus');


        if($create_payment_on_core == true) {
          // create payment input (if date is not later)
          $new_payments = $api->sendRequest(array(
            'action'         => 'create',
            'controller'     => 'payment',
            'account_id'     => 0,
            'user_id'        => aisai::user_logged_in(),
            'payment_plan'   => 'genius_plus',
            'payment_amount' => $result->subscription->price,
            'type'           => 'subscription',
          ));

        }

        // unset $_POST and $_SESSION['update_membership']

        unset($_POST['first_name']);
        unset($_POST['last_name']);
        unset($_POST['user_email']);
        unset($_POST['full_name']);
        unset($_POST['card_number']);
        unset($_POST['expiry_month']);
        unset($_POST['expiry_year']);
        unset($_POST['card_cvv']);

        unset($_SESSION['update_membership']['plan_before']);
        unset($_SESSION['update_membership']['plan_after']);
        unset($_SESSION['update_membership']['type_before']);
        unset($_SESSION['update_membership']['type_after']);
        unset($_SESSION['update_membership']['price']);


        #########            SUCCESS               #########
        ####################################################


      } else {


        ####################################################
        #########             FAILED               #########

        // payment was not successful and need to start over
        $payment_form = "<div class='info-area'>Payment failed. " . $result . "</div>" . $form;


        unset($_POST['first_name']);
        unset($_POST['last_name']);
        unset($_POST['user_email']);
        unset($_POST['full_name']);
        unset($_POST['card_number']);
        unset($_POST['expiry_month']);
        unset($_POST['expiry_year']);
        unset($_POST['card_cvv']);


        #########             FAILED               #########
        ####################################################

      }

    } else {

      $payment_form = "<div class='info-area'>Could not create a customer.</div>";
    }


} else {

  $payment_form = $form;

}






}










############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
















} elseif ($_SESSION['update_membership']['type_after'] == 'one-off') {





if($_SESSION['update_membership']['plan_before'] == 'genius') {

  // then we need to upgrade


  // then we need to upgrade
  $upgrading = true;

  $payment_form = $form;

  # calculate upgrade fee

  $upgrade_price = get_upgrade_price($genius_plan_price, $genius_plus_plan_price);

  # next billing date

  // identify next payment date (DEFAULT NOW)
  $next_payment_date = strtotime('now');
  // this is needed for checking if we are making payment today and need to create payment record.
  $create_payment_on_core = true;

  // if this is true than we are formatting next plan and so on differently
  $payment_result = false;

  global $api;
  $payments = $api->sendRequest(array(
    'action'         => 'read',
    'controller'     => 'payment',
    'account_id'     => '',
    'user_id'        => aisai::user_logged_in()
  ));

  if(count($payments->payments) > 0) {
    $last_payment = $payments->payments[0];
    // check if last payment is active
    if(strtotime('now') < strtotime($last_payment->payment_date . '+1 month')) {
      // BUT IF WE HAVE LAST PAYMENT WHICH IS ACTIVE
      // THEN:
      $next_payment_date = strtotime($last_payment->payment_date . '+1 month');
      $create_payment_on_core = false;
    }
  }




  #create customer

  // check if we returned to this page from the same page and have $post vars
  if(isset($_POST['first_name']) &&
    isset($_POST['last_name']) &&
    isset($_POST['user_email']) &&
    isset($_POST['full_name']) &&
    isset($_POST['card_number']) &&
    isset($_POST['expiry_month']) &&
    isset($_POST['expiry_year']) &&
    isset($_POST['card_cvv']) ) {



    // if all of that is set then we need to create a customer
    $creditCardToken = create_customer();



    // if credit card token is valid
    if(strlen($creditCardToken) > 0) {

      try {

        // make current transaction to upgrade from genius
        $single_pay = Braintree_Transaction::sale([
          'amount' => $upgrade_price,
          'paymentMethodToken' => $creditCardToken,
          'options' => [
            'submitForSettlement' => True
          ]
        ]);
      }
      catch(Exception $e) {
        $payment_form = 'Error occured:' .$e->getMessage();
      }



      if($single_pay->success) {

        ####################################################
        #########            SUCCESS               #########


        //payment  successful
        $payment_form = "<div class='info-area'>Transaction was successful. Your plan was upgraded to Genius Plus.</div>";

        $payment_result = true;

        // update user's meta

        set_user_subscription(0, 0, 0, 0, 0);



          // create payment input (if date is not later)
          $new_payments = $api->sendRequest(array(
            'action'         => 'update',
            'controller'     => 'payment',
            'id'             => $last_payment->id,
            'payment_plan'   => 'genius_plus',
            'type'           => 'subscription',
          ));



        // unset $_POST and $_SESSION['update_membership']

        unset($_POST['first_name']);
        unset($_POST['last_name']);
        unset($_POST['user_email']);
        unset($_POST['full_name']);
        unset($_POST['card_number']);
        unset($_POST['expiry_month']);
        unset($_POST['expiry_year']);
        unset($_POST['card_cvv']);

        unset($_SESSION['update_membership']['plan_before']);
        unset($_SESSION['update_membership']['plan_after']);
        unset($_SESSION['update_membership']['type_before']);
        unset($_SESSION['update_membership']['type_after']);
        unset($_SESSION['update_membership']['price']);


        #########            SUCCESS               #########
        ####################################################


      } else {


        ####################################################
        #########             FAILED               #########

        // payment was not successful and need to start over
        $payment_form = "<div class='info-area'>Payment failed. " . $result . "</div>" . $form;


        unset($_POST['first_name']);
        unset($_POST['last_name']);
        unset($_POST['user_email']);
        unset($_POST['full_name']);
        unset($_POST['card_number']);
        unset($_POST['expiry_month']);
        unset($_POST['expiry_year']);
        unset($_POST['card_cvv']);


        #########             FAILED               #########
        ####################################################

      }

    } else {

      $payment_form = "<div class='info-area'>Could not create a customer.</div>";
    }


} else {

  $payment_form = $form;

}



} else {

  // then regular plan upgrade or downgrade procedure







  // create one off
  if($_SESSION['update_membership']['plan_before'] == 'smart') {
  // if the previous is smart then need payment details and show single payment form


    $next_payment_date = strtotime('now');
    $create_payment_on_core = true;


      if(isset($_POST['first_name']) &&
        isset($_POST['last_name']) &&
        isset($_POST['user_email']) &&
        isset($_POST['full_name']) &&
        isset($_POST['card_number']) &&
        isset($_POST['expiry_month']) &&
        isset($_POST['expiry_year']) &&
        isset($_POST['card_cvv']) ) {


        // if all of that is set then we need to create a customer
        $creditCardToken = create_customer();

        // if credit card token is valid
        if(strlen($creditCardToken) > 0) {

          try {
            $date_object = date("m/d/Y h:i:s A T", $next_payment_date);

            $result = Braintree_Transaction::sale([
              'amount' => $genius_plus_plan_price,
              'paymentMethodToken' => $creditCardToken,
              'options' => [
                'submitForSettlement' => True
              ]
            ]);

          }
          catch(Exception $e) {
            $payment_form = 'Error occured:' .$e->getMessage();
          }

          if($result->success) {

            ####################################################
            #########            SUCCESS               #########

            //payment  successful
            $payment_form = "<div class='info-area'>Transaction was successful.</div>";

            // update user's meta

            set_user_subscription(0, 0, 0, 0, 0);

            if($create_payment_on_core == true) {
              // create payment input (if date is not later)
              global $api;
              $new_payments = $api->sendRequest(array(
                'action'         => 'create',
                'controller'     => 'payment',
                'account_id'     => 0,
                'user_id'        => aisai::user_logged_in(),
                'payment_plan'   => 'genius_plus',
                'payment_amount' => $genius_plus_plan_price,
                'type'           => 'one-off',
              ));

            }

            // unset $_POST and $_SESSION['update_membership']

            unset($_POST['first_name']);
            unset($_POST['last_name']);
            unset($_POST['user_email']);
            unset($_POST['full_name']);
            unset($_POST['card_number']);
            unset($_POST['expiry_month']);
            unset($_POST['expiry_year']);
            unset($_POST['card_cvv']);

            unset($_SESSION['update_membership']['plan_before']);
            unset($_SESSION['update_membership']['plan_after']);
            unset($_SESSION['update_membership']['type_before']);
            unset($_SESSION['update_membership']['type_after']);
            unset($_SESSION['update_membership']['price']);

            $to_genius_one_off = true;

            #########            SUCCESS               #########
            ####################################################


          } else {


            ####################################################
            #########             FAILED               #########

            // payment was not successful and need to start over
            $payment_form = "<div class='info-area'>Payment failed. " . $result . "</div>" . $form;


            unset($_POST['first_name']);
            unset($_POST['last_name']);
            unset($_POST['user_email']);
            unset($_POST['full_name']);
            unset($_POST['card_number']);
            unset($_POST['expiry_month']);
            unset($_POST['expiry_year']);
            unset($_POST['card_cvv']);


            #########             FAILED               #########
            ####################################################

          }

        } else {

          $payment_form = "<div class='info-area'>Could not create a customer.</div>";
        }


    } else {

      $payment_form = $form;

    }



  } else {
    // if the previous was genius or genius_plus we just write a message
    $payment_form = "<div class='info-area'>Your subscription has been cancelled. You can still use your current plan until the end of current billing cycle.</div>";

    unset($_SESSION['update_membership']['plan_before']);
    unset($_SESSION['update_membership']['plan_after']);
    unset($_SESSION['update_membership']['type_before']);
    unset($_SESSION['update_membership']['type_after']);
    unset($_SESSION['update_membership']['price']);
    $to_genius_one_off = true;
  }





}



} else {
  // do not know what to do.

}
?>

<div class="payment">
  <div class="content-wrapper">
    <div class="middle-large-section clearfix payment-main">
      <h2>Please Enter Your Payment Details</h2>
      <div class="col-50">

        <?php if( $payment_result == true) { ?>

        <h4><span class="title">Next Plan:</span>  <span class="plan genius-plus">Genius Plus (recurring)</span></h4>

        <?php } elseif($to_genius_one_off == true) { ?>

        <h4><span class="title">Next Plan:</span>  <span class="plan genius-plus">Genius Plus (one-off)</span></h4>

        <?php } else {?>

        <h4><span class="title">Current Plan:</span> <span class="plan <?php echo $_SESSION['update_membership']['plan_before']; ?>"><?php echo get_plan_label($_SESSION['update_membership']['plan_before']); ?></span></h4>
        <h4><span class="title">Next Plan:</span>  <span class="plan <?php echo $_SESSION['update_membership']['plan_after']; ?>"><?php echo get_plan_label($_SESSION['update_membership']['plan_after']); ?></span></h4>
        <?php if ($_SESSION['update_membership']['plan_after'] != 'smart') { ?>
        <h4><span class="title">Subscription:</span>  <span class="type"><?php echo $_SESSION['update_membership']['type_after']; ?></span></h4>
        <?php } ?>
        <h4><span class="title">Total:</span>  <span class="price">£ <?php echo $_SESSION['update_membership']['price']; ?></span></h4>


        <?php } ?>


      </div>
      <div class="col-50">
        <div class="image-container">
          <img class="ssl" alt="ssl" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ssl.png">
          <br>
          <img class="braintree" alt="ssl" src="<?php echo get_stylesheet_directory_uri(); ?>/images/braintree.png">
        </div>
      </div>
      <div class="plan-change-notice"><?php echo $procedure_label; ?></div>
    </div>

  </div>

  <div class="content-wrapper section-white">

    <div class="middle-large-section clearfix payment-details">
      <div class="columns-wrapper">
        <div class="col-50 plan-description">
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Plan Description</h3>
            </div>
            <div class="panel-body">
              Great, you're upgrading to Genius+. This gives you access to recruit continually for a month and run up to 10 active job positions at once and assign two additional users.
            </div>
          </div>
          <div class="image-container">
            <img class="credit-cards" alt="ssl" src="<?php echo get_stylesheet_directory_uri(); ?>/images/credit-cards.png">
          </div>
        </div>

        <div class="col-50 credit-card">

          <?php echo $payment_form; ?>

        </div>
      </div>
    </div>
  </div>
</div>
<?php
}
?>
