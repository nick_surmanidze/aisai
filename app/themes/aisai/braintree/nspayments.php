<?php

// Load admin page
require_once( dirname( __FILE__ ). '/options.php' );

// Load ApiCaller
if ( !class_exists( 'ApiCaller' ) && file_exists( dirname( __FILE__ ) . '/lib/apicaller.class.php' ) ) {
    require_once( dirname( __FILE__ ) . '/lib/apicaller.class.php' );
}

// Load environment
require_once( dirname( __FILE__ ). '/_environment.php' );


// Load payment functions
require_once( dirname( __FILE__ ). '/update-to-smart.php' );
require_once( dirname( __FILE__ ). '/update-to-genius.php' );
require_once( dirname( __FILE__ ). '/update-to-genius-plus.php' );


function braintree_text_field($label, $name, $result) {
    echo('<div class="form-group"><label for="">' . $label . '</label>');
    $fieldValue = isset($result) ? $result->valueForHtmlField($name) : '';
    echo('<input type="text" class="form-control" name="' . $name .'" value="' . $fieldValue . '" placehoder="'.$fieldValue.'" /></div>');
    $errors = isset($result) ? $result->errors->onHtmlField($name) : array();
    foreach($errors as $error) {
        echo('<div style="color: red;">' . $error->message . '</div>');
    }
    echo("\n");
}


#Function to cancel recurring billing
function cancel_transaction($subscription){

 $result = Braintree_Subscription::cancel($subscription);

    if ($result->success) {

        return "Subscription ID: " . $result->subscription->id . " cancelled<br />";

    } else {

        foreach ($result->errors->deepAll() as $error) {
            return $error->code . ": " . $error->message . "<br />";
        }
        exit;
    }
}


#function for removing current subscription and card token
function cancel_subscription_remove_card() {
  // if has subscription, disable subscription
  // Subscription is in user's meta
  $subscription = get_user_subscription();
  if(isset($subscription['subscription_id']) && strlen($subscription['subscription_id']) > 3) {

    // Wrap into try - catch to make sure we do not get fatal error if such id will not be found
    try {
      $subscription_cancelled = cancel_transaction($subscription['subscription_id']);
    }
    catch(Exception $e) {
      echo '' .$e->getMessage();
    }
    // Now we also need to remove the card.
    try {
      $card_removed = Braintree_PaymentMethod::delete($subscription['card_id']);
      }
      catch(Exception $e) {
        echo '' .$e->getMessage();
      }
  }


  // update user subscription meta
  set_user_subscription(0, 0, 0, 0, 0);
}



function update_last_active_payment_to_one_off() {
  // update last payment on og_core to one-off
  global $api;
  $payments = $api->sendRequest(array(
    'action'         => 'read',
    'controller'     => 'payment',
    'account_id'     => '',
    'user_id'        => aisai::user_logged_in()
  ));

  if(count($payments->payments) > 0) {

    $last_payment = $payments->payments[0];

    // check if last payment is active

    if(strtotime('now') < strtotime($last_payment->payment_date . '+1 month')) {

      $api->sendRequest(array(
        'action'     => 'update',
        'controller' => 'payment',
        'id'         => $last_payment->id,
        'type'       => 'one-off'
      ));

    }
  }
}









function create_customer(){
 #Set timezone if not specified in php.ini
//date_default_timezone_set('America/Los_Angeles');
 $includeAddOn = false;

 /* First we create a new user using the BT API */
 $result = Braintree_Customer::create(array(
        'firstName' => mysql_real_escape_string($_POST['first_name']),
        'lastName'  => mysql_real_escape_string($_POST['last_name']),
        //'company'   => mysql_real_escape_string($_POST['company']),
        'email'     => mysql_real_escape_string($_POST['user_email']),
        //'phone'     => mysql_real_escape_string($_POST['user_phone']),

 // we can create a credit card at the same time
                'creditCard' => array(
          'cardholderName'  => mysql_real_escape_string($_POST['full_name']),
          'number'          => mysql_real_escape_string($_POST['card_number']),
          'expirationMonth' => mysql_real_escape_string($_POST['expiry_month']),
          'expirationYear'  => mysql_real_escape_string($_POST['expiry_year']),
          'cvv'             => mysql_real_escape_string($_POST['card_cvv']),
                )
            ));
    if ($result->success) {
      return $result->customer->creditCards[0]->token;
    } else {
        foreach ($result->errors->deepAll() as $error) {
            $errorFound = $error->message . "<br />";
        }
    return $errorFound ;
        exit;
    }
}




#Function to initiate recurring billing accepts credit card token as parameter
function create_transaction($creditCardToken, $planId, $date){

 #Copy Plan ID from BrainTree Sanbox
 //$planId = 'czdw';

 $subscriptionData = array(
 'paymentMethodToken' => $creditCardToken,
 'planId' => $planId,
 'firstBillingDate' => $date
 );

 $result1 = Braintree_Subscription::create($subscriptionData);

 if ($result1->success) {
    return $result1;
 } else {
 foreach ($result1->errors->deepAll() as $error1) {
    $errorFound .= $error1->message . "<br />";
 }
 return $errorFound ;
 exit;
 }
}




function get_single_payment_form($price = 10) {

        if (isset($_GET["id"])) {
            //$result = Braintree_TransparentRedirect::confirm($_SERVER['QUERY_STRING']);
            try {
              $result = Braintree_TransparentRedirect::confirm($_SERVER['QUERY_STRING']);
            }
            catch(Exception $e) {
              echo '' .$e->getMessage();
            }
        }

        if (isset($result) && $result->success) { ?>
            <h5>Payment details:</h5>
            <?php $transaction = $result->transaction; ?>
            <table>
                <tr><td>transaction id: </td><td><?php echo htmlentities($transaction->id); ?></td></tr>
                <tr><td>transaction status: </td><td><?php echo htmlentities($transaction->status); ?></td></tr>
                <tr><td>transaction amount: </td><td><?php echo htmlentities($transaction->amount); ?></td></tr>
                <tr><td>customer first name: </td><td><?php echo htmlentities($transaction->customerDetails->firstName); ?></td></tr>
                <tr><td>customer last name: </td><td><?php echo htmlentities($transaction->customerDetails->lastName); ?></td></tr>
                <tr><td>customer email: </td><td><?php echo htmlentities($transaction->customerDetails->email); ?></td></tr>
                <tr><td>credit card number: </td><td><?php echo htmlentities($transaction->creditCardDetails->maskedNumber); ?></td></tr>
                <tr><td>expiration date: </td><td><?php echo htmlentities($transaction->creditCardDetails->expirationDate); ?></td></tr>
            </table>
        <?php

        } else {

            if (!isset($result)) { $result = null; } ?>

            <?php if (isset($result)) { ?>

                <div style="color: red;"><?php echo $result->errors->deepSize(); ?> error(s)</div>

            <?php } ?>

            <form method="POST" action="<?php echo Braintree_TransparentRedirect::url() ?>" autocomplete="off">
                    <?php braintree_text_field('First Name', 'transaction[customer][first_name]', $result); ?>
                    <?php braintree_text_field('Last Name', 'transaction[customer][last_name]', $result); ?>
                    <?php braintree_text_field('Email', 'transaction[customer][email]', $result); ?>

                    <?php braintree_text_field('Credit Card Number', 'transaction[credit_card][number]', $result); ?>
                    <?php braintree_text_field('Expiration Date (MM/YY)', 'transaction[credit_card][expiration_date]', $result); ?>
                    <?php braintree_text_field('CVV', 'transaction[credit_card][cvv]', $result); ?>

                <?php $tr_data = Braintree_TransparentRedirect::transactionData(
                    array('redirectUrl' => "http://" . $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH),
                    'transaction' => array('amount' => '10.00', 'type' => 'sale'))) ?>
                <input type="hidden" name="tr_data" value="<?php echo $tr_data ?>" />

                <br />
                <input  class="btn btn-success complete-payment" type="submit" value="Submit" />
            </form>

        <?php }
}
?>
