<?php
require_once(dirname( __FILE__ ) . '/lib/Braintree.php');
//Braintree_Configuration::environment('production');
Braintree_Configuration::environment('sandbox');
Braintree_Configuration::merchantId(esc_attr(get_option("nspay_merchant_id")));
Braintree_Configuration::publicKey(esc_attr(get_option("nspay_public_key")));
Braintree_Configuration::privateKey(esc_attr(get_option("nspay_private_key")));
?>
