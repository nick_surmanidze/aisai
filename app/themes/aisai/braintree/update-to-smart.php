<?php
function update_to_smart() {

// when downgrading to smart plan we do not need payment details.

###############################################################################################
###############################################################################################

cancel_subscription_remove_card();

###############################################################################################
###############################################################################################

// if has account, it will be automatically disables once the check plan function fires
// and determines that the user's subscription level is insufficient

if(isset($_SESSION['update_membership']['plan_before']) && $_SESSION['update_membership']['plan_before'] != 'smart') {
  $additional_info = "Your premium features will remain active until your premium memberhsip expires.";
} else {
  $additional_info = '';
}

###############################################################################################
###############################################################################################

update_last_active_payment_to_one_off();

###############################################################################################
###############################################################################################


?>
<div class="payment">
        <div class="content-wrapper">
          <div class="middle-large-section clearfix payment-main">

            <h2>Your Membership changed</h2>

            <div class="col-50">

              <h4><span class="title">Current Plan:</span> <span class="plan <?php echo $_SESSION['update_membership']['plan_before']; ?>"><?php echo get_plan_label($_SESSION['update_membership']['plan_before']); ?></span></h4>
              <h4><span class="title">Next Plan:</span>  <span class="plan <?php echo $_SESSION['update_membership']['plan_after']; ?>"><?php echo get_plan_label($_SESSION['update_membership']['plan_after']); ?></span></h4>
              <h4><span class="title">Total:</span>  <span class="price">£ <?php echo $_SESSION['update_membership']['price']; ?></span></h4>

            </div>


            <div class="col-50">

              <div class="image-container">

                <img class="ssl" alt="ssl" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ssl.png">
                <br>
                <img class="braintree" alt="ssl" src="<?php echo get_stylesheet_directory_uri(); ?>/images/braintree.png">

              </div>
            </div>

            <div class="info-area">You have been downgraded to Smart plan. <?php echo $additional_info; ?></div>

          </div>

        </div>
      </div>

<?php
###############################################################################################
###############################################################################################
// Once the procedure is finished under session variables.
unset($_SESSION['update_membership']['plan_before']);
unset($_SESSION['update_membership']['plan_after']);
unset($_SESSION['update_membership']['type_before']);
unset($_SESSION['update_membership']['type_after']);
unset($_SESSION['update_membership']['price']);
###############################################################################################
###############################################################################################
} ?>
