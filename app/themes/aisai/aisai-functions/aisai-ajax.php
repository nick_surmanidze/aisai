<?php
/**
 * Project related ajax functions
 */



////////////////////////////////////////////////////////////////////////////////////////////////
// aiAjax  -  Create Search

add_action('wp_ajax_ai_create_search', 'ai_create_search');
add_action('wp_ajax_nopriv_ai_create_search', 'ai_create_search');

function ai_create_search() {
  // starting session to access session variables
  @session_start();

  if(isset($_POST['params']) && ($_SESSION['nsauth']['can']['can_publish'] == true)) {

      global $api;

      if(aisai::user_logged_in()) {

      $user_id = aisai::user_logged_in();

      // if user is working in behalf of other account than created job should be using id id of accont host
      if(isset($_SESSION['nsauth']['account']['account_recruiter']) && ($_SESSION['nsauth']['account']['account_recruiter'] > 0)) {
        $user_id = $_SESSION['nsauth']['account']['account_recruiter'];
      }

    $result = $api->sendRequest(array(

      'action'                            => 'create',
      'controller'                        => 'job',
      'id'                                => '',
      'job_author'                        => $user_id,
      'job_title'                         => $_POST['params']['details']['title'],
      'company'                           => $_POST['params']['details']['employer'],
      'job_description'                   => $_POST['params']['details']['job_description'],
      'job_status'                        => 'pending',
      'og_job_query_company_experience'   => array(),
      'og_job_query_company_type'         => aisai::check_array($_POST['params']['searchFields']['idealCompanyType']),
      'og_job_query_contract'             => aisai::check_array($_POST['params']['searchFields']['idealContract']),
      'og_job_query_course_name'          => aisai::check_array($_POST['params']['searchFields']['courseName']),
      'og_job_query_current_contract'     => aisai::check_array($_POST['params']['searchFields']['contract']),
      'og_job_query_current_pay'          => aisai::check_array($_POST['params']['searchFields']['currentPay']),
      'og_job_query_function'             => aisai::check_array($_POST['params']['searchFields']['idealJobFunction']),
      'og_job_query_grade_attained'       => aisai::check_array($_POST['params']['searchFields']['gradeAttained']),
      'og_job_query_industry'             => aisai::check_array($_POST['params']['searchFields']['idealIndustry']),
      'og_job_query_industry_experience'  => aisai::check_array($_POST['params']['searchFields']['industryExperience']),
      'og_job_query_institution'          => array(),
      'og_job_query_location'             => aisai::check_array($_POST['params']['searchFields']['idealLocation']),
      'og_job_query_pay'                  => aisai::check_array($_POST['params']['searchFields']['idealPay']),
      'og_job_query_currency'             => $_POST['params']['searchFields']['currency'],
      'og_job_query_qualification_type'   => aisai::check_array($_POST['params']['searchFields']['qualificationType']),
      'og_job_query_role_experience'      => aisai::check_array($_POST['params']['searchFields']['roleExperience']),
      'og_job_query_seniority'            => aisai::check_array($_POST['params']['searchFields']['idealSeniority']),
      'og_job_query_seniority_experience' => aisai::check_array($_POST['params']['searchFields']['seniority']),
      'og_job_applications'               => array(),
      'og_job_test_requirements'          => aisai::check_array($_POST['params']['capabilities'])
      ));

    // end of the request

    print_r($result);
    die();


      } else {
        // user is not logged in
        print_r('failed');
        die();
      }

  } else {
    // params are not set
    print_r('failed');
    die();
  }

}


// aiAjax  -  Create Search
////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////
// aiAjax  -  output jobs list

add_action('wp_ajax_ai_output_jobs', 'ai_output_jobs');
add_action('wp_ajax_nopriv_ai_output_jobs', 'ai_output_jobs');

function ai_output_jobs() {
  // starting session to access session variables
  @session_start();

  if(isset($_POST['params'])) {

      global $api;

      if(aisai::user_logged_in()) {

        $user_id = aisai::user_logged_in();

        if(isset($_SESSION['nsauth']['account']['account_recruiter']) && ($_SESSION['nsauth']['account']['account_recruiter'] > 0)) {
          $user_id = $_SESSION['nsauth']['account']['account_recruiter'];
        }

        $job_list = $api->sendRequest(array(
          'action'       => 'read',
          'controller'   => 'job',
          'id'           => '',
          'recruiter_id' => $user_id, // indicate 0 for showing all jobs and disregard the author
          'multiple'     => true, //****
          'query_string' => $_POST['params']['query'], // search in title
          'limit'        => $_POST['params']['limit'], //
          'offset'       => $_POST['params']['offset'], //
          'sort_by'      => $_POST['params']['sort_by'], // date, title
          'order'        => $_POST['params']['order'],
          'status'       => $_POST['params']['status'], //all, live, pending, archive
          ));

    $output = '';


    $output['total'] = $job_list->total;

    if($output['total'] > 0) {


    if(aisai::user_logged_in() == $user_id) {
      // if account owner is equal to logged in user

      if(isset($_SESSION['nsauth']['pipeline'])) {

         $pipeline = array(
        'shortlisted_label'  => $_SESSION['nsauth']['pipeline']->shortlisted_label,
        'phone_screen_label' => $_SESSION['nsauth']['pipeline']->phone_screen_label,
        'interview_label'    => $_SESSION['nsauth']['pipeline']->interview_label,
        'offer_label'        => $_SESSION['nsauth']['pipeline']->offer_label,
        'hired_label'        => $_SESSION['nsauth']['pipeline']->hired_label,
        'rejected_label'     => $_SESSION['nsauth']['pipeline']->rejected_label,
        );

      } else {

        $pipeline = array(
          'shortlisted_label'  => 'Shortlisted',
          'phone_screen_label' => 'Phone Screen',
          'interview_label'    => 'Interview',
          'offer_label'        => 'Offer',
          'hired_label'        => 'Hired',
          'rejected_label'     => 'Rejected',
          );
      }


    } else {

         $pipeline = array(
        'shortlisted_label'  => $_SESSION['nsauth']['account']['pipeline']['shortlisted_label'],
        'phone_screen_label' => $_SESSION['nsauth']['account']['pipeline']['phone_screen_label'],
        'interview_label'    => $_SESSION['nsauth']['account']['pipeline']['interview_label'],
        'offer_label'        => $_SESSION['nsauth']['account']['pipeline']['offer_label'],
        'hired_label'        => $_SESSION['nsauth']['account']['pipeline']['hired_label'],
        'rejected_label'     => $_SESSION['nsauth']['account']['pipeline']['rejected_label'],
        );


    }


      $can_edit_details = true;
      if(isset($_SESSION['nsauth']['account']['can_change_details'])) {
        if($_SESSION['nsauth']['account']['can_change_details'] == 0) {
          $can_edit_details = false;
        }
      }


      $output['data'] = '';

      foreach ($job_list->jobs as $single_job) {

        $output['data'] .= '
                  <!-- Job Card - START -->
                    <div class="job-card">
                      <div class="title-row">
                        <span class="title">' . $single_job->job_title . '</span><span class="company-name">' .  $single_job->company . '</span>
                        <div class="btn-wrapper">';

                        if($can_edit_details == true) {

                          $output['data'] .= '<button class="btn btn-xs btn-primary" data-id="'.$single_job->id.'" id="edit_job_details_trigger"><i class="fa fa-pencil"></i> Edit</button>';
                        }

        $output['data'] .= '<a class="btn btn-xs btn-success" href="/manage-job?id='.$single_job->id.'"><i class="fa fa-pie-chart"></i>  Manage</a>
                        </div>

                      </div>
                      <div class="columns">

                          <div class="col-1-6">
                            <div class="line title">'.$pipeline['shortlisted_label'].'</div>
                            <div class="line value">'.$single_job->pipeline->shortlisted.'</div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title">'.$pipeline['phone_screen_label'].'</div>
                            <div class="line value">'.$single_job->pipeline->phone_screen.'</div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title">'.$pipeline['interview_label'].'</div>
                            <div class="line value">'.$single_job->pipeline->interview.'</div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title">'.$pipeline['offer_label'].'</div>
                            <div class="line value">'.$single_job->pipeline->offer.'</div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title">'.$pipeline['hired_label'].'</div>
                            <div class="line value">'.$single_job->pipeline->hired.'</div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title">'.$pipeline['rejected_label'].'</div>
                            <div class="line value">'.$single_job->pipeline->rejected.'</div>
                          </div>

                      </div>
                      <div class="footer-line">

                      <span class="created-on">Created on: ' . $single_job->date_created . '</span>

                      <div class="status-wrapper">
                        <span class="title">Status: </span>
                        <span class="status">' . $single_job->job_status . '</span>
                      </div>

                      </div>

                    </div>
                  <!-- Job Card - END -->';

      }

    } else {

      $output['data'] = '<div class="no-jobs-found">Sorry, nothing found</span>';
    }

    // end of the request

    print_r(json_encode($output));
    die();


      } else {
        // user is not logged in
        print_r('failed');
        die();
      }

  } else {
    // params are not set
    print_r('failed');
    die();
  }

}


// aiAjax  -  output jobs list
////////////////////////////////////////////////////////////////////////////////////////////////










////////////////////////////////////////////////////////////////////////////////////////////////
// aiAjax  -  output jobs list on admin page

add_action('wp_ajax_ai_output_jobs_admin', 'ai_output_jobs_admin');
add_action('wp_ajax_nopriv_ai_output_jobs_admin', 'ai_output_jobs_admin');

function ai_output_jobs_admin() {
  // starting session to access session variables
  @session_start();

  if(isset($_POST['params'])) {

      global $api;


        $job_list = $api->sendRequest(array(
          'action'       => 'read',
          'controller'   => 'job',
          'id'           => '',
          'recruiter_id' => 0, // indicate 0 for showing all jobs and disregard the author
          'multiple'     => true, //****
          'query_string' => $_POST['params']['query'], // search in title
          'limit'        => $_POST['params']['limit'], //
          'offset'       => $_POST['params']['offset'], //
          'sort_by'      => $_POST['params']['sort_by'], // date, title
          'order'        => $_POST['params']['order'],
          'status'       => $_POST['params']['status'], //all, live, pending, archive
          ));

    $output = '';


    $output['total'] = $job_list->total;

    if($output['total'] > 0) {



      $pipeline = array(
        'shortlisted_label'  => 'Shortlisted',
        'phone_screen_label' => 'Phone Screen',
        'interview_label'    => 'Interview',
        'offer_label'        => 'Offer',
        'hired_label'        => 'Hired',
        'rejected_label'     => 'Rejected',
        );


      $output['data'] = '';

      foreach ($job_list->jobs as $single_job) {

        $output['data'] .= '
                  <!-- Job Card - START -->
                    <div class="job-card">
                      <div class="title-row">
                        <span class="title">' . $single_job->job_title . '</span><span class="company-name">' .  $single_job->company . '</span>
                        <div class="btn-wrapper">
                          <a href="admin-manage-job?id=' . $single_job->id . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Edit / Approve</a>

                        </div>

                      </div>
                      <div class="columns">

                          <div class="col-1-6">
                            <div class="line title">'.$pipeline['shortlisted_label'].'</div>
                            <div class="line value">'.$single_job->pipeline->shortlisted.'</div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title">'.$pipeline['phone_screen_label'].'</div>
                            <div class="line value">'.$single_job->pipeline->phone_screen.'</div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title">'.$pipeline['interview_label'].'</div>
                            <div class="line value">'.$single_job->pipeline->interview.'</div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title">'.$pipeline['offer_label'].'</div>
                            <div class="line value">'.$single_job->pipeline->offer.'</div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title">'.$pipeline['hired_label'].'</div>
                            <div class="line value">'.$single_job->pipeline->hired.'</div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title">'.$pipeline['rejected_label'].'</div>
                            <div class="line value">'.$single_job->pipeline->rejected.'</div>
                          </div>

                      </div>
                      <div class="footer-line">

                      <span class="created-on">Created on: ' . $single_job->date_created . '</span>

                      <div class="status-wrapper">
                        <span class="title">Status: </span>
                        <span class="status">' . $single_job->job_status . '</span>
                      </div>

                      </div>

                    </div>
                  <!-- Job Card - END -->';

      }

    } else {

      $output['data'] = '<div class="no-jobs-found">Sorry, nothing found</span>';
    }

    // end of the request

    print_r(json_encode($output));
    die();


  } else {
    // params are not set
    print_r('failed');
    die();
  }

}


// aiAjax  -  output jobs list on admin page
////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  load more conversations
add_action('wp_ajax_og_load_more_conversations', 'og_load_more_conversations');
add_action('wp_ajax_nopriv_og_load_more_conversations', 'og_load_more_conversations');

function og_load_more_conversations() {

  // starting session to access session variables
  @session_start();
  $user_id = aisai::user_logged_in();
    // here all the magic will happen
  if(isset($_POST['params']['limit']) && isset($_POST['params']['section'])) {

    $section = $_POST['params']['section'];
    $limit = $_POST['params']['limit'];

    if(isset($_POST['params']['sort_by'])) {
      $sort_by = $_POST['params']['sort_by'];
    } else {
      $sort_by = 'date';
    }

    if(isset($_POST['params']['order'])) {
      $order = $_POST['params']['order'];
    } else {
      $order = 'DESC';
    }

    if(isset($_POST['params']['query'])) {
      $query = $_POST['params']['query'];
    } else {
      $query = '';
    }

    if($section == 'unread') {
//get_messages($from, $to, $query, $subject, $read, $limit, $offset, $sort_by = 'date', $order = 'DESC', $section = 'inbox')
      $output = aisai::get_messages(0, $user_id, $query, '', 0, $limit, 0, $sort_by, $order, $section);

    } elseif($section == 'sent') {

      $output = aisai::get_messages($user_id, 0, $query, '', 1, $limit, 0, $sort_by, $order, $section);

    } elseif($section == 'inbox') {

      $output = aisai::get_messages(0, $user_id, $query, '', 1, $limit, 0, $sort_by, $order, $section);

    }



    $output_html = '';

    // now we nee to generate output html

    if(count((array) $output->messages) > 0) {

      if($section == 'sent') {

        foreach( (array) $output->messages as $message) {

          $output_html .= '<a href="' . get_home_url() . '/conversation?from-user=' . $message->from_user_id . '&amp;to-user=' . $message->to_user_id . '&amp;subject=' .$message->message_subject .'" class="list-group-item inbox-item">';

          $output_html .= '<h4 class="list-group-item-heading"><span class="direction">FROM:</span>' . $message->sent_to . ' / <span class="subject">' . $message->message_subject . '</span></h4>';
          $output_html .= '<span class="date">' . $message->message_sent_date . '</span>';
          $output_html .= '<p class="list-group-item-text">' . $message->message_body . '</p>';
          $output_html .= '</a>';
         }



      } else {

        foreach( (array) $output->messages as $message) {


          $output_html .= '<a href="' . get_home_url() . '/conversation?from-user=' . $message->from_user_id . '&amp;to-user=' . $message->to_user_id . '&amp;subject=' .$message->message_subject .'" class="list-group-item inbox-item">';

          $output_html .= '<h4 class="list-group-item-heading"><span class="direction">FROM:</span>' . $message->sent_from . ' / <span class="subject">' . $message->message_subject . '</span>';
          if($message->message_read == 0) {
          $output_html .= '<span class="label label-danger">New</span>';
          }
          $output_html .= '</h4>';
          $output_html .= '<span class="date">' . $message->message_sent_date . '</span>';
          $output_html .= '<p class="list-group-item-text">' . $message->message_body . '</p>';
          $output_html .= '</a>';

         }


      }

    $response = array();
    $response['data'] = $output_html;
    $response['total'] = $output->total;

    } else {

    $output_html = "<div class='no-messages'>There are no messages!</div>";

    $response = array();
    $response['data'] = $output_html;
    $response['total'] = 0;
    }




    print_r(json_encode($response));

  } else {
    return 'failed';
    die();
  }


    // end of the request
  die();
}




////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  send message
add_action('wp_ajax_og_send_message', 'og_send_message');
add_action('wp_ajax_nopriv_og_send_message', 'og_send_message');

function og_send_message() {

  // starting session to access session variables
  @session_start();


  $from_id = aisai::user_logged_in();


  if(isset($_POST['params']['recipient']) && isset($_POST['params']['subject'])) {




    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

    $result = $apicaller->sendRequest(array(
        'action'          => 'create',
        'controller'      => 'message',
        'from_user_id'    => $from_id,
        'to_user_id'      => $_POST['params']['recipient'],
        'message_subject' => $_POST['params']['subject'],
        'message_body'    => $_POST['params']['body'],
        'message_read'    => '0', // initially message is unread

      ));


    print_r($result);


  } else {
    return 'failed';
    die();
  }
    // end of the request
  die();
}



////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  get messages.
add_action('wp_ajax_og_get_conversation', 'og_get_conversation');
add_action('wp_ajax_nopriv_og_get_conversation', 'og_get_conversation');

function og_get_conversation() {

  // starting session to access session variables
  @session_start();


  $user_id = aisai::user_logged_in();


  if(isset($_POST['params']['recipient']) && isset($_POST['params']['subject']) && isset($_POST['params']['limit']) && isset($_POST['params']['offset'])) {


    $output = array();
    $html = '';

    $conversation = aisai::get_conversation($user_id, $_POST['params']['recipient'], $_POST['params']['subject'], $_POST['params']['limit'], $_POST['params']['offset']);

    foreach((array) $conversation->messages as $message) {

      if ($message->from_user_id == $user_id) {

        $html .=  "<div class='item out'><div class='timeline'><div class='circle'></div><div class='line'></div></div><div class='item-content'><div class='panel panel-default'><div class='panel-body'>";
        $html .=  "<b>" . $message->sent_from . " ( " . $message->message_sent_date . " )  </b><br>" . $message->message_body . "</div></div></div></div>";

      } else {

        $html .=  "<div class='item in'><div class='timeline'><div class='circle'></div><div class='line'></div></div><div class='item-content'><div class='panel panel-default'><div class='panel-body'>";
        $html .=  "<b>" . $message->sent_from . " ( " . $message->message_sent_date . " )  </b><br>" . $message->message_body . "</div></div></div></div>";

      }


    }

    $output['data'] = $html;
    $output['total'] = $conversation->total;


    print_r(json_encode($output));


  } else {
    return 'failed';
    die();
  }
    // end of the request
  die();
}




////////////////////////////////////////////////////////////////////////////////////////////////
// aisai ajax - update recruitment settings
add_action('wp_ajax_ai_update_rec_settings', 'ai_update_rec_settings');
add_action('wp_ajax_nopriv_ai_update_rec_settings', 'ai_update_rec_settings');

function ai_update_rec_settings() {

  // starting session to access session variables
  @session_start();


  $user_id = aisai::user_logged_in();


  if(  isset($_POST['params']['shortlisted_label'])
    && isset($_POST['params']['phone_screen_label'])
    && isset($_POST['params']['interview_label'])
    && isset($_POST['params']['offer_label'])
    && isset($_POST['params']['hired_label'])
    && isset($_POST['params']['rejected_label'])) {

    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

    $shortlisted_label = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'shortlisted_label', // read by meta id
        'meta_value'   => $_POST['params']['shortlisted_label'], // read by meta id
    ));

    $phone_screen_label = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'phone_screen_label', // read by meta id
        'meta_value'   => $_POST['params']['phone_screen_label'], // read by meta id
    ));

    $interview_label = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'interview_label', // read by meta id
        'meta_value'   => $_POST['params']['interview_label'], // read by meta id
    ));

    $offer_label = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'offer_label', // read by meta id
        'meta_value'   => $_POST['params']['offer_label'], // read by meta id
    ));

    $hired_label = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'hired_label', // read by meta id
        'meta_value'   => $_POST['params']['hired_label'], // read by meta id
    ));

    $rejected_label = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'rejected_label', // read by meta id
        'meta_value'   => $_POST['params']['rejected_label'], // read by meta id
    ));


    // Update session vars

  $_SESSION['nsauth']['pipeline']->shortlisted_label  = $_POST['params']['shortlisted_label'];
  $_SESSION['nsauth']['pipeline']->phone_screen_label = $_POST['params']['phone_screen_label'];
  $_SESSION['nsauth']['pipeline']->interview_label    = $_POST['params']['interview_label'];
  $_SESSION['nsauth']['pipeline']->offer_label        = $_POST['params']['offer_label'];
  $_SESSION['nsauth']['pipeline']->hired_label        = $_POST['params']['hired_label'];
  $_SESSION['nsauth']['pipeline']->rejected_label     = $_POST['params']['rejected_label'];

    print_r('done');

  } else {
    return 'failed';
    die();
  }
    // end of the request
  die();
}



////////////////////////////////////////////////////////////////////////////////////////////////
// aisai ajax - update 'my info' settings
add_action('wp_ajax_ai_update_info_settings', 'ai_update_info_settings');
add_action('wp_ajax_nopriv_ai_update_info_settings', 'ai_update_info_settings');

function ai_update_info_settings() {

  // starting session to access session variables
  @session_start();


  $user_id = aisai::user_logged_in();


  if(  isset($_POST['params']['name'])
    && isset($_POST['params']['surname'])
    && isset($_POST['params']['display_name'])
    && isset($_POST['params']['employer_name'])
    && isset($_POST['params']['linkedin'])
    && isset($_POST['params']['recruiter_email_frequency'])) {

    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

    $name = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'name', // read by meta id
        'meta_value'   => $_POST['params']['name'], // read by meta id
    ));

    $surname = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'surname', // read by meta id
        'meta_value'   => $_POST['params']['surname'], // read by meta id
    ));

    $display_name = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'display_name', // read by meta id
        'meta_value'   => $_POST['params']['display_name'], // read by meta id
    ));

    $employer_name = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'employer_name', // read by meta id
        'meta_value'   => $_POST['params']['employer_name'], // read by meta id
    ));

    $linkedin = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'linkedin', // read by meta id
        'meta_value'   => $_POST['params']['linkedin'], // read by meta id
    ));


    $email_frequency = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'recruiter_email_frequency', // read by meta id
        'meta_value'   => $_POST['params']['recruiter_email_frequency'], // read by meta id
    ));

    $_SESSION['nsauth']['my_info']->name = $_POST['params']['name'];
    $_SESSION['nsauth']['my_info']->surname = $_POST['params']['surname'];
    $_SESSION['nsauth']['my_info']->display_name = $_POST['params']['display_name'];
    $_SESSION['nsauth']['my_info']->employer_name = $_POST['params']['employer_name'];
    $_SESSION['nsauth']['my_info']->linkedin = $_POST['params']['linkedin'];
    $_SESSION['nsauth']['my_info']->recruiter_email_frequency = $_POST['params']['recruiter_email_frequency'];

    print_r('done');

  } else {
    return 'failed';
    die();
  }
    // end of the request
  die();
}



////////////////////////////////////////////////////////////////////////////////////////////////
// aisai ajax - get job details by id
add_action('wp_ajax_ai_get_job_details', 'ai_get_job_details');
add_action('wp_ajax_nopriv_ai_get_job_details', 'ai_get_job_details');

function ai_get_job_details() {

  // starting session to access session variables
  @session_start();


  $user_id = aisai::user_logged_in();


  if(isset($_SESSION['nsauth']['account']['account_recruiter']) && ($_SESSION['nsauth']['account']['account_recruiter'] > 0)) {
    $user_id = $_SESSION['nsauth']['account']['account_recruiter'];
  }

  if(  isset($_POST['params']['id'])) {

    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

     $job = $apicaller->sendRequest(array(
          'action'       => 'read',
          'controller'   => 'job',
          'id'           => $_POST['params']['id'],
          'read_by_profile_id' => 0 // profile id goes here
          ));

     if($job->job->job_author == $user_id) {
      print_r(json_encode($job));
     } else {
      print_r(json_encode(array('hacking attempt?')));
     }



  } else {

    print_r('failed');
    die();
  }
    // end of the request
  die();
}



////////////////////////////////////////////////////////////////////////////////////////////////
// aisai ajax - update job details by id
add_action('wp_ajax_ai_update_job_details', 'ai_update_job_details');
add_action('wp_ajax_nopriv_ai_update_job_details', 'ai_update_job_details');

function ai_update_job_details() {

  // starting session to access session variables
  @session_start();


  $user_id = aisai::user_logged_in();

  if(isset($_SESSION['nsauth']['account']['account_recruiter']) && ($_SESSION['nsauth']['account']['account_recruiter'] > 0)) {
    $user_id = $_SESSION['nsauth']['account']['account_recruiter'];
  }

  if(  isset($_POST['params']['text'])  && isset($_POST['params']['job_id']) ) {

    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

     $job = $apicaller->sendRequest(array(
          'action'          => 'update',
          'controller'      => 'job',
          'id'              => $_POST['params']['job_id'],
          'job_description' => $_POST['params']['text']
          ));



  print_r($_POST['params']['text']);

  } else {

    print_r('failed');
    die();
  }
    // end of the request
  die();
}


////////////////////////////////////////////////////////////////////////////////////////////////
// aisai ajax - update job status
add_action('wp_ajax_ai_update_job_status', 'ai_update_job_status');
add_action('wp_ajax_nopriv_ai_update_job_status', 'ai_update_job_status');

function ai_update_job_status() {

  // starting session to access session variables
  @session_start();

  $user_id = aisai::user_logged_in();

  if(  isset($_POST['params']['status'])  && isset($_POST['params']['job_id']) ) {

    if($_POST['params']['status'] == 'archive') {
      $status = 'archive';
    } else {
      $status = 'pending';
    }

    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

     $job = $apicaller->sendRequest(array(
          'action'          => 'update',
          'controller'      => 'job',
          'id'              => $_POST['params']['job_id'],
          'job_status'      => $status,
          ));


  print_r($status);

  } else {

    print_r('failed');
    die();
  }
    // end of the request
  die();
}


////////////////////////////////////////////////////////////////////////////////////////////////
// aisai ajax - load more candidates
add_action('wp_ajax_ai_load_more_candidates', 'ai_load_more_candidates');
add_action('wp_ajax_nopriv_ai_load_more_candidates', 'ai_load_more_candidates');

function ai_load_more_candidates() {

  // starting session to access session variables
  @session_start();

  $user_id = aisai::user_logged_in();

  if(  isset($_POST['params']['job_id'])  && isset($_POST['params']['limit']) ) {

  $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
  $candidates = $apicaller->sendRequest(array(
    'action'     => 'read',
    'controller' => 'interest',
    'profile_id' => 0,
    'job_id'     => $_POST['params']['job_id'],
    'result'     => 'pass',
    'limit'      => $_POST['params']['limit'],
    'offset'     => 0,
    'show'       => 'company',
    ));

  // defaults before populating an array
  $output = array(
    'live_html'       => '',
    'rejected_html'   => '',
    'live_loaded'     => 0,
    'rejected_loaded' => 0,
    'live_total'      => 0,
    'rejected_total'  => 0,
    );

  $output['live_html'] = '';

  foreach ((array) $candidates->live_candidates as $applicant) {
    $candidate_details = array(
      'name_surname' => 'n/a',
      'job' => '---',
      'company' => '---',
      'profile' => 0,
      'user' => 0
    );

    if($applicant->first_name !== "Nothing Selected" && $applicant->last_name != "Nothing Selected") {
      $candidate_details['name_surname'] = $applicant->first_name . " " . $applicant->last_name;
    } else {
      $candidate_details['name_surname'] = $applicant->user_display_name;
    }

    if($applicant->current_job_function !== "Nothing Selected") {
      $candidate_details['job'] = $applicant->current_job_function;
    }

    if($applicant->current_job_organisation_name !== "Nothing Selected") {
      $candidate_details['company'] = $applicant->current_job_organisation_name;
    }

    $candidate_details['profile'] = $applicant->profile_id;
    $candidate_details['user'] = $applicant->user_id;

    $output['live_html'] .= "<a href='#' data-profile='" . $candidate_details['profile'] . "' data-user='" .  $candidate_details['user'] . "' class='list-group-item candidate-card'>";
    $output['live_html'] .= "<h4 class='list-group-item-heading'>" . $candidate_details['name_surname'] . "</h4>";
    $output['live_html'] .= "<p class='list-group-item-text'>" . $candidate_details['job'] . " at " . $candidate_details['company']. "</p></a>";
  }

  $output['rejected_html'] = '';

  foreach ((array) $candidates->rejected_candidates as $applicant) {
    $candidate_details = array(
      'name_surname' => 'n/a',
      'job' => '---',
      'company' => '---',
      'profile' => 0,
      'user' => 0
    );

    if($applicant->first_name !== "Nothing Selected" && $applicant->last_name != "Nothing Selected") {
      $candidate_details['name_surname'] = $applicant->first_name . " " . $applicant->last_name;
    } else {
      $candidate_details['name_surname'] = $applicant->user_display_name;
    }

    if($applicant->current_job_function !== "Nothing Selected") {
      $candidate_details['job'] = $applicant->current_job_function;
    }

    if($applicant->current_job_organisation_name !== "Nothing Selected") {
      $candidate_details['company'] = $applicant->current_job_organisation_name;
    }

    $candidate_details['profile'] = $applicant->profile_id;
    $candidate_details['user'] = $applicant->user_id;

    $output['rejected_html'] .= "<a href='#' data-profile='" . $candidate_details['profile'] . "' data-user='" .  $candidate_details['user'] . "' class='list-group-item candidate-card'>";
    $output['rejected_html'] .= "<h4 class='list-group-item-heading'>" . $candidate_details['name_surname'] . "</h4>";
    $output['rejected_html'] .= "<p class='list-group-item-text'>" . $candidate_details['job'] . " at " . $candidate_details['company']. "</p></a>";
  }


  $output['live_loaded']     = count((array) $candidates->live_candidates);
  $output['rejected_loaded'] = count((array) $candidates->rejected_candidates);

  $output['live_total']      = $candidates->total_live_candidates;
  $output['rejected_total']  = $candidates->total_rejected_candidates;

  print_r(json_encode($output));

  } else {

    print_r('failed');
    die();
  }
    // end of the request
  die();
}









////////////////////////////////////////////////////////////////////////////////////////////////
// aisai ajax - load more candidates
add_action('wp_ajax_ai_load_candidate_data', 'ai_load_candidate_data');
add_action('wp_ajax_nopriv_ai_load_candidate_data', 'ai_load_candidate_data');

function ai_load_candidate_data() {

  // starting session to access session variables
  @session_start();

  $user_id = aisai::user_logged_in();
  $guest_id = aisai::user_logged_in();

  if(isset($_SESSION['nsauth']['account']['account_recruiter']) && ($_SESSION['nsauth']['account']['account_recruiter'] > 0)) {
    $user_id = $_SESSION['nsauth']['account']['account_recruiter'];
  }



  if(  isset($_POST['params']['job_id'])  && isset($_POST['params']['user_id']) && isset($_POST['params']['profile_id'])) {

    $output = array();


    // get the pipeline
    if(aisai::user_logged_in() == $user_id) {
      // if account owner is equal to logged in user

      if(isset($_SESSION['nsauth']['pipeline'])) {

         $pipeline = array(
        'shortlisted_label'  => $_SESSION['nsauth']['pipeline']->shortlisted_label,
        'phone_screen_label' => $_SESSION['nsauth']['pipeline']->phone_screen_label,
        'interview_label'    => $_SESSION['nsauth']['pipeline']->interview_label,
        'offer_label'        => $_SESSION['nsauth']['pipeline']->offer_label,
        'hired_label'        => $_SESSION['nsauth']['pipeline']->hired_label,
        'rejected_label'     => $_SESSION['nsauth']['pipeline']->rejected_label,
        );

      } else {

        $pipeline = array(
          'shortlisted_label'  => 'Shortlisted',
          'phone_screen_label' => 'Phone Screen',
          'interview_label'    => 'Interview',
          'offer_label'        => 'Offer',
          'hired_label'        => 'Hired',
          'rejected_label'     => 'Rejected',
          );
      }


    } else {

         $pipeline = array(
        'shortlisted_label'  => $_SESSION['nsauth']['account']['pipeline']['shortlisted_label'],
        'phone_screen_label' => $_SESSION['nsauth']['account']['pipeline']['phone_screen_label'],
        'interview_label'    => $_SESSION['nsauth']['account']['pipeline']['interview_label'],
        'offer_label'        => $_SESSION['nsauth']['account']['pipeline']['offer_label'],
        'hired_label'        => $_SESSION['nsauth']['account']['pipeline']['hired_label'],
        'rejected_label'     => $_SESSION['nsauth']['account']['pipeline']['rejected_label'],
        );


    }


    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
    $candidate = $apicaller->sendRequest(array(
    'action'     => 'read',
    'controller' => 'interest',
    'profile_id' => $_POST['params']['profile_id'],
    'job_id'     => $_POST['params']['job_id'],
    'result'      => 'any',
    'limit' => 1000,
    'offset' => 0,
    'show' => 'profile', // company or full
    'genuine_user_id' => $guest_id // if the user is under account
    ));

    $output['raw'] = $candidate;

    // getting current label to display on the profile
    $current_label = 'Shortlisted';
    $special_class = '';

    if($candidate->pipeline_step == 'shortlisted') {
      $current_label = $pipeline['shortlisted_label'];
    } elseif($candidate->pipeline_step == 'phone_screen') {
      $current_label = $pipeline['phone_screen_label'];
    }  elseif($candidate->pipeline_step == 'interview') {
      $current_label = $pipeline['interview_label'];
    }  elseif($candidate->pipeline_step == 'offer') {
      $current_label = $pipeline['offer_label'];
    }  elseif($candidate->pipeline_step == 'hired') {
      $current_label = $pipeline['hired_label'];
    }   elseif($candidate->pipeline_step == 'rejected') {
      $current_label = $pipeline['rejected_label'];
      $special_class = 'rejected-red';
    }



    // data for populating name and tags section

    $name_and_tags = array(
      'name' => '',
      'phone' => 'n/a',
      'email' => 'n/a',
      'job' => 'n/a',
      'company' => 'n/a',
      'tags' => '',
      'pool_id' => '');


    if($candidate->profile->first_name !== "Nothing Selected" && $candidate->profile->last_name != "Nothing Selected") {
      $name_and_tags['name'] = $candidate->profile->first_name . " " . $candidate->profile->last_name;
    } else {
      $name_and_tags['name'] = $candidate->user->user_display_name;
    }

    if($candidate->profile->current_job->current_job_function !== "Nothing Selected") {
      $name_and_tags['job'] = $candidate->profile->current_job->current_job_function;
    }

    if($candidate->profile->current_job->current_job_organisation_name !== "Nothing Selected") {
      $name_and_tags['company'] = $candidate->profile->current_job->current_job_organisation_name;
    }

    if($candidate->profile->email_address !== "Nothing Selected") {
      $name_and_tags['email'] = $candidate->profile->email_address;
    }

    if($candidate->profile->phone_number !== "Nothing Selected") {
      $name_and_tags['phone'] = $candidate->profile->phone_number;
    }

    if(count((array) $candidate->tags) > 0) {
      if(isset($candidate->tags->pool_tag1) && (strlen($candidate->tags->pool_tag1)) > 0) {
        $name_and_tags['tags'] .= "<span class='tag'>".$candidate->tags->pool_tag1."</span>";
      }
      if(isset($candidate->tags->pool_tag2) && (strlen($candidate->tags->pool_tag2)) > 0) {
        $name_and_tags['tags'] .= "<span class='tag'>".$candidate->tags->pool_tag2."</span>";
      }
      if(isset($candidate->tags->pool_tag3) && (strlen($candidate->tags->pool_tag3)) > 0) {
        $name_and_tags['tags'] .= "<span class='tag'>".$candidate->tags->pool_tag3."</span>";
      }
      if(isset($candidate->tags->id) && ($candidate->tags->id > 0)) {
        $name_and_tags['pool_id'] = $candidate->tags->id;
      } else {
        $name_and_tags['pool_id'] = 0;
      }
    }






    // profile fields
    $profile = $candidate->profile;

    $ideal = array(
    'function'      => aisai::check_selected($profile->ideal_job->og_pr_ideal_job_function),
    'seniority'     => aisai::check_selected($profile->ideal_job->ideal_job_seniority),
    'industry'      => aisai::check_selected($profile->ideal_job->og_pr_ideal_job_industry),
    'location'      => aisai::check_selected($profile->ideal_job->og_pr_ideal_job_location),
    'company_type'  => aisai::check_selected($profile->ideal_job->og_pr_ideal_job_company_type),
    'contract_type' => aisai::check_selected($profile->ideal_job->ideal_job_contract_type),
    'salary'        => aisai::check_selected($profile->ideal_job->ideal_job_basic_salary),
    'currency'      => aisai::check_selected($profile->ideal_job->ideal_job_basic_salary_currency)
    );


    $personal = array(
      'name'    => aisai::check_selected($profile->first_name),
      'surname' => aisai::check_selected($profile->last_name),
      'email'   => aisai::check_selected($profile->email_address),
      'phone'   => aisai::check_selected($profile->phone_number),
      'country' => aisai::check_selected($profile->country),
      'summary' => stripslashes(aisai::check_selected($profile->personal->personality))
    );



  if (aisai::completed_current_job($profile)) {
    $current = array(
      'organisation_name' => aisai::check_selected($profile->current_job->current_job_organisation_name),
      'industry'          => aisai::check_selected($profile->current_job->current_job_industry),
      'company_type'      => aisai::check_selected($profile->current_job->current_job_company_type),
      'job_function'      => aisai::check_selected($profile->current_job->current_job_function),
      'seniority'         => aisai::check_selected($profile->current_job->current_job_seniority),
      'year_started'      => aisai::check_selected($profile->current_job->current_job_year_started),
      'contract'          => aisai::check_selected($profile->current_job->current_job_type_of_contract),
      'notice_period'     => aisai::check_selected($profile->current_job->current_job_notice_period),
      'salary'            => aisai::check_selected($profile->current_job->current_job_basic_salary),
      'currency'          => aisai::check_selected($profile->current_job->current_job_basic_salary_currency),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->current_job->current_job_main_responsibilities))
    );
  }


  if (aisai::completed_experience_one($profile)) {
    $experience_1 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_1->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_1->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_1->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_1->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_1->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_1->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_1->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_1->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_1->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_experience_two($profile)) {
    $experience_2 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_2->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_2->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_2->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_2->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_2->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_2->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_2->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_2->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_2->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_experience_three($profile)) {
    $experience_3 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_3->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_3->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_3->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_3->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_3->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_3->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_3->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_3->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_3->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_experience_four($profile)) {
    $experience_4 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_4->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_4->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_4->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_4->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_4->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_4->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_4->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_4->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_4->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_experience_five($profile)) {
    $experience_5 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_5->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_5->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_5->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_5->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_5->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_5->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_5->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_5->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_5->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_experience_six($profile)) {
    $experience_6 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_6->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_6->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_6->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_6->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_6->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_6->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_6->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_6->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_6->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_experience_seven($profile)) {
    $experience_7 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_7->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_7->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_7->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_7->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_7->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_7->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_7->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_7->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_7->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_education_one($profile)) {
    $education_1 = array(
      'university'    => stripslashes(aisai::check_selected($profile->education_1->university)),
      'course_name'   => stripslashes(aisai::check_selected($profile->education_1->course_name)),
      'qualification' => aisai::check_selected($profile->education_1->qualification_type),
      'grade'         => aisai::check_selected($profile->education_1->grade_attained),
      'year'          => aisai::check_selected($profile->education_1->year_attained)
    );
  }

  if (aisai::completed_education_two($profile)) {
    $education_2 = array(
      'university'    => stripslashes(aisai::check_selected($profile->education_2->university)),
      'course_name'   => stripslashes(aisai::check_selected($profile->education_2->course_name)),
      'qualification' => aisai::check_selected($profile->education_2->qualification_type),
      'grade'         => aisai::check_selected($profile->education_2->grade_attained),
      'year'          => aisai::check_selected($profile->education_2->year_attained)
    );
  }

  if (aisai::completed_education_three($profile)) {
    $education_3 = array(
      'university'    => stripslashes(aisai::check_selected($profile->education_3->university)),
      'course_name'   => stripslashes(aisai::check_selected($profile->education_3->course_name)),
      'qualification' => aisai::check_selected($profile->education_3->qualification_type),
      'grade'         => aisai::check_selected($profile->education_3->grade_attained),
      'year'          => aisai::check_selected($profile->education_3->year_attained)
    );
  }

  if (aisai::completed_skills($profile)) {
    $skill = array(
        'one'   => aisai::check_selected($profile->skills->specialist_skill_1),
        'two'   => aisai::check_selected($profile->skills->specialist_skill_2),
        'three' => aisai::check_selected($profile->skills->specialist_skill_3)
    );
  }



// id we are in the account, than do not output 'edit tags' button
if(aisai::user_logged_in() != $user_id) {
  $show_tags_editor = false;
} else {
  $show_tags_editor = true;
}

$is_premium_member = true;

if(aisai::user_logged_in() == $user_id) {
  // user is not using an account
  if(isset($_SESSION['nsauth']['plan']) && $_SESSION['nsauth']['plan'] == 'smart') {
    $is_premium_member = false;
  }
}


    $output['html'] = "    <!-- ############# Candidate data start ################ -->
    <!-- Start of toolbar -->
    <div class='toolbar'>
      <div class='candidate_pipeline_step ".$special_class."' data-step='".$candidate->pipeline_step."'>".$current_label."</div>
      <div class='btn-group' role='group' aria-label='...'>
        <a href='#profile' class='btn btn-default'>Profile</a>
        <a href='#chat' class='btn btn-default'>Chat</a>";


         if($candidate->pipeline_step == 'shortlisted') {

            $button_set = "<li><a href='#' data-next='phone_screen'>".$pipeline['phone_screen_label']."</a></li>
            <li><a href='#' data-next='interview'>".$pipeline['interview_label']."</a></li>
            <li><a href='#' data-next='offer'>".$pipeline['offer_label']."</a></li>
            <li><a href='#' data-next='hired'>".$pipeline['hired_label']."</a></li>
            <li><a href='#' data-next='rejected'>".$pipeline['rejected_label']."</a></li>";

         } elseif ($candidate->pipeline_step == 'phone_screen') {

            $button_set = "<li><a href='#' data-next='interview'>".$pipeline['interview_label']."</a></li>
            <li><a href='#' data-next='offer'>".$pipeline['offer_label']."</a></li>
            <li><a href='#' data-next='hired'>".$pipeline['hired_label']."</a></li>
            <li><a href='#' data-next='rejected'>".$pipeline['rejected_label']."</a></li>";

         } elseif ($candidate->pipeline_step == 'interview') {

            $button_set = "<li><a href='#' data-next='offer'>".$pipeline['offer_label']."</a></li>
            <li><a href='#' data-next='hired'>".$pipeline['hired_label']."</a></li>
            <li><a href='#' data-next='rejected'>".$pipeline['rejected_label']."</a></li>";

         } elseif ($candidate->pipeline_step == 'offer') {

            $button_set = "<li><a href='#' data-next='hired'>".$pipeline['hired_label']."</a></li>
            <li><a href='#' data-next='rejected'>".$pipeline['rejected_label']."</a></li>";

         } elseif ($candidate->pipeline_step == 'hired') {

            $button_set = "<li><a href='#' data-next='rejected'>".$pipeline['rejected_label']."</a></li>";

         }

        if($candidate->pipeline_step != 'rejected') {
          $output['html'] .= "<div class='btn-group' role='group'>
          <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            Move to
            <span class='caret'></span>
          </button>
          <ul class='dropdown-menu move-through-pipeline' data-profile-id='".$_POST['params']['profile_id']."'>"
          .$button_set.
          "
          </ul>
        </div>";
        }



    $output['html'] .= "
      </div>
    </div>
    <!-- End of toolbar -->

    <!-- Name and Tags - Begin -->
    <div class='white-panel basic-profile-panel'>
      <h4 class='list-group-item-heading'>".$name_and_tags['name']."</h4>
      <p class='list-group-item-text'>".$name_and_tags['job']." at ".$name_and_tags['company']."</p>
      <span class='mail'><i class='fa fa-envelope-o'></i>".$name_and_tags['email']."</span>
      <span class='phone'><i class='fa fa-phone'></i>".$name_and_tags['phone']."</span>

      <div class='tags-area'>";

      if($is_premium_member == true) {

       $output['html'] .= " <div class='tags'>
          ".$name_and_tags['tags']."
        </div>";
        if($show_tags_editor == true) {
          $output['html'] .= "<button class='btn btn-sm btn-info' id='edit-candidate-tags' data-pool-id='".$name_and_tags['pool_id']."'>Add / Edit Tags</button>";
        }


   } else {

    $output['html'] .= "<div class='premium-feature-box'>Talent pool is available only for premium members.<br> You can <a href='/membership-settings'>upgrade your membership here >> </a></div>";

   }
    $output['html'] .= "</div>";
    $output['html'] .= "</div>
    <!-- Name and Tags - END -->";



  // Messaging

  $conversation = $candidate->conversation;

  $recruiter_display_name = $_SESSION['nsauth']['user_display_name'];
  $message_output = '';



   if(count( (array) $conversation->messages) > 0 ) {


    foreach ((array) $conversation->messages as $message) {

      if ($message->from_user_id == $guest_id) {
        // if current user id is "from_user_id" then this is a sent message

        $message_output .= "<div class='item out'>
          <div class='timeline'>
            <div class='circle'></div>
            <div class='line'></div>
          </div>
          <div class='item-content'>
            <div class='panel panel-default'>
              <div class='panel-body'>
               <b>". $message->sent_from." ( ". $message->message_sent_date." )  </b><br>". $message->message_body."
              </div>
            </div>
          </div>
        </div>";


       } else {


        $message_output .= "<div class='item in'>
          <div class='timeline'>
            <div class='circle'></div>
            <div class='line'></div>
          </div>
          <div class='item-content'>
            <div class='panel panel-default'>
              <div class='panel-body'>
              <b>". $message->sent_from." ( ". $message->message_sent_date." )  </b><br>". $message->message_body."
              </div>
            </div>
          </div>
        </div>";

       }

    }

  } else {

    $message_output .= "<div class='no-messages'>You do not have any open conversation with this user regarding this job.</div>";

  }









    $output['html'] .= "<!-- Messaging - Start -->
    <div class='white-panel chat-panel' id='chat'>

      <div class='section-content'>

        <!-- Start Conversation -->
        <div class='conversation'>";

        if( count( (array) $conversation->messages) < $conversation->total) {

          $output['html'] .= "<div class='load-more'>
            <span id='load-older-msgs'>Load Older Messages</span>
          </div>";

         }

          $output['html'] .= "<div class='timeline-wrapper  col-md-12'>

            ".$message_output."

          </div>
        </div>

        <div class='editor'>
          <textarea name='msg' id='msg-text' cols='30' rows='4'></textarea>
          <span class='chars-counter'>chars left <span class='chars-num'>1500</span></span>
          <span id='send-btn' data-recipient='".$_POST['params']['user_id']."' data-sender-name='".$recruiter_display_name."' class='btn btn-info btn-sm'><i class='fa fa-paper-plane-o'></i> send message</span>
        </div>

        <!-- End Conversation -->

      </div>
    </div>
    <!-- Messaging - END -->

    <!-- Profile - Begin -->
    <div class='white-panel full-profile-panel' id='profile'>
      <div class='section-content'>
        <!-- Summary - start -->
        <div class='subtitle col-md-12'>
          <h2>Summary</h2>
        </div>
        <div class='summary col-md-12'>
          ".$personal['summary']."
        </div>
        <!-- Summary - end -->

        <div class='subtitle col-md-12'>
          <h2>Career History and Key Achievements</h2>
        </div>

        <div class='career timeline-wrapper col-md-12'>";

        // Output carreer history

        if(aisai::completed_current_job($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$current['seniority'] .", </span>
                        <span>".$current['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$current['organisation_name'] .", </span>
                        <span>".$current['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$current['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$current['year_started'] ." </span><span> to </span>
                        <span>date. </span><br>";
                        if(strlen(strip_tags($current['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$current['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }


        if(aisai::completed_experience_one($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_1['seniority'] .", </span>
                        <span>".$experience_1['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_1['organisation_name'] .", </span>
                        <span>".$experience_1['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_1['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_1['year_started'] ." </span><span> to </span>
                        <span>".$experience_1['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_1['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_1['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }

        if(aisai::completed_experience_two($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_2['seniority'] .", </span>
                        <span>".$experience_2['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_2['organisation_name'] .", </span>
                        <span>".$experience_2['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_2['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_2['year_started'] ." </span><span> to </span>
                        <span>".$experience_2['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_2['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_2['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }


        if(aisai::completed_experience_three($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_3['seniority'] .", </span>
                        <span>".$experience_3['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_3['organisation_name'] .", </span>
                        <span>".$experience_3['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_3['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_3['year_started'] ." </span><span> to </span>
                        <span>".$experience_3['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_3['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_3['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }


        if(aisai::completed_experience_four($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_4['seniority'] .", </span>
                        <span>".$experience_4['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_4['organisation_name'] .", </span>
                        <span>".$experience_4['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_4['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_4['year_started'] ." </span><span> to </span>
                        <span>".$experience_4['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_4['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_4['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }


        if(aisai::completed_experience_five($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_5['seniority'] .", </span>
                        <span>".$experience_5['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_5['organisation_name'] .", </span>
                        <span>".$experience_5['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_5['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_5['year_started'] ." </span><span> to </span>
                        <span>".$experience_5['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_5['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_5['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }


        if(aisai::completed_experience_six($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_6['seniority'] .", </span>
                        <span>".$experience_6['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_6['organisation_name'] .", </span>
                        <span>".$experience_6['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_6['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_6['year_started'] ." </span><span> to </span>
                        <span>".$experience_6['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_6['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_6['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }



        if(aisai::completed_experience_seven($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_7['seniority'] .", </span>
                        <span>".$experience_7['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_7['organisation_name'] .", </span>
                        <span>".$experience_7['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_7['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_7['year_started'] ." </span><span> to </span>
                        <span>".$experience_7['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_7['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_7['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }


        $output['html'] .= "</div>";


        $output['html'] .= "<div class='subtitle col-md-12'>
          <h2>Education and Skills</h2>
        </div>

        <div class='education timeline-wrapper col-md-12'>";




          if(aisai::completed_education_one($profile)) {

            $output['html'] .= "<div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>
                    <h5 class='list-group-item-heading'>
                        <span>". $education_1['university'] ."</span>
                    </h5>
                    <span>". $education_1['course_name'] ."</span>
                    <span>(". $education_1['qualification'] .")</span><br>
                    <span>Grade Attained: ". $education_1['grade'] ."</span><br>
                    <small>
                        <span>Awarded in ". $education_1['year'] ."</span>
                    </small>
                  </div>
                </div>
              </div>
            </div>";

          }

          if(aisai::completed_education_two($profile)) {

            $output['html'] .= "<div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>
                    <h5 class='list-group-item-heading'>
                        <span>". $education_2['university'] ."</span>
                    </h5>
                    <span>". $education_2['course_name'] ."</span>
                    <span>(". $education_2['qualification'] .")</span><br>
                    <span>Grade Attained: ". $education_2['grade'] ."</span><br>
                    <small>
                        <span>Awarded in ". $education_2['year'] ."</span>
                    </small>
                  </div>
                </div>
              </div>
            </div>";

          }

          if(aisai::completed_education_three($profile)) {

            $output['html'] .= "<div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>
                    <h5 class='list-group-item-heading'>
                        <span>". $education_3['university'] ."</span>
                    </h5>
                    <span>". $education_3['course_name'] ."</span>
                    <span>(". $education_3['qualification'] .")</span><br>
                    <span>Grade Attained: ". $education_3['grade'] ."</span><br>
                    <small>
                        <span>Awarded in ". $education_3['year'] ."</span>
                    </small>
                  </div>
                </div>
              </div>
            </div>";

          }


        $output['html'] .= "</div>";

        $output['html'] .= "<div class='subtitle col-md-12'>
          <h2>Specialist and Technical Skills</h2>
        </div>

        <div class='skills col-md-12'>
          <ul class='list-group'>";

            if(isset($skill['one']) && strlen($skill['one']) > 0) {
              $output['html'] .= "<li class='list-group-item'>".$skill['one']."</li>";
            }
            if(isset($skill['two']) && strlen($skill['two']) > 0) {
              $output['html'] .= "<li class='list-group-item'>".$skill['two']."</li>";
            }
            if(isset($skill['three']) && strlen($skill['three']) > 0) {
              $output['html'] .= "<li class='list-group-item'>".$skill['three']."</li>";
            }

          $output['html'] .= "</ul>
        </div>
      </div>
    </div>
    <!-- Profile - END -->
    <!-- ############# Candidate data end ################ -->";

  print_r(json_encode($output));

  } else {

    print_r('failed');
    die();
  }
    // end of the request
  die();
}



////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  send message
add_action('wp_ajax_ai_move_through_pipeline', 'ai_move_through_pipeline');
add_action('wp_ajax_nopriv_ai_move_through_pipeline', 'ai_move_through_pipeline');

function ai_move_through_pipeline() {

  // starting session to access session variables
  @session_start();

  if(isset($_POST['params']['job_id']) && isset($_POST['params']['profile_id']) && isset($_POST['params']['pipeline_step'])) {

    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

    $result = $apicaller->sendRequest(array(
      'action'     => 'update',
      'controller' => 'interest',
      'profile_id' => $_POST['params']['profile_id'],
      'job_id'     => $_POST['params']['job_id'],
      'pipeline_step' => $_POST['params']['pipeline_step'],
    ));

    print_r($result);

  } else {
    return 'failed';
    die();
  }
    // end of the request
  die();
}




////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  send message
add_action('wp_ajax_ai_update_candidate_tags', 'ai_update_candidate_tags');
add_action('wp_ajax_nopriv_ai_update_candidate_tags', 'ai_update_candidate_tags');

function ai_update_candidate_tags() {

  // starting session to access session variables
  @session_start();

  if(isset($_POST['params']['pool_id'])
    && isset($_POST['params']['profile_id'])
    && isset($_POST['params']['action'])
    && isset($_POST['params']['tag_one'])
    && isset($_POST['params']['tag_two'])
    && isset($_POST['params']['tag_three'])
    ) {

    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

    if($_POST['params']['action'] == 'update') {


      $result = $apicaller->sendRequest(array(
        'action'     => 'update',
        'controller' => 'pool',
        'id' => $_POST['params']['pool_id'],
        'pool_tag1' => $_POST['params']['tag_one'],
        'pool_tag2' => $_POST['params']['tag_two'],
        'pool_tag3' => $_POST['params']['tag_three']
      ));

    }

    if($_POST['params']['action'] == 'create') {

      $user_id = aisai::user_logged_in();

      $result = $apicaller->sendRequest(array(
         'action'     => 'create',
         'controller' => 'pool',
         'recruiter_id' => $user_id,
         'profile_id' => $_POST['params']['profile_id'],
         'pool_tag1' => $_POST['params']['tag_one'],
         'pool_tag2' => $_POST['params']['tag_two'],
         'pool_tag3' => $_POST['params']['tag_three']
      ));

    }


    print_r(json_encode((array) $result));

  } else {
    return 'failed';
    die();
  }
    // end of the request
  die();
}









////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  send message
add_action('wp_ajax_ai_remove_candidate_from_pool', 'ai_remove_candidate_from_pool');
add_action('wp_ajax_nopriv_ai_remove_candidate_from_pool', 'ai_remove_candidate_from_pool');

function ai_remove_candidate_from_pool() {

  // starting session to access session variables
  @session_start();

  if(isset($_POST['params']['pool_id'])) {

    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

    $result = $apicaller->sendRequest(array(
      'action'     => 'delete',
      'controller' => 'pool',
      'id' => $_POST['params']['pool_id']
    ));

    print_r(json_encode((array) $result));

  } else {
    return 'failed';
    die();
  }
    // end of the request
  die();
}









////////////////////////////////////////////////////////////////////////////////////////////////
// aiajax - load more pool cards
add_action('wp_ajax_ai_load_more_pool', 'ai_load_more_pool');
add_action('wp_ajax_nopriv_ai_load_more_pool', 'ai_load_more_pool');

function ai_load_more_pool() {

  // starting session to access session variables
  @session_start();


  $recruiter_id = aisai::user_logged_in();

  if(isset($_SESSION['nsauth']['account']['account_recruiter']) && ($_SESSION['nsauth']['account']['account_recruiter'] > 0)) {
    $recruiter_id = $_SESSION['nsauth']['account']['account_recruiter'];
  }

  if($recruiter_id == aisai::user_logged_in()) {
    $can_edit_pool = true;
  } else {
    $can_edit_pool = false;
  }

  if(isset($_POST['params']['limit'])
    && isset($_POST['params']['query'])
    && isset($_POST['params']['sort_by'])
    && isset($_POST['params']['order'])) {




    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

    $result = $apicaller->sendRequest(array(
      'action'       => 'read',
      'controller'   => 'pool',
      'id'           => '',
      'multi'        => true,
      'recruiter_id' => $recruiter_id,
      'profile_id'   => '',
      'qstring'      => $_POST['params']['query'],
      'order'        => $_POST['params']['order'],
      'order_by'     => $_POST['params']['sort_by'],
      'limit'        => $_POST['params']['limit'],
      'offset'       => 0
      ));



    $output = array(
      'current' => $result->current,
      'total' => $result->total,
      'html' => '');

    if($result->current > 0) {
      foreach ((array) $result->pool as $talent) {
        if(strlen($talent->current_job_function) > 0 && $talent->current_job_function != 'Nothing Selected') {
          $job = $talent->current_job_function;
        } else {
          $job = 'n/a';
        }
        if(strlen($talent->current_job_organisation_name) > 0 && $talent->current_job_organisation_name != 'Nothing Selected') {
          $company = $talent->current_job_organisation_name;
        } else {
          $company = 'n/a';
        }
        if((strlen($talent->first_name) > 0) && (strlen($talent->first_name) > 0)) {
          $name = $talent->first_name . ' ' . $talent->last_name;
        } else {
          $name = $talent->user_display_name;
        }
        $tags = '';
        foreach((array) $talent->tags as $tag) {
          if(strlen($tag) > 0) {
            $tags .= "<span class='tag'>" . $tag . "</span>";
          }
        }

      $output['html'] .= "<div  class='list-group-item pool-item pool-" . $talent->id ."'>
            <h4 class='list-group-item-heading'>" . $name ."</h4>
            <div class='buttons'>
              <div class='btn-group btn-group-sm' role='group' aria-label='...'>
                <button type='button' class='btn btn-default show-pool-profile' data-profile-id='" . $talent->profile_id ."' data-pool-slug='" . "pool-".$talent->id ."'><i class='fa fa-eye'></i> View</button>";
                if($can_edit_pool == true) {
                $output['html'] .= "<button type='button' class='btn btn-default edit-pool-tags' data-profile-id='" . $talent->profile_id ."' data-pool-slug='" . "pool-".$talent->id ."'  data-pool-id='" . $talent->id ."' data-recruiter-id='" . $recruiter_id ."'><i class='fa fa-pencil'></i> Edit</button>";
                }

              $output['html'] .= "</div>
            </div>
            <h5>" . $job ." at " . $company ."</h5>
            <span class='tags-label'>Tags:</span><span class='tags-container'>" . $tags ."</span>
          </div>";
      }

    } else {

    $output['html'] .= "<div class='candidate-placeholder'>
    <img src='" . get_stylesheet_directory_uri() . "'/images/logo.png'><br>
      Your search did not give results.
    </div>";

    }



    print_r(json_encode($output));


  } else {
    return 'failed';
    die();
  }
    // end of the request
  die();
}





////////////////////////////////////////////////////////////////////////////////////////////////
// aisai - load cancidate profile from pool
add_action('wp_ajax_ai_load_profile_from_pool', 'ai_load_profile_from_pool');
add_action('wp_ajax_nopriv_ai_load_profile_from_pool', 'ai_load_profile_from_pool');

function ai_load_profile_from_pool() {

  // starting session to access session variables
  @session_start();

  if(isset($_POST['params']['profile_id'])) {

    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

    $result = $apicaller->sendRequest(array(
      'action'     => 'read',
      'controller' => 'profile',
      'id' => $_POST['params']['profile_id']
    ));


    $output = array();

    $output['html'] = '';



   // profile fields
    $profile = $result;

    $ideal = array(
    'function'      => aisai::check_selected($profile->ideal_job->og_pr_ideal_job_function),
    'seniority'     => aisai::check_selected($profile->ideal_job->ideal_job_seniority),
    'industry'      => aisai::check_selected($profile->ideal_job->og_pr_ideal_job_industry),
    'location'      => aisai::check_selected($profile->ideal_job->og_pr_ideal_job_location),
    'company_type'  => aisai::check_selected($profile->ideal_job->og_pr_ideal_job_company_type),
    'contract_type' => aisai::check_selected($profile->ideal_job->ideal_job_contract_type),
    'salary'        => aisai::check_selected($profile->ideal_job->ideal_job_basic_salary),
    'currency'      => aisai::check_selected($profile->ideal_job->ideal_job_basic_salary_currency)
    );


    $personal = array(
      'name'    => aisai::check_selected($profile->first_name),
      'surname' => aisai::check_selected($profile->last_name),
      'email'   => aisai::check_selected($profile->email_address),
      'phone'   => aisai::check_selected($profile->phone_number),
      'country' => aisai::check_selected($profile->country),
      'summary' => stripslashes(aisai::check_selected($profile->personal->personality))
    );



  if (aisai::completed_current_job($profile)) {
    $current = array(
      'organisation_name' => aisai::check_selected($profile->current_job->current_job_organisation_name),
      'industry'          => aisai::check_selected($profile->current_job->current_job_industry),
      'company_type'      => aisai::check_selected($profile->current_job->current_job_company_type),
      'job_function'      => aisai::check_selected($profile->current_job->current_job_function),
      'seniority'         => aisai::check_selected($profile->current_job->current_job_seniority),
      'year_started'      => aisai::check_selected($profile->current_job->current_job_year_started),
      'contract'          => aisai::check_selected($profile->current_job->current_job_type_of_contract),
      'notice_period'     => aisai::check_selected($profile->current_job->current_job_notice_period),
      'salary'            => aisai::check_selected($profile->current_job->current_job_basic_salary),
      'currency'          => aisai::check_selected($profile->current_job->current_job_basic_salary_currency),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->current_job->current_job_main_responsibilities))
    );
  }


  if (aisai::completed_experience_one($profile)) {
    $experience_1 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_1->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_1->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_1->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_1->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_1->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_1->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_1->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_1->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_1->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_experience_two($profile)) {
    $experience_2 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_2->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_2->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_2->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_2->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_2->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_2->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_2->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_2->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_2->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_experience_three($profile)) {
    $experience_3 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_3->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_3->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_3->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_3->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_3->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_3->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_3->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_3->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_3->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_experience_four($profile)) {
    $experience_4 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_4->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_4->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_4->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_4->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_4->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_4->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_4->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_4->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_4->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_experience_five($profile)) {
    $experience_5 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_5->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_5->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_5->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_5->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_5->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_5->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_5->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_5->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_5->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_experience_six($profile)) {
    $experience_6 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_6->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_6->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_6->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_6->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_6->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_6->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_6->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_6->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_6->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_experience_seven($profile)) {
    $experience_7 = array(
      'organisation_name' => stripslashes(aisai::check_selected($profile->experience_7->previous_exp_organisation_name)),
      'industry'          => aisai::check_selected($profile->experience_7->previous_exp_industry),
      'company_type'      => aisai::check_selected($profile->experience_7->previous_exp_company_type),
      'job_function'      => aisai::check_selected($profile->experience_7->previous_exp_job_function),
      'seniority'         => aisai::check_selected($profile->experience_7->previous_exp_seniority),
      'year_started'      => aisai::check_selected($profile->experience_7->previous_exp_year_started),
      'year_ended'        => aisai::check_selected($profile->experience_7->previous_exp_year_ended),
      'contract'          => aisai::check_selected($profile->experience_7->previous_exp_type_of_contract),
      'responsibilities'  => stripslashes(aisai::check_selected($profile->experience_7->previous_exp_main_responsibilities))
    );
  }

  if (aisai::completed_education_one($profile)) {
    $education_1 = array(
      'university'    => stripslashes(aisai::check_selected($profile->education_1->university)),
      'course_name'   => stripslashes(aisai::check_selected($profile->education_1->course_name)),
      'qualification' => aisai::check_selected($profile->education_1->qualification_type),
      'grade'         => aisai::check_selected($profile->education_1->grade_attained),
      'year'          => aisai::check_selected($profile->education_1->year_attained)
    );
  }

  if (aisai::completed_education_two($profile)) {
    $education_2 = array(
      'university'    => stripslashes(aisai::check_selected($profile->education_2->university)),
      'course_name'   => stripslashes(aisai::check_selected($profile->education_2->course_name)),
      'qualification' => aisai::check_selected($profile->education_2->qualification_type),
      'grade'         => aisai::check_selected($profile->education_2->grade_attained),
      'year'          => aisai::check_selected($profile->education_2->year_attained)
    );
  }

  if (aisai::completed_education_three($profile)) {
    $education_3 = array(
      'university'    => stripslashes(aisai::check_selected($profile->education_3->university)),
      'course_name'   => stripslashes(aisai::check_selected($profile->education_3->course_name)),
      'qualification' => aisai::check_selected($profile->education_3->qualification_type),
      'grade'         => aisai::check_selected($profile->education_3->grade_attained),
      'year'          => aisai::check_selected($profile->education_3->year_attained)
    );
  }

  if (aisai::completed_skills($profile)) {
    $skill = array(
        'one'   => aisai::check_selected($profile->skills->specialist_skill_1),
        'two'   => aisai::check_selected($profile->skills->specialist_skill_2),
        'three' => aisai::check_selected($profile->skills->specialist_skill_3)
    );
  }

    $name_and_tags = array(
      'name' => '',
      'phone' => 'n/a',
      'email' => 'n/a',
      'job' => 'n/a',
      'company' => 'n/a');


    if($profile->first_name !== "Nothing Selected" && $profile->last_name != "Nothing Selected") {
      $name_and_tags['name'] = $profile->first_name . " " . $profile->last_name;
    } else {
      $name_and_tags['name'] = 'N/A';
    }

    if($profile->current_job->current_job_function !== "Nothing Selected") {
      $name_and_tags['job'] = $profile->current_job->current_job_function;
    }

    if($profile->current_job->current_job_organisation_name !== "Nothing Selected") {
      $name_and_tags['company'] = $profile->current_job->current_job_organisation_name;
    }

    if($profile->email_address !== "Nothing Selected") {
      $name_and_tags['email'] = $profile->email_address;
    }

    if($profile->phone_number !== "Nothing Selected") {
      $name_and_tags['phone'] = $profile->phone_number;
    }


    $output['html'] .= "<!-- Name and Tags - Begin -->
    <div class='white-panel basic-profile-panel'>
      <h4 class='list-group-item-heading'>".$name_and_tags['name']."</h4>
      <p class='list-group-item-text'>".$name_and_tags['job']." at ".$name_and_tags['company']."</p>
      <span class='mail'><i class='fa fa-envelope-o'></i>".$name_and_tags['email']."</span>
      <span class='phone'><i class='fa fa-phone'></i>".$name_and_tags['phone']."</span>
    </div>
    <!-- Name and Tags - END -->";


    $output['html'] .= "<!-- Profile - Begin -->
    <div class='white-panel full-profile-panel' id='profile'>
      <div class='section-content'>
        <!-- Summary - start -->
        <div class='subtitle col-md-12'>
          <h2>Summary</h2>
        </div>
        <div class='summary col-md-12'>
          ".$personal['summary']."
        </div>
        <!-- Summary - end -->

        <div class='subtitle col-md-12'>
          <h2>Career History and Key Achievements</h2>
        </div>

        <div class='career timeline-wrapper col-md-12'>";

        // Output carreer history

        if(aisai::completed_current_job($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$current['seniority'] .", </span>
                        <span>".$current['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$current['organisation_name'] .", </span>
                        <span>".$current['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$current['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$current['year_started'] ." </span><span> to </span>
                        <span>date. </span><br>";
                        if(strlen(strip_tags($current['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$current['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }


        if(aisai::completed_experience_one($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_1['seniority'] .", </span>
                        <span>".$experience_1['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_1['organisation_name'] .", </span>
                        <span>".$experience_1['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_1['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_1['year_started'] ." </span><span> to </span>
                        <span>".$experience_1['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_1['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_1['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }

        if(aisai::completed_experience_two($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_2['seniority'] .", </span>
                        <span>".$experience_2['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_2['organisation_name'] .", </span>
                        <span>".$experience_2['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_2['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_2['year_started'] ." </span><span> to </span>
                        <span>".$experience_2['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_2['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_2['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }


        if(aisai::completed_experience_three($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_3['seniority'] .", </span>
                        <span>".$experience_3['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_3['organisation_name'] .", </span>
                        <span>".$experience_3['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_3['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_3['year_started'] ." </span><span> to </span>
                        <span>".$experience_3['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_3['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_3['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }


        if(aisai::completed_experience_four($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_4['seniority'] .", </span>
                        <span>".$experience_4['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_4['organisation_name'] .", </span>
                        <span>".$experience_4['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_4['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_4['year_started'] ." </span><span> to </span>
                        <span>".$experience_4['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_4['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_4['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }


        if(aisai::completed_experience_five($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_5['seniority'] .", </span>
                        <span>".$experience_5['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_5['organisation_name'] .", </span>
                        <span>".$experience_5['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_5['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_5['year_started'] ." </span><span> to </span>
                        <span>".$experience_5['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_5['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_5['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }


        if(aisai::completed_experience_six($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_6['seniority'] .", </span>
                        <span>".$experience_6['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_6['organisation_name'] .", </span>
                        <span>".$experience_6['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_6['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_6['year_started'] ." </span><span> to </span>
                        <span>".$experience_6['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_6['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_6['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }



        if(aisai::completed_experience_seven($profile)) {


          $output['html'] .= "
            <div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>

                    <h5 class='list-group-item-heading'>
                        <span>".$experience_7['seniority'] .", </span>
                        <span>".$experience_7['job_function'] ." </span>
                        <br>
                    </h5>
                        <span>".$experience_7['organisation_name'] .", </span>
                        <span>".$experience_7['company_type'] ." </span>
                        <span> in the </span>
                        <span>".$experience_7['industry'] ." </span>
                        <span> sector.</span> <br>
                    <small>
                      <span>".$experience_7['year_started'] ." </span><span> to </span>
                        <span>".$experience_7['year_ended'] . "</span><br>";
                        if(strlen(strip_tags($experience_7['responsibilities'])) > 0) {
                          $output['html'] .= "<span><pre>".$experience_7['responsibilities'] ."</pre></span>";
                        }

                   $output['html'] .= "</small>

                  </div>
                </div>
              </div>
            </div>
          ";

        }


        $output['html'] .= "</div>";


        $output['html'] .= "<div class='subtitle col-md-12'>
          <h2>Education and Skills</h2>
        </div>

        <div class='education timeline-wrapper col-md-12'>";




          if(aisai::completed_education_one($profile)) {

            $output['html'] .= "<div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>
                    <h5 class='list-group-item-heading'>
                        <span>". $education_1['university'] ."</span>
                    </h5>
                    <span>". $education_1['course_name'] ."</span>
                    <span>(". $education_1['qualification'] .")</span><br>
                    <span>Grade Attained: ". $education_1['grade'] ."</span><br>
                    <small>
                        <span>Awarded in ". $education_1['year'] ."</span>
                    </small>
                  </div>
                </div>
              </div>
            </div>";

          }

          if(aisai::completed_education_two($profile)) {

            $output['html'] .= "<div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>
                    <h5 class='list-group-item-heading'>
                        <span>". $education_2['university'] ."</span>
                    </h5>
                    <span>". $education_2['course_name'] ."</span>
                    <span>(". $education_2['qualification'] .")</span><br>
                    <span>Grade Attained: ". $education_2['grade'] ."</span><br>
                    <small>
                        <span>Awarded in ". $education_2['year'] ."</span>
                    </small>
                  </div>
                </div>
              </div>
            </div>";

          }

          if(aisai::completed_education_three($profile)) {

            $output['html'] .= "<div class='item'>
              <div class='timeline'>
                <div class='circle'></div>
                <div class='line'></div>
              </div>
              <div class='item-content'>
                <div class='panel panel-default'>
                  <div class='panel-body'>
                    <h5 class='list-group-item-heading'>
                        <span>". $education_3['university'] ."</span>
                    </h5>
                    <span>". $education_3['course_name'] ."</span>
                    <span>(". $education_3['qualification'] .")</span><br>
                    <span>Grade Attained: ". $education_3['grade'] ."</span><br>
                    <small>
                        <span>Awarded in ". $education_3['year'] ."</span>
                    </small>
                  </div>
                </div>
              </div>
            </div>";

          }


        $output['html'] .= "</div>";

        $output['html'] .= "<div class='subtitle col-md-12'>
          <h2>Specialist and Technical Skills</h2>
        </div>

        <div class='skills col-md-12'>
          <ul class='list-group'>";

            if(isset($skill['one']) && strlen($skill['one']) > 0) {
              $output['html'] .= "<li class='list-group-item'>".$skill['one']."</li>";
            }
            if(isset($skill['two']) && strlen($skill['two']) > 0) {
              $output['html'] .= "<li class='list-group-item'>".$skill['two']."</li>";
            }
            if(isset($skill['three']) && strlen($skill['three']) > 0) {
              $output['html'] .= "<li class='list-group-item'>".$skill['three']."</li>";
            }

          $output['html'] .= "</ul>
        </div>
      </div>
    </div>
    <!-- Profile - END -->
    <!-- ############# Candidate data end ################ -->";



    print_r(json_encode((array) $output));

  } else {
    return 'failed';
    die();
  }
    // end of the request
  die();
}







////////////////////////////////////////////////////////////////////////////////////////////////
// aiAjax  -  update job from admin

add_action('wp_ajax_ai_moderate_job', 'ai_moderate_job');
add_action('wp_ajax_nopriv_ai_moderate_job', 'ai_moderate_job');

function ai_moderate_job() {
  // starting session to access session variables
  @session_start();

  if(isset($_POST['params'])) {

  global $api;

     $job = $api->sendRequest(array(
          'action'          => 'update',
          'controller'      => 'job',
          'id'              => $_POST['params']['job_id'],
          'job_title'       => $_POST['params']['job_title'],
          'company'         => $_POST['params']['company'],
          'job_description' => $_POST['params']['job_description'],
          'job_status'      => $_POST['params']['job_status'],
          'capabilities'    => $_POST['params']['capabilities']
          ));

print_r($job);

  } else {
    // params are not set
    print_r('failed');
    die();
  }

}


// aiAjax  -  update job from admin
////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////
// aiAjax  -  Leave account

add_action('wp_ajax_ai_leave_account', 'ai_leave_account');
add_action('wp_ajax_nopriv_ai_leave_account', 'ai_leave_account');

function ai_leave_account() {
  // starting session to access session variables
  @session_start();

  if(isset($_POST['params']['account_id'])) {

  $user_id = aisai::user_logged_in();

  global $api;

  // get account
  $account = $api->sendRequest(array(
      'action'          => 'read',
      'controller'      => 'account',
      'id'              => $_POST['params']['account_id'],
      ));

  $guest_id_array = array();

  if(count((array)$account->accounts[0]->guests) > 0) {
    foreach((array)$account->accounts[0]->guests as $guest) {
      if($user_id != $guest->user_id) {
        array_push($guest_id_array, $guest->user_id);
      }
    }
  }

  $update = $api->sendRequest(array(
      'action'          => 'update',
      'controller'      => 'account',
      'id'              => $_POST['params']['account_id'],
      'guests'          => $guest_id_array,
  ));

  // switch back to current account (only if deleted account was activated)

  if($_SESSION['nsauth']['account']['id'] == $_POST['params']['account_id']) {

    $account = array(
    'id'                 => 0,
    'account_recruiter'  => $user_id,
    'can_see_pool'       => 1,
    'can_change_status'  => 1,
    'can_create_new'     => 1,
    'can_change_details' => 1,
    'account_title'      => 'My Account');

    $set_default_account = $api->sendRequest(array(
      'action'       => 'create',
      'controller'   => 'meta',
      'data_type'    => 'user', // can be job, profile or user
      'data_id'      => $user_id, // read by profile id -> output all meta as an array
      'meta_key'     => 'current_account_id', // read by meta id
      'meta_value'   => 0, // read by meta id
    ));

   $_SESSION['nsauth']['account'] = $account;

  }

  print_r($guest_id_array);

  } else {
    // params are not set
    print_r('failed');
    die();
  }

}


// aiAjax  -  Leave account
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
// aiAjax  -  Update My Account

add_action('wp_ajax_ai_update_my_account', 'ai_update_my_account');
add_action('wp_ajax_nopriv_ai_update_my_account', 'ai_update_my_account');

function ai_update_my_account() {
  // starting session to access session variables
  @session_start();

  if(isset($_POST['params'])) {

  $user_id = aisai::user_logged_in();

  // temporary default title
  $account_title = 'account_' . $user_id;

  if(isset($_POST['params']['account_title'])) {
    $account_title = $_POST['params']['account_title'];
  }


  global $api;

  // get account (if exists)

  $account = $api->sendRequest(array(
      'action'          => 'read',
      'controller'      => 'account',
      'read_by'         => 'recruiter_id',
      'recruiter_id'    => $user_id,
      ));



  if(isset($account->accounts[0]) && ($account->accounts[0]->id > 0)) {
    // then we already have an account
    $my_account = $account->accounts[0];

  } else {
    // account does not exist and we need to create one!
  $created_account = $api->sendRequest(array(
    'action'             => 'create',
    'controller'         => 'account',
    'recruiter_id'       => $user_id,
    'account_title'      => $account_title,
    'active'             => 1, // account is active or not.
    'can_change_status'  => 1, // guest has permission to change job details
    'can_see_pool'       => 1, // guest can see account owner's talent pool
    'can_change_details' => 1, // guest can change job details
    'can_create_new'     => 1  // guest can create new jobs on behalf of account owner
    ));

  }


  // now re-check if the account was created.

  $account = $api->sendRequest(array(
      'action'          => 'read',
      'controller'      => 'account',
      'read_by'         => 'recruiter_id',
      'recruiter_id'    => $user_id,
      ));


  if(isset($account->accounts[0]) && ($account->accounts[0]->id > 0)) {

  $my_account = $account->accounts[0];

  // account was created if it did not exist before. Now we can handle users.

  // set defaults
  $can_see_pool = 1;
  $can_change_status = 1;
  $can_change_details = 1;
  $can_create_new = 1;

  $new_id_array = array();

  if(isset($_POST['params']['can_see_pool']) && $_POST['params']['can_see_pool'] == 0) {
    $can_see_pool = 0;
  }
  if(isset($_POST['params']['can_change_status']) && $_POST['params']['can_change_status'] == 0) {
    $can_change_status = 0;
  }
  if(isset($_POST['params']['can_change_details']) && $_POST['params']['can_change_details'] == 0) {
    $can_change_details = 0;
  }
  if(isset($_POST['params']['can_create_new']) && $_POST['params']['can_create_new'] == 0) {
    $can_create_new = 0;
  }


  // get the array of old users

  $old_array_of_user_emails = array();

  foreach((array) $my_account->guests as $guest) {
    array_push($old_array_of_user_emails, $guest->user->user_login);
  }

  // get the list of new user emails

  $new_emails = array();

  if(isset($_POST['params']['user_emails']) && (count($_POST['params']['user_emails']) > 0)) {
    $new_emails = $_POST['params']['user_emails'];
  }


  // if new list of user emails matches the old list then just update the para
  if (($old_array_of_user_emails === $new_emails)) {

    $current_guest_ids = array();

    foreach($my_account->guests as $guest) {
      array_push($current_guest_ids, $guest->user_id);
    }


    $api->sendRequest(array(
    'action'             => 'update',
    'controller'         => 'account',
    'id'                 => $my_account->id,
    'account_title'      => $account_title,
    'active'             => 1, // account is active or not.
    'can_change_status'  => $can_change_status, // guest has permission to change job details
    'can_see_pool'       => $can_see_pool, // guest can see account owner's talent pool
    'can_change_details' => $can_change_details, // guest can change job details
    'can_create_new'     => $can_create_new,  // guest can create new jobs on behalf of account owner
    'guests'             => $current_guest_ids,
    ));


    print_r('email list match. updated the rest of the account');
    die();



  } else {


    foreach($new_emails as $new_email) {

      if(in_array($new_email, $old_array_of_user_emails)) {

        //if the user is the same as in the old list then we just add his id to the $new_id_array

        // get user id by email
        $new_user_id = $api->sendRequest(array(
            'action'       => 'read',
            'controller'   => 'user',
            'query_value'  => $new_email,
            'query_handle' => 'login',
          ));

        $new_user_id = $new_user_id->id;

        // if id is valid
        if($new_user_id > 0) {
          array_push($new_id_array, $new_user_id);
        }

      } else {

        // check if the user is registered and send him an email. If user is not registered, than register him.

        $new_user_object = $api->sendRequest(array(
            'action'       => 'read',
            'controller'   => 'user',
            'query_value'  => $new_email,
            'query_handle' => 'login',
          ));



        if(isset($new_user_object->id) && $new_user_object->id > 0) {
          // if this is new user added to account and IS registered

          //push to array
          array_push($new_id_array, $new_user_object->id);

          // send email
          send_email($new_email, 'Aisai - Account was shared with you', 'Account was shared with you. Please log into your Aisai Account to see shared account.');

          // update user meta

          $api->sendRequest(array(
              'action'       => 'create',
              'controller'   => 'meta',
              'data_type'    => 'user', // can be job, profile or user
              'data_id'      => $new_user_object->id, // read by profile id -> output all meta as an array
              'meta_key'     => 'added_to_account_notification', // read by meta id
              'meta_value'   => $my_account->id, // read by meta id
          ));


        } else {

          // if this is new user added to account and IS NOT registered

          // user is not registered. Register him and send him an email notification.
          // $parts = explode("@", $new_email);
          // $name = $parts[0];

          // $created_user = $api->sendRequest(array(
          //   'action'       => 'create',
          //   'controller'   => 'user',
          //   'id'           => 0,
          //   'login'        => $new_email,
          //   'pass'         => 'someJibberishThatDoesnotmatter',
          //   'email'        => $new_email,
          //   'display_name' => $name,
          //   'role'         => "recruiter",
          //   'registered'   => "",
          //   'account_id'   => 0
          // ));

          // if($created_user->id > 0 ) {
          //   // push to array
          //   array_push($new_id_array, $created_user->id);

          $token = array('token' => generateRandomString($length = 10),
            'email' => $new_email,
            'account' => $my_account->id);

          $current_tokens = $api->sendRequest(array(
              'action'       => 'read',
              'controller'   => 'meta',
              'data_type'    => 'user',
              'data_id'      => $user_id,
              'meta_key'     => 'invited_tokens'
          ));

          if(strlen($current_tokens) > 0) {
            // we have some tokens
            if(count(unserialize($current_tokens)) > 0 ) {
              // we have valid tokens
              $current_tokens = unserialize($current_tokens);
            } else {
              $current_tokens = array();
            }
          } else {
            $current_tokens = array();
          }

          // we push new token to old tokens
          array_push($current_tokens, $token);

          $api->sendRequest(array(
              'action'       => 'create',
              'controller'   => 'meta',
              'data_type'    => 'user', // can be job, profile or user
              'data_id'      => $user_id, // read by profile id -> output all meta as an array
              'meta_key'     => 'invited_tokens', // read by meta id
              'meta_value'   => serialize($current_tokens), // read by meta id
          ));

          $link = home_url() . "/?hostaccount=" . $my_account->id . "&hostuser=" . $user_id . "&invitationtoken=" . $token['token'];

          // send email
          send_email($new_email, 'Aisai - Account was shared with you', 'Account was shared with you. Please follow this link to register on aisai and see the shared account. <br> <a href="'.$link.'">Aisai</a>');




            // update meta

          //   $api->sendRequest(array(
          //       'action'       => 'create',
          //       'controller'   => 'meta',
          //       'data_type'    => 'user', // can be job, profile or user
          //       'data_id'      => $created_user->id, // read by profile id -> output all meta as an array
          //       'meta_key'     => 'added_to_account_notification', // read by meta id
          //       'meta_value'   => $my_account->id, // read by meta id
          //   ));

          // }

        }

      }

    }

    // update the account
    $api->sendRequest(array(
    'action'             => 'update',
    'controller'         => 'account',
    'id'                 => $my_account->id,
    'account_title'      => $account_title,
    'active'             => 1, // account is active or not.
    'can_change_status'  => $can_change_status, // guest has permission to change job details
    'can_see_pool'       => $can_see_pool, // guest can see account owner's talent pool
    'can_change_details' => $can_change_details, // guest can change job details
    'can_create_new'     => $can_create_new,  // guest can create new jobs on behalf of account owner
    'guests' => $new_id_array,
    ));


    print_r('emails do not match. Sending emails to new members.');
    die();
  }


  } else {

    print_r('failed to create account');
    die();

  }

  } else {
    // params are not set
    print_r('failed');
    die();
  }

}


// aiAjax  -  Update My Account
////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////////////////////
// aiAjax  -  ai_remove_added_to_account_notification

add_action('wp_ajax_ai_remove_added_to_account_notification', 'ai_remove_added_to_account_notification');
add_action('wp_ajax_nopriv_ai_remove_added_to_account_notification', 'ai_remove_added_to_account_notification');

function ai_remove_added_to_account_notification() {

  // starting session to access session variables
  @session_start();
  $user_id = aisai::user_logged_in();

  global $api;

  $api->sendRequest(array(
    'action'       => 'delete',
    'controller'   => 'meta',
    'data_type'    => 'user',
    'data_id'      => $user_id,
    'meta_key'     => 'added_to_account_notification',
    ));


  print_r('done');
  die();


}


// aiAjax  -  ai_remove_added_to_account_notification
////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////////////////////
// aiAjax  -  ai_deactivate_extra_jobs

add_action('wp_ajax_ai_deactivate_extra_jobs', 'ai_deactivate_extra_jobs');
add_action('wp_ajax_nopriv_ai_deactivate_extra_jobs', 'ai_deactivate_extra_jobs');

function ai_deactivate_extra_jobs() {

  // starting session to access session variables
  @session_start();
  $user_id = aisai::user_logged_in();
  global $api;


  $live_jobs = $api->sendRequest(array(

    'action'       => 'read',
    'controller'   => 'job',
    'id'           => '',
    'recruiter_id' => $user_id, // indicate 0 for showing all jobs and disregard the author
    'multiple'     => true, //****
    'query_string' => '', // search in title ******
    'limit'        => 100, // ***
    'offset'       => 0, // ****
    'sort_by'      => 'date', // date, title
    'order'        => 'desc',
    'status'       => 'live', //all, live, pending, archive
  ));

  $pending_jobs = $api->sendRequest(array(

    'action'       => 'read',
    'controller'   => 'job',
    'id'           => '',
    'recruiter_id' => $user_id, // indicate 0 for showing all jobs and disregard the author
    'multiple'     => true, //****
    'query_string' => '', // search in title ******
    'limit'        => 100, // ***
    'offset'       => 0, // ****
    'sort_by'      => 'date', // date, title
    'order'        => 'desc',
    'status'       => 'pending', //all, live, pending, archive
  ));

  $active_jobs_array = array_merge($live_jobs->jobs, $pending_jobs->jobs);

  if(isset($_POST['params']['keep_job']) && $_POST['params']['keep_job'] > 0) {
    $keep_job = $_POST['params']['keep_job'];
  } else { $keep_job = 0; }

  foreach($active_jobs_array as $active_job) {
    if($active_job->id != $keep_job) {

      $api->sendRequest(array(
        'action'          => 'update',
        'controller'      => 'job',
        'id'              => $active_job->id,
        'job_status'      => 'archive'
      ));

    }

  }


  print_r('done');
  die();


}


// aiAjax  -  ai_deactivate_extra_jobs
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
// aiAjax  -  ai_remove_added_to_account_notification

add_action('wp_ajax_ai_delete_user_account', 'ai_delete_user_account');
add_action('wp_ajax_nopriv_ai_delete_user_account', 'ai_delete_user_account');

function ai_delete_user_account() {

  // starting session to access session variables
  @session_start();
  $user_id = aisai::user_logged_in();

  global $api;

  // DELETE MESSAGES
  $api->sendRequest(array(
    'action'       => 'delete',
    'controller'   => 'message',
    'delete_by_user_id' => $user_id,
  ));

  // DELETE JOBS
  $api->sendRequest(array(
    'action'            => 'delete',
    'controller'        => 'job',
    'delete_by_user_id' => $user_id,
  ));

  // DELETE PAYMENTS
  $api->sendRequest(array(
    'action'            => 'delete',
    'controller'        => 'payment',
    'delete_by_user_id' => $user_id,
  ));

  // DELETE POOL
  $api->sendRequest(array(
    'action'            => 'delete',
    'controller'        => 'pool',
    'multi'             => true,
    'delete_by_user_id' => $user_id,
  ));

  // DELETE META
  $api->sendRequest(array(
    'action'            => 'delete',
    'controller'        => 'meta',
    'delete_by_user_id' => $user_id,
  ));



  // leave accounts shared with me



$accounts_i_m_in = get_accounts_i_m_in();

$accounts = array();

if(count($accounts_i_m_in) > 0) {
  $accounts = $accounts_i_m_in;

  if(count($accounts) > 0) {

    foreach($accounts as $acct) {

      // get account
      $account = $api->sendRequest(array(
          'action'          => 'read',
          'controller'      => 'account',
          'id'              => $acct->id,
          ));

      $guest_id_array = array();

      if(count((array)$account->accounts[0]->guests) > 0) {
        foreach((array)$account->accounts[0]->guests as $guest) {
          if($user_id != $guest->user_id) {
            array_push($guest_id_array, $guest->user_id);
          }
        }
      }

      $update = $api->sendRequest(array(
          'action'          => 'update',
          'controller'      => 'account',
          'id'              => $acct->id,
          'guests'          => $guest_id_array,
      ));

    }
  }
}









  // LOG USER OUT
  unset($_SESSION['nsauth']);


  print_r('done');
  die();


}


// aiAjax  -  ai_remove_added_to_account_notification
////////////////////////////////////////////////////////////////////////////////////////////////


?>
