<?php
  if(
      isset($_POST["bt_signature"]) &&
      isset($_POST["bt_payload"])
  ) {
      $webhookNotification = Braintree_WebhookNotification::parse(
          $_POST["bt_signature"], $_POST["bt_payload"]
      );

      $message =
          "[Webhook Received " . $webhookNotification->timestamp->format('Y-m-d H:i:s') . "] "
          . "Kind: " . $webhookNotification->kind . " | "
          . "Subscription: " . $webhookNotification->subscription->id . "\n";

      file_put_contents(__DIR__ . "/webhook.txt", $message, FILE_APPEND);


      $subscription_id = $webhookNotification->subscription->id;

      // ok, we received a subscription id.
      # We need to find user that has this subscription id
    global $api;

    $user = $api->sendRequest(array(
      'action'       => 'read',
      'controller'   => 'user',
      'query_handle' => 'subscription_id',
      'query_value'  => 'ff6xxr'
    ));

    if($user->id > 0) {

      // then we have such user with such subscription

      # then find out if we have created a payment within 24 hours
      // Basically check if the last payment is older than 24 hours

      $payments = $api->sendRequest(array(
        'action'         => 'read',
        'controller'     => 'payment',
        'account_id'     => '',
        'user_id'        => $user->id
      ));

      if(count($payments->payments) > 0) {

        // we have payments
        $last_payment = $payments->payments[0];

        // check if last payment is made outside 24 hour buffer period around now. Yeah sounds weird!
        // but if we just made payment we already added it to database and it would be wrong to do that twice

        if(strtotime($last_payment->payment_date) < strtotime('now -1 day') ) {

          // then create a new payment

          // get amount

          if($last_payment->payment_plan == 'genius') {
            $amount = get_option("nspay_genius_single_price");
          } else {
            $amount = get_option("nspay_genius_plus_single_price");
          }

          $new_payments = $api->sendRequest(array(
            'action'         => 'create',
            'controller'     => 'payment',
            'account_id'     => 0,
            'user_id'        => $user->id,
            'payment_plan'   => $last_payment->payment_plan,
            'payment_amount' => $amount,
            'type'           => 'subscription',
          ));

        }
      }

    }
  }
?>
