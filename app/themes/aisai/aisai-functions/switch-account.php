<?php


add_action('template_redirect', 'og_account_redirect');

function og_account_redirect(){

@session_start();
global $api;
$current_user_id = aisai::user_logged_in();

if($current_user_id > 0) {
  if(is_page('switch-account')) {


    // when switching via dropdown
    if(isset($_GET['switch_to'])) {



      if($_GET['switch_to'] == 0) {

        $account = array(
          'id' => 0,
          'account_recruiter' => $current_user_id,
          'can_see_pool' => 1,
          'can_change_status' => 1,
          'can_create_new' => 1,
          'can_change_details' => 1,
          'account_title' => 'My Account');

          $set_default_account = $api->sendRequest(array(
            'action'       => 'create',
            'controller'   => 'meta',
            'data_type'    => 'user', // can be job, profile or user
            'data_id'      => $current_user_id, // read by profile id -> output all meta as an array
            'meta_key'     => 'current_account_id', // read by meta id
            'meta_value'   => 0, // read by meta id
          ));

        $_SESSION['nsauth']['account'] = $account;

        // set default account in user meta

      } elseif($_GET['switch_to'] > 0) {

        // check if such account exists and if we are in that account
        $accounts_i_m_in = get_accounts_i_m_in();

        if($accounts_i_m_in != false) {

          foreach($accounts_i_m_in as $acct) {
            if($acct->id == $_GET['switch_to']) {

                $account = array(
                'id' => $acct->id,
                'account_recruiter' => $acct->recruiter_id,
                'can_see_pool' => $acct->can_see_pool,
                'can_change_status' => $acct->can_change_status,
                'can_create_new' => $acct->can_create_new,
                'can_change_details' => $acct->can_change_details,
                'account_title' => $acct->account_title,
                'pipeline' => (array) $acct->pipeline);

                $set_default_account = $api->sendRequest(array(
                  'action'       => 'create',
                  'controller'   => 'meta',
                  'data_type'    => 'user', // can be job, profile or user
                  'data_id'      => $current_user_id, // read by profile id -> output all meta as an array
                  'meta_key'     => 'current_account_id', // read by meta id
                  'meta_value'   => $acct->id, // read by meta id
                ));

              $_SESSION['nsauth']['account'] = $account;

            }
          }

        }



      }




    }


    $referer = $_SERVER['HTTP_REFERER'];
    if ((false !== stripos($_SERVER['HTTP_REFERER'], "manage-job")) && (false !== stripos($_SERVER['HTTP_REFERER'], "create-search"))){
       wp_redirect( site_url() . "/dashboard" ); exit;
    } else {
      wp_redirect( $referer ); exit;

    }


  } else {



    // if we are not on switch to page that means that we are on any page.
    // for example after logging in or registering
    // if session->nsauth->account is not set, then we should set it

   // if(!isset($_SESSION['nsauth']['account'])) {
        $default_account = $api->sendRequest(array(
          'action'       => 'read',
          'controller'   => 'meta',
          'data_type'    => 'user', // can be job, profile or user
          'data_id'      => $current_user_id, // read by profile id -> output all meta as an array
          'meta_key'     => 'current_account_id'
        ));


        if(is_numeric($default_account) && $default_account > 0)  {

        // check if such account exists and if we are in that account
        $accounts_i_m_in = get_accounts_i_m_in();

        if($accounts_i_m_in != false) {

          foreach($accounts_i_m_in as $acct) {
            if($acct->id == $default_account) {

                $account = array(
                'id' => $acct->id,
                'account_recruiter' => $acct->recruiter_id,
                'can_see_pool' => $acct->can_see_pool,
                'can_change_status' => $acct->can_change_status,
                'can_create_new' => $acct->can_create_new,
                'can_change_details' => $acct->can_change_details,
                'account_title' => $acct->account_title,
                'pipeline' => (array) $acct->pipeline);

                $set_default_account = $api->sendRequest(array(
                  'action'       => 'create',
                  'controller'   => 'meta',
                  'data_type'    => 'user', // can be job, profile or user
                  'data_id'      => $current_user_id, // read by profile id -> output all meta as an array
                  'meta_key'     => 'current_account_id', // read by meta id
                  'meta_value'   => $acct->id, // read by meta id
                ));

              $_SESSION['nsauth']['account'] = $account;

            }
          }

        }

        } else {

        $account = array(
          'id'                 => 0,
          'account_recruiter'  => $current_user_id,
          'can_see_pool'       => 1,
          'can_change_status'  => 1,
          'can_create_new'     => 1,
          'can_change_details' => 1,
          'account_title'      => 'My Account');

          $set_default_account = $api->sendRequest(array(
            'action'       => 'create',
            'controller'   => 'meta',
            'data_type'    => 'user', // can be job, profile or user
            'data_id'      => $current_user_id, // read by profile id -> output all meta as an array
            'meta_key'     => 'current_account_id', // read by meta id
            'meta_value'   => 0, // read by meta id
          ));

         $_SESSION['nsauth']['account'] = $account;

        }

   // }


  }
 }
}

?>
