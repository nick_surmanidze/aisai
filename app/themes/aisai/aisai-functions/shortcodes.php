<?php
// Register shortcodes here

function small_pricing() {

  $output = '<div class="pricing-table-small">

<div class="plan smart">
  <div class="title">
    SMART
  </div>
  <div class="description">
  Better hiring starts here. The tools you need to hire people and save on recruitment costs.
  </div>
  <div class="link-to-the-features">
    <a href="/pricing" class="link">Explore Smart Features</a><i class="fa fa-angle-right"></i>
  </div>
  <div class="price">
    FREE
  </div>
  <div class="button-wrapper">
    <a href="/get-started" class="btn btn-primary">SIGN UP</a>
  </div>
</div>

<div class="plan genius">
  <div class="title">
    GENIUS
  </div>
  <div class="description">
  Eliminate wasted time by adding bespoke tests so you only see applications that meet your strictest criteria.
  </div>
  <div class="link-to-the-features">
    <a href="/pricing" class="link">Explore Genius Features</a><i class="fa fa-angle-right"></i>
  </div>
  <div class="price">
    £89 per month
  </div>
  <div class="button-wrapper">
    <a href="/get-started?next=membership" class="btn btn-primary">GET GENIUS</a>
  </div>
</div>

<div class="plan genius-plus">
  <div class="title">
    GENIUS+
  </div>
  <div class="description">
  Perfect for fast growing businesses and professional recruiters. Run 10 active jobs and assign two extra users.
  </div>
  <div class="link-to-the-features">
    <a href="/pricing" class="link">Explore Genius+ Features</a><i class="fa fa-angle-right"></i>
  </div>
  <div class="price">
    £269 per per month
  </div>
  <div class="button-wrapper">
    <a href="/get-started?next=membership" class="btn btn-primary">TAKE GENIUS+</a>
  </div>
</div>


</div>';

  return $output;
}
add_shortcode( 'small_pricing', 'small_pricing' );
?>
