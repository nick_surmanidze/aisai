<?php

@session_start();

add_action('template_redirect', 'ai_redirects');

function ai_redirects(){
### being inside a session is necessary for logging in and out.
@session_start();


   // array with restricted pages
	$restricted_if_not_logged_in = array(
		'dashboard',
		'inbox',
		'conversation',
		'create-search',
		'talent-pool',
		'settings',
		'my-account',
		'shared-accounts',
		'recruitment-settings',
		'membership-settings',
		'payment-details',
		'manage-job'
		);


  $restricted_if_not_admin = array(
    'admin-archive',
    'admin-manage-job',
    'admin-registered-recruiters'
    );


	// if not logged in redirect to home page
	if(aisai::user_logged_in() == false) {
		foreach ($restricted_if_not_logged_in as $restricted_page) {
			if(is_page($restricted_page)) {
				wp_redirect(home_url());
			}
		}
	}

  // if logged in and can manage options
  if(!current_user_can( 'manage_options' )) {
    foreach ($restricted_if_not_admin as $restricted_page) {
      if(is_page($restricted_page)) {
        wp_redirect(home_url());
      }
    }
  }

	// if logged in
	  if(aisai::user_logged_in() != false) {
		if(is_front_page()) {
			wp_redirect(home_url() . '/dashboard');
		}



		// IF USER IS ON dashbaord and comes from upgrade account link
		if(is_page('dashboard') && isset($_SESSION['gts']['next'])) {

      if($_SESSION['gts']['next'] == 'membership') {

        unset($_SESSION['gts']['next']);
        wp_redirect(home_url() . '/membership-settings');

      } elseif($_SESSION['gts']['next'] == 'settings') {

        unset($_SESSION['gts']['next']);
        wp_redirect(home_url() . '/settings');

      }

		}

	}






}

?>
