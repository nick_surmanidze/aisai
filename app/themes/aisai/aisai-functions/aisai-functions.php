<?php


////////////////////////////////////////////////////////////
// Working with values
	@session_start();

// Instatciate api caller

$api = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
//////   #### Calling the functions if needed ####    //////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////


# check plan and update session variable
$user_id = aisai::user_logged_in();
if($user_id  > 0) {

  check_plan();

  $can_create_jobs = can_create_jobs();

  $_SESSION['nsauth']['can'] = $can_create_jobs;

  add_invited_user_to_account();

}




/////////////////////////////////////////////////////////////
// handling webhook



  // if(
  //     isset($_POST["bt_signature"]) &&
  //     isset($_POST["bt_payload"])
  // ) {
  //     $webhookNotification = Braintree_WebhookNotification::parse(
  //         $_POST["bt_signature"], $_POST["bt_payload"]
  //     );

  //     $message =
  //         "[Webhook Received " . $webhookNotification->timestamp->format('Y-m-d H:i:s') . "] "
  //         . "Kind: " . $webhookNotification->kind . " | "
  //         . "Subscription: " . $webhookNotification->subscription->id . "\n";

  //     file_put_contents(__DIR__ . "/webhook.txt", $message, FILE_APPEND);

  // }






/////////////////////////////////////////////////////////////






# functions

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

/**
 * checks if the user registered via invite and adds him to account.
 */

function add_invited_user_to_account() {

if(isset($_SESSION['gts']['hostaccount']) && isset($_SESSION['gts']['hostuser']) && isset($_SESSION['gts']['invitationtoken'])) {

// then the user userd invitation link

// now let's grab the vars
  @session_start();
  global $api;

  $user_id    = aisai::user_logged_in();
  $account_id = $_SESSION['gts']['hostaccount'];
  $host_id    = $_SESSION['gts']['hostuser'];
  $token      = $_SESSION['gts']['invitationtoken'];
  if($user_id > 0) {

  // user is registered and logged in

  // first we need to check the token

  $current_tokens = $api->sendRequest(array(
    'action'       => 'read',
    'controller'   => 'meta',
    'data_type'    => 'user',
    'data_id'      => $host_id,
    'meta_key'     => 'invited_tokens'
  ));


  if(strlen($current_tokens) > 0 && count(unserialize($current_tokens)) > 0) {

    $match = false;

    $current_tokens = unserialize($current_tokens);
    $updates_tokens_array = array();

    foreach($current_tokens as $current_token) {

      if($current_token['token'] == $token) {

        $match = true;

      } else {
        array_push($updates_tokens_array, $current_token);
      }

    }

  // write tokens back to the host (except the used one)
  $api->sendRequest(array(
    'action'       => 'create',
    'controller'   => 'meta',
    'data_type'    => 'user',
    'data_id'      => $host_id,
    'meta_key'     => 'invited_tokens',
    'meta_value'   => serialize($updates_tokens_array)
  ));


    if($match == true) {

        // add user to account

      $account = $api->sendRequest(array(
        'action'          => 'read',
        'controller'      => 'account',
        'read_by'         => 'id',
        'id'    => $account_id,
      ));

      if(isset($account->accounts[0]) && ($account->accounts[0]->id > 0)) {

        $my_account = $account->accounts[0];

        if(count((array) $my_account->guests) < 2) {
          // if still have less than 2 guest then can add this one too

            $current_guest_ids = array();
            foreach($my_account->guests as $guest) {
              array_push($current_guest_ids, $guest->user_id);
            }
              array_push($current_guest_ids, $user_id);


              $api->sendRequest(array(
              'action'             => 'update',
              'controller'         => 'account',
              'id'                 => $my_account->id,
              'account_title'      => $my_account->account_title,
              'active'             => $my_account->active, // account is active or not.
              'can_change_status'  => $my_account->can_change_status, // guest has permission to change job details
              'can_see_pool'       => $my_account->can_see_pool, // guest can see account owner's talent pool
              'can_change_details' => $my_account->can_change_details, // guest can change job details
              'can_create_new'     => $my_account->can_create_new,  // guest can create new jobs on behalf of account owner
              'guests'             => $current_guest_ids,
              ));

              // now update user meta

            $api->sendRequest(array(
                'action'       => 'create',
                'controller'   => 'meta',
                'data_type'    => 'user', // can be job, profile or user
                'data_id'      => $user_id, // read by profile id -> output all meta as an array
                'meta_key'     => 'added_to_account_notification', // read by meta id
                'meta_value'   => $my_account->id, // read by meta id
            ));

        }

      }


    }


  }


  }

}

    // there are no tokens in host's meta
    unset($_SESSION['gts']['hostaccount']);
    unset($_SESSION['gts']['hostuser']);
    unset($_SESSION['gts']['invitationtoken']);

}




/**
 * Get accounts user is added as a guest. Returns an array
 */

function get_accounts_i_m_in() {

    @session_start();
    global $api;
    $user_id = aisai::user_logged_in();

    $result = $api->sendRequest(array(
    'action'         => 'read',
    'controller'     => 'account',
    'read_by'        => 'guest_id', // id, recruiter_id, guest_id
    'active'         => 1,
    'guest_id'       => $user_id
    ));

    if(count((array) $result->accounts) > 0) {

      return $result->accounts;

    } else {

      return false;

    }

}


function set_html_content_type() {
  return 'text/html';
}

function send_email($to, $subject, $body) {

  add_filter( 'wp_mail_content_type', 'set_html_content_type' );

  wp_mail( $to, $subject, $body );

  // Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
  remove_filter( 'wp_mail_content_type', 'set_html_content_type' );


}




/**
 * Check payment plan
 */


function check_plan() {

  @session_start();

  $user_id = aisai::user_logged_in();

  if($user_id > 0) {

    global $api;

    $plan = 'smart';

    $payments = $api->sendRequest(array(
      'action'         => 'read',
      'controller'     => 'payment',
      'account_id'     => '',
      'user_id'        => $user_id
    ));

  // check if user has payment
   if(count((array) $payments->payments) > 0)  {

    foreach( (array) $payments->payments as $pay_record) {

      // 1. check if the payment date is ahead.
      if( strtotime($pay_record->payment_date) < strtotime('now +24 hours')) {
        // if payment date is before today

        $date_of_last_payment = strtotime($pay_record->payment_date);

        $date_had_to_pay = strtotime('now -32 days');

        if( $date_of_last_payment > $date_had_to_pay ) {
          // then you have a valid payment
          $plan = $pay_record->payment_plan;

        }

      }

    }

    }


    if($plan != 'genius_plus') {
      deactivate_account($user_id);
    } else {
      activate_account($user_id);
    }

    $_SESSION['nsauth']['plan'] = $plan;

  } else {

    return false;

  }
}



function check_plan_by_user_id($user_id) {

  @session_start();

  if($user_id > 0) {

    global $api;

    $plan = 'smart';

    $payments = $api->sendRequest(array(
      'action'         => 'read',
      'controller'     => 'payment',
      'account_id'     => '',
      'user_id'        => $user_id
    ));

  // check if user has payment
   if(count((array) $payments->payments) > 0)  {

    foreach( (array) $payments->payments as $pay_record) {

      // 1. check if the payment date is ahead.
      if( strtotime($pay_record->payment_date) < strtotime('now +24 hours')) {
        // if payment date is before today

        $date_of_last_payment = strtotime($pay_record->payment_date);

        $date_had_to_pay = strtotime('now -32 days');

        if( $date_of_last_payment > $date_had_to_pay ) {
          // then you have a valid payment
          $plan = $pay_record->payment_plan;

        }

      }

    }

    }



    return $plan;

  } else {

    return false;

  }
}

/**
 * Activate Account
 */

function activate_account($recruiter_id = 0) {
  global $api;

    $account = $api->sendRequest(array(
      'action'          => 'read',
      'controller'      => 'account',
      'read_by'         => 'recruiter_id',
      'recruiter_id'    => $recruiter_id,
    ));

    if(isset($account->accounts[0]) && ($account->accounts[0]->id > 0)) {
      // user has an account
      $my_account = $account->accounts[0];
      $current_guest_ids = array();
      foreach($my_account->guests as $guest) {
        array_push($current_guest_ids, $guest->user_id);
      }

      $api->sendRequest(array(
        'action'             => 'update',
        'controller'         => 'account',
        'id'                 => $my_account->id,
        'account_title'      => $my_account->account_title,
        'active'             => 1, // account is active or not.
        'can_change_status'  => $my_account->can_change_status, // guest has permission to change job details
        'can_see_pool'       => $my_account->can_see_pool, // guest can see account owner's talent pool
        'can_change_details' => $my_account->can_change_details, // guest can change job details
        'can_create_new'     => $my_account->can_create_new,  // guest can create new jobs on behalf of account owner
        'guests'             => $current_guest_ids,
      ));
    }
}



/**
 * Deactivate Account
 */

function deactivate_account($recruiter_id = 0) {

    global $api;
    $account = $api->sendRequest(array(
      'action'          => 'read',
      'controller'      => 'account',
      'read_by'         => 'recruiter_id',
      'recruiter_id'    => $recruiter_id,
    ));

    if(isset($account->accounts[0]) && ($account->accounts[0]->id > 0)) {
      // user has an account
      $my_account = $account->accounts[0];
      $current_guest_ids = array();
      foreach($my_account->guests as $guest) {
        array_push($current_guest_ids, $guest->user_id);
      }

      $api->sendRequest(array(
        'action'             => 'update',
        'controller'         => 'account',
        'id'                 => $my_account->id,
        'account_title'      => $my_account->account_title,
        'active'             => 0, // account is active or not.
        'can_change_status'  => $my_account->can_change_status, // guest has permission to change job details
        'can_see_pool'       => $my_account->can_see_pool, // guest can see account owner's talent pool
        'can_change_details' => $my_account->can_change_details, // guest can change job details
        'can_create_new'     => $my_account->can_create_new,  // guest can create new jobs on behalf of account owner
        'guests'             => $current_guest_ids,
      ));

      foreach($current_guest_ids as $guest_id) {
        // switch all guest from that account to their current account
        // if they are currently switchied to the account we just deactivated
        $their_current = $api->sendRequest(array(
            'action'       => 'read',
            'controller'   => 'meta',
            'data_type'    => 'user', // can be job, profile or user
            'data_id'      => $guest_id, // read by profile id -> output all meta as an array
            'meta_key'     => 'current_account_id', // read by meta id
        ));

        if($my_account->id == $their_current) {

          $their_current = $api->sendRequest(array(
            'action'       => 'update',
            'controller'   => 'meta',
            'data_type'    => 'user', // can be job, profile or user
            'data_id'      => $guest_id, // read by profile id -> output all meta as an array
            'meta_key'     => 'current_account_id', // read by meta id
            'meta_value' => 0
          ));

        }


      }

    }
}




/**
 * Read payment plan
 */
function read_plan() {

  @session_start();
  $user_id = aisai::user_logged_in();

  if($user_id > 0) {

    if(isset($_SESSION['nsauth']['plan'])) {

      if($_SESSION['nsauth']['plan'] == 'genius') {

        return 'genius';

      } elseif($_SESSION['nsauth']['plan'] == 'genius_plus') {

        return 'genius_plus';

      } else {

        return 'smart';
      }

    } else {

      return 'smart';
    }

  } else {
    return false;
  }
}





/**
* get price that we need to pay when upgrading from genius to genius_plus
 */

function get_upgrade_price($genius_price = 99, $genius_plus_price = 179) {

  @session_start();

  $user_id = aisai::user_logged_in();

  global $api;

  if($user_id > 0 && read_plan() == 'genius') {

    $payments = $api->sendRequest(array(
      'action'         => 'read',
      'controller'     => 'payment',
      'account_id'     => '',
      'user_id'        => $user_id
    ));

    if (count($payments->payments) > 0) {

      $last_payment = $payments->payments[0];

      $last_payment_date = strtotime($last_payment->payment_date);
      $next_payment_date = strtotime($last_payment->payment_date . '+1 month');
      $days_in_month = floor(($next_payment_date - $last_payment_date)/(60*60*24));
      // $days_in_month = 30;
      $days_before_expires = floor(($next_payment_date - strtotime('now'))/(60*60*24));

      // genius plus price / 30 * 15  less genius price / 30 * 15


      $upgrade_fee = ($genius_plus_price / $days_in_month * $days_before_expires) - ($genius_price / $days_in_month * $days_before_expires);

      if($upgrade_fee > 0) {
        return floor($upgrade_fee);
      } else {
        return 0;
      }



    } else { return 0; }


  }

  return 0;

}





/**
 * Has Extra Jobs Live.
 * check if the user has extra jobs live. returns true or false. Correct answer is false :)
 * BTW pending jobs also count.
 */

function has_extra_jobs_live() {

  @session_start();

  $user_id = aisai::user_logged_in();

  global $api;

  $live_jobs = $api->sendRequest(array(

    'action'       => 'read',
    'controller'   => 'job',
    'id'           => '',
    'recruiter_id' => $user_id, // indicate 0 for showing all jobs and disregard the author
    'multiple'     => true, //****
    'query_string' => '', // search in title ******
    'limit'        => 100, // ***
    'offset'       => 0, // ****
    'sort_by'      => 'date', // date, title
    'order'        => 'desc',
    'status'       => 'live', //all, live, pending, archive
  ));

$pending_jobs = $api->sendRequest(array(

    'action'       => 'read',
    'controller'   => 'job',
    'id'           => '',
    'recruiter_id' => $user_id, // indicate 0 for showing all jobs and disregard the author
    'multiple'     => true, //****
    'query_string' => '', // search in title ******
    'limit'        => 100, // ***
    'offset'       => 0, // ****
    'sort_by'      => 'date', // date, title
    'order'        => 'desc',
    'status'       => 'pending', //all, live, pending, archive
  ));

  $total_active_jobs = $live_jobs->total + $pending_jobs->total;

  $plan = read_plan();

  if($plan == 'smart' && $total_active_jobs > 1) {

    return true;

  } elseif ($plan == 'genius' && $total_active_jobs > 1) {

    return true;

  } else {

    return false;
  }
}



/**
 * Get User Subscription
 */
function get_user_subscription() {

  @session_start();

  $user_id = aisai::user_logged_in();

  global $api;

  if($user_id > 0) {

    $output = array(
      'has_subscription' => 1,
      'plan_id'          => 0,
      'card_id'          => 0,
      'subscription_id'  => 0,
      'plan_slug'        => 'none',
      );

    // get the details from user meta

    $output['has_subscription'] = $api->sendRequest(array(
        'action'       => 'read',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'has_subscription', // read by meta id
    ));

    $output['plan_id'] = $api->sendRequest(array(
        'action'       => 'read',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'plan_id', // read by meta id
    ));

    $output['card_id'] = $api->sendRequest(array(
        'action'       => 'read',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'card_id', // read by meta id
    ));

    $output['subscription_id'] = $api->sendRequest(array(
        'action'       => 'read',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'subscription_id', // read by meta id
    ));

    $output['plan_slug'] = $api->sendRequest(array(
        'action'       => 'read',
        'controller'   => 'meta',
        'data_type'    => 'user', // can be job, profile or user
        'data_id'      => $user_id, // read by profile id -> output all meta as an array
        'meta_key'     => 'plan_slug', // read by meta id
    ));

  return $output;

  } else {
    $output = array(
      'has_subscription' => 0,
      'plan_id'          => 0,
      'card_id'          => 0,
      'subscription_id'  => 0,
      'plan_slug'        => 'none',
      );

    return $output;
  }
}




/**
 * SET User Subscription
 */
function set_user_subscription($has_subscription = 0, $plan_id = 0, $card_id = 0, $subscription_id = 0, $plan_slug = 0) {

  @session_start();
  $user_id = aisai::user_logged_in();
  global $api;

  if($user_id > 0) {


    // get the details from user meta
    $api->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user',
        'data_id'      => $user_id,
        'meta_key'     => 'has_subscription',
        'meta_value'   => $has_subscription,
    ));

    $api->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user',
        'data_id'      => $user_id,
        'meta_key'     => 'plan_id',
        'meta_value'   => $plan_id,
    ));

    $api->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user',
        'data_id'      => $user_id,
        'meta_key'     => 'card_id',
        'meta_value'   => $card_id,
    ));

    $api->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user',
        'data_id'      => $user_id,
        'meta_key'     => 'subscription_id',
        'meta_value'   => $subscription_id,
    ));

    $api->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user',
        'data_id'      => $user_id,
        'meta_key'     => 'plan_slug',
        'meta_value'   => $plan_slug,
    ));

    $subscription = get_user_subscription();

    return $subscription;

  } else {

    return false;

  }
}





/**
 * Get Plan Label by slug
 */


function get_plan_label($slug = 'smart') {
  if($slug == 'genius') {
    return "Genius";
  } elseif($slug == 'genius_plus') {
    return "Genius Plus";
  } else {
    return "Smart";
  }
}


/**
 * Can use talent pool
 */


function can_use_talent_pool() {
  if(isset($_SESSION['nsauth']['account']) && isset($_SESSION['nsauth']['plan'])) {

    if($_SESSION['nsauth']['account']['id'] > 0) {

      // user is using account

      if($_SESSION['nsauth']['account']['can_see_pool'] == 1) {

        return true;
      } else {
        return false;
      }



    } else {

      if($_SESSION['nsauth']['plan'] == 'smart') {
        return false;
      } else {
        return true;
      }


    }

  } else {
    return false;
  }
}

/**
 * Can indicate capabilities
 */

function can_indicate_capabilities() {
  if(isset($_SESSION['nsauth']['account']) && isset($_SESSION['nsauth']['plan'])) {

    if($_SESSION['nsauth']['account']['id'] > 0) {

      // user is using account

      if($_SESSION['nsauth']['account']['can_see_pool'] == 1) {

        return true;
      } else {
        return false;
      }



    } else {

      if($_SESSION['nsauth']['plan'] == 'smart') {
        return false;
      } else {
        return true;
      }


    }

  } else {
    return false;
  }
}


/**
 * Can Create More Jobs (return: true or false)
 */
function can_create_jobs() {

  @session_start();

  $output = array(
    'can_publish' => false,
    'jobs_left' => 0
    );

  global $api;

  $user_id = aisai::user_logged_in();

  if($user_id > 0) {

    // check user plan

    if(isset($_SESSION['nsauth']['account']['id']) && $_SESSION['nsauth']['account']['id'] > 0
      && ($_SESSION['nsauth']['account']['id'] != $user_id)) {

      # user is using an account

      // now let's check if the user has restriction on creating jobs

      if($_SESSION['nsauth']['account']['can_create_new'] == 1) {

        #user can create jobs

        // now let's check if the account host has more jobs slots left

        $host = $_SESSION['nsauth']['account']['account_recruiter'];

        // if user is a guest of the account, then the recruiter can certainly have up to 10 live jobs.

        $live_jobs = $api->sendRequest(array(

            'action'       => 'read',
            'controller'   => 'job',
            'id'           => '',
            'recruiter_id' => $host, // indicate 0 for showing all jobs and disregard the author
            'multiple'     => true, //****
            'query_string' => '', // search in title ******
            'limit'        => 100, // ***
            'offset'       => 0, // ****
            'sort_by'      => 'date', // date, title
            'order'        => 'desc',
            'status'       => 'live', //all, live, pending, archive
          ));

        $pending_jobs = $api->sendRequest(array(

            'action'       => 'read',
            'controller'   => 'job',
            'id'           => '',
            'recruiter_id' => $host, // indicate 0 for showing all jobs and disregard the author
            'multiple'     => true, //****
            'query_string' => '', // search in title ******
            'limit'        => 100, // ***
            'offset'       => 0, // ****
            'sort_by'      => 'date', // date, title
            'order'        => 'desc',
            'status'       => 'pending', //all, live, pending, archive
          ));

        $total_live_and_pending = $live_jobs->total + $pending_jobs->total;

        if($total_live_and_pending > 9) {
          // limit is reached
            $output = array(
            'can_publish' => false,
            'jobs_left' => 0,
            'jobs_published' => $total_live_and_pending,
            'plan' => $plan
            );
            return $output;

        } else {

            $output = array(
            'can_publish' => true,
            'jobs_left' => (10 - $total_live_and_pending),
            'jobs_published' => $total_live_and_pending,
            'plan' => $plan
            );
            return $output;

        }


      } else {

        # user cannot create new jobs under this account

            $output = array(
            'can_publish' => false,
            'jobs_left' => 0,
            'jobs_published' => 0,
            'plan' => $plan
            );
            return $output;

      }


    } else {

      # user is using his own account

      // if so, let's identify the plan first

      // set default
      $plan = 'smart';

      if(isset($_SESSION['nsauth']['plan'])) {

        if( $_SESSION['nsauth']['plan'] == 'genius' || $_SESSION['nsauth']['plan'] == 'genius_plus')  {
          $plan = $_SESSION['nsauth']['plan'];
        }

      }

      # we already know the plan.

      // Now, let's get the number if live and pending jobs the user has
      $live_jobs = $api->sendRequest(array(

          'action'       => 'read',
          'controller'   => 'job',
          'id'           => '',
          'recruiter_id' => $user_id, // indicate 0 for showing all jobs and disregard the author
          'multiple'     => true, //****
          'query_string' => '', // search in title ******
          'limit'        => 100, // ***
          'offset'       => 0, // ****
          'sort_by'      => 'date', // date, title
          'order'        => 'desc',
          'status'       => 'live', //all, live, pending, archive
        ));

      $pending_jobs = $api->sendRequest(array(

          'action'       => 'read',
          'controller'   => 'job',
          'id'           => '',
          'recruiter_id' => $user_id, // indicate 0 for showing all jobs and disregard the author
          'multiple'     => true, //****
          'query_string' => '', // search in title ******
          'limit'        => 100, // ***
          'offset'       => 0, // ****
          'sort_by'      => 'date', // date, title
          'order'        => 'desc',
          'status'       => 'pending', //all, live, pending, archive
        ));

        $total_live_and_pending = $live_jobs->total + $pending_jobs->total;

        #if plan is smart
        if($plan == 'smart') {
          if(max((1 - $total_live_and_pending),0) == 0) {
            $output = array(
            'can_publish' => false,
            'jobs_left' => 0,
            'jobs_published' => $total_live_and_pending,
            'plan' => $plan
            );
            return $output;
        } else {
            $output = array(
            'can_publish' => true,
            'jobs_left' => max((1 - $total_live_and_pending),0),
            'jobs_published' => $total_live_and_pending,
            'plan' => $plan
            );
            return $output;
        }
      }


        #if plan is genius
        if($plan == 'genius') {
          if(max((1 - $total_live_and_pending),0) == 0) {
            $output = array(
            'can_publish' => false,
            'jobs_left' => 0,
            'jobs_published' => $total_live_and_pending,
            'plan' => $plan
            );
            return $output;
        } else {
            $output = array(
            'can_publish' => true,
            'jobs_left' => max((1 - $total_live_and_pending),0),
            'jobs_published' => $total_live_and_pending,
            'plan' => $plan
            );
            return $output;
        }
      }


        #if plan is genius_plus
        if($plan == 'genius_plus') {
          if(max((10 - $total_live_and_pending),0) == 0) {
            $output = array(
            'can_publish' => false,
            'jobs_left' => 0,
            'jobs_published' => $total_live_and_pending,
            'plan' => $plan
            );
            return $output;
        } else {
            $output = array(
            'can_publish' => true,
            'jobs_left' => (10 - $total_live_and_pending),
            'jobs_published' => $total_live_and_pending,
            'plan' => $plan
            );
            return $output;
          }
        }

    }


  } else {
    $output = array(
    'can_publish' => false,
    'jobs_left' => 0,
    'jobs_published' => 0,
    'plan' => $plan
    );

    return $output;
  }
}


/**
 * getting the value of current profile field
 */

function get_current_profile_value($value) {
	if (isset($_SESSION['my_profile'])) {
		// profile is in the session
		return $_SESSION['my_profile'][$value];
	} else {
		return "Nothing Selected";
	}
}


/**
 * Function is creting <option> 's
 * First argument is the value of textarea, for example textarea from ACF (get_field() function).
 * Second paramenter is optional. It indicated current value and adds "active" mark.
 * Third parameter indicates if current value us an array of current values.
 */
function list_options_from_textarea($textarea_text, $current, $is_array = false, $nothing_selected = true, $prepand = false) {

	$list_array = explode("\n", $textarea_text);
	$output = null;

	if ($prepand != false) {
		array_unshift ($list_array, $prepand);
	}

	foreach($list_array as $item) {

		$item = sanitize_text_field($item);

		$is_selected = '';

		if($is_array) {
			if(in_array($item, (array) $current)) {
				$is_selected = "selected='selected'";
			}
		} else {
			if ($current == $item) {
				$is_selected = "selected='selected'";
			}
		}

		$output .= "<option value='".esc_html(stripslashes($item))."' ".$is_selected.">".esc_html(stripslashes($item))."</option>";

	}


	if ($nothing_selected) {
		$output = "<option value=''>None selected</option>" . $output;
	}

	return $output;
}








function list_options_from_array($input_array, $current, $is_array = false, $nothing_selected = true, $prepand = false) {

  $list_array = $input_array;
  $output = null;

  if ($prepand != false) {
    array_unshift ($list_array, $prepand);
  }

  foreach($list_array as $item) {

    $item = sanitize_text_field($item);

    $is_selected = '';

    if($is_array) {
      if(in_array($item, (array) $current)) {
        $is_selected = "selected='selected'";
      }
    } else {
      if ($current == $item) {
        $is_selected = "selected='selected'";
      }
    }

    $output .= "<option value='".esc_html(stripslashes($item))."' ".$is_selected.">".esc_html(stripslashes($item))."</option>";

  }


  if ($nothing_selected) {
    $output = "<option value=''>None selected</option>" . $output;
  }

  return $output;
}






class aisai {

// checking values befroe outputting to the view
function check_selected($value) {
	if ($value == 'Nothing Selected') {
		return '';
	} else {
		return $value;
	}
}


// checking values before sending via api request
public static function check_isset($value) {

	if(isset($value)) {
		return $value;
	} else {
		return '';
	}

}


// checking if the array has elements, if not then returning an empty array
public static function check_array($value) {

  if(is_array($value) && count($value) > 0) {
    return $value;
  } else {
    $empty = array();
    return $empty;
  }

}


/**
 * Checks if the user is logged in. If yes -> returns user id
 */
public static function user_logged_in() {

	if(isset($_SESSION['nsauth'])) {
		return $_SESSION['nsauth']['user_id'];
	} else {
		return false;
	}

}

/**
 * if user has profile then return id, else -> false
 */
public static function has_profile() {

	if(isset($_SESSION['nsauth'])) {
		if(isset($_SESSION['nsauth']['user_profile']) && $_SESSION['nsauth']['user_profile'] !== 'none') {

			return $_SESSION['nsauth']['user_profile']->id;

		} else {
			return false;
		}

	} else {
		return false;
	}
}

/**
 * returns profie array/object mix
 */

public static function get_profile() {

	if(self::user_logged_in() !== false && self::has_profile() !== false) {

		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
			'action'     => 'read',
			'controller' => 'profile',
			'id'         => self::has_profile(),
		));

		return $result;

	} else {

		return false;

	}

}


/**
 * returns profie array/object mix
 */

public static function get_profile_by_id($id) {

	if(isset($id)) {
$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
		$result = $apicaller->sendRequest(array(
			'action'     => 'read',
			'controller' => 'profile',
			'id'         => $id,
		));

		return $result;

	} else {

		return false;

	}

}




public static function get_number_of_new_messages() {

  if(isset($_SESSION['nsauth']['user_id']) && $_SESSION['nsauth']['user_id'] > 0) {
    $user_id = $_SESSION['nsauth']['user_id'];

    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
    $result = $apicaller->sendRequest(array(
      'action'         => 'read',
      'controller'     => 'message',
      'from_user'      => 0,
      'to_user'        => $user_id,
      'qstring'        => '',
      'subject'        => '',
      'unique_subject' => false,
      'limit'          => 0,
      'offset'         => 0,
      'multi'          => true,
      'read'           => 0, // this is for new messages or 1 for all messages
    ));

    $new = $result->total_unread;
    return $new;
  } else {
    return 0;
  }

  return 0;


}

/**
 * Returns messages
 */

public static function get_messages($from, $to, $query, $subject, $read, $limit, $offset, $sort_by = 'date', $order = 'DESC', $section = 'inbox') {
    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
    $result = $apicaller->sendRequest(array(
      'action'         => 'read',
      'controller'     => 'message',
      'from_user'      => $from,
      'to_user'        => $to,
      'qstring'        => $query,
      'subject'        => $subject,
      'sort_by'        => $sort_by, // subject, talent, date
      'order'          => $order, // ASC, DESC
      'section'        => $section, // inbox or sent. Needed to determine sorting by talent. If we are in sent then we are sorting by sent to user name
      'unique_subject' => true,
      'limit'          => $limit,
      'offset'         => $offset,
      'multi'          => true,
      'read'           => $read, // this is for new messages or 1 for all messages
    ));
  return $result;
}



/**
 * Returns conversation
 */

public static function get_conversation($to, $from, $subject, $limit, $offset) {
    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
    $result = $apicaller->sendRequest(array(

      'action'         => 'read',
      'controller'     => 'message',
      'from_user'      => $from,
      'to_user'        => $to,
      'qstring'        => '',
      'subject'        => $subject,
      'unique_subject' => false,

      'sort_by'        => 'date', // subject, talent, date
      'order'          => 'DESC', // ASC, DESC
      'section'        => 'inbox', // inbox or sent. Needed to determine sorting by talent. If we are in sent then we are sorting by sent to user name

      'load' => 'conversation',

      'limit'          => $limit,
      'offset'         => $offset,
      'multi'          => true,
      'read'           => 1, // this is for new messages or 1 for all messages
      ));
  return $result;
}



/**
 * Tracking profile completion
 */

public static function check($value) {

	if(is_object($value)) {


	$value_arr = (array) $value;

	$result = false;

		foreach($value_arr as $array_item) {
			if($array_item != 'Nothing Selected') {
				$result = true;
			}
			return $result;
		}

	} else {
			if($value !== 'Nothing Selected') {
					return true;
			} else {
				return false;
			}
	}

}



public static function check_against_nothing_selected($value) {

  $value_arr = (array) $value;
  $result = true;
  foreach($value_arr as $array_item) {
    if(sanitize_text_field($array_item) == 'Nothing Selected') {
      $result = false;
    }
    return $result;
  }

}


public static function completed_personal($profile = 'undefined') {

	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(self::check($profile->first_name) && self::check($profile->last_name) && self::check($profile->email_address) ) {

			return true;
		} else {

			return false;
		}

	}

}

function completed_ideal_job($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile != false) {

		// checking for required fields.
		if(  self::check($profile->ideal_job->ideal_job_seniority)
			&& self::check($profile->ideal_job->ideal_job_contract_type)
			&& self::check($profile->ideal_job->ideal_job_basic_salary)
			&& self::check($profile->ideal_job->ideal_job_basic_salary_currency)
			&& self::check($profile->ideal_job->og_pr_ideal_job_company_type)
			&& self::check($profile->ideal_job->og_pr_ideal_job_function)
			&& self::check($profile->ideal_job->og_pr_ideal_job_industry)
			&& self::check($profile->ideal_job->og_pr_ideal_job_location)
			) {

			return true;

		} else {

			return false;
		}

	}
}

function completed_current_job($profile = 'undefined') {

	if($profile == 'undefined') {
		$profile = self::get_profile();
	}


	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->current_job->current_job_organisation_name)
			&& self::check($profile->current_job->current_job_industry)
			&& self::check($profile->current_job->current_job_company_type)
			&& self::check($profile->current_job->current_job_function)
			&& self::check($profile->current_job->current_job_seniority)
			&& self::check($profile->current_job->current_job_year_started)
			) {

			return true;

		} else {

			return false;
		}

	}
}

function completed_experience_one($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_1->previous_exp_organisation_name)
			&& self::check($profile->experience_1->previous_exp_industry)
			&& self::check($profile->experience_1->previous_exp_company_type)
			&& self::check($profile->experience_1->previous_exp_job_function)
			&& self::check($profile->experience_1->previous_exp_seniority)
			&& self::check($profile->experience_1->previous_exp_year_started)
			&& self::check($profile->experience_1->previous_exp_year_ended)
			) {

			return true;

		} else {

			return false;
		}

	}
}


function completed_experience_two($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_2->previous_exp_organisation_name)
			&& self::check($profile->experience_2->previous_exp_industry)
			&& self::check($profile->experience_2->previous_exp_company_type)
			&& self::check($profile->experience_2->previous_exp_job_function)
			&& self::check($profile->experience_2->previous_exp_seniority)
			&& self::check($profile->experience_2->previous_exp_year_started)
			&& self::check($profile->experience_2->previous_exp_year_ended)
			) {

			return true;

		} else {

			return false;
		}

	}
}

function completed_experience_three($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_3->previous_exp_organisation_name)
			&& self::check($profile->experience_3->previous_exp_industry)
			&& self::check($profile->experience_3->previous_exp_company_type)
			&& self::check($profile->experience_3->previous_exp_job_function)
			&& self::check($profile->experience_3->previous_exp_seniority)
			&& self::check($profile->experience_3->previous_exp_year_started)
			&& self::check($profile->experience_3->previous_exp_year_ended)
			) {

			return true;

		} else {

			return false;
		}

	}
}

function completed_experience_four($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_4->previous_exp_organisation_name)
			&& self::check($profile->experience_4->previous_exp_industry)
			&& self::check($profile->experience_4->previous_exp_company_type)
			&& self::check($profile->experience_4->previous_exp_job_function)
			&& self::check($profile->experience_4->previous_exp_seniority)
			&& self::check($profile->experience_4->previous_exp_year_started)
			&& self::check($profile->experience_4->previous_exp_year_ended)
			) {

			return true;

		} else {

			return false;
		}

	}
}

function completed_experience_five($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_5->previous_exp_organisation_name)
			&& self::check($profile->experience_5->previous_exp_industry)
			&& self::check($profile->experience_5->previous_exp_company_type)
			&& self::check($profile->experience_5->previous_exp_job_function)
			&& self::check($profile->experience_5->previous_exp_seniority)
			&& self::check($profile->experience_5->previous_exp_year_started)
			&& self::check($profile->experience_5->previous_exp_year_ended)
			) {

			return true;

		} else {

			return false;
		}

	}
}

function completed_experience_six($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_6->previous_exp_organisation_name)
			&& self::check($profile->experience_6->previous_exp_industry)
			&& self::check($profile->experience_6->previous_exp_company_type)
			&& self::check($profile->experience_6->previous_exp_job_function)
			&& self::check($profile->experience_6->previous_exp_seniority)
			&& self::check($profile->experience_6->previous_exp_year_started)
			&& self::check($profile->experience_6->previous_exp_year_ended)
			) {

			return true;

		} else {

			return false;
		}

	}
}

function completed_experience_seven($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_7->previous_exp_organisation_name)
			&& self::check($profile->experience_7->previous_exp_industry)
			&& self::check($profile->experience_7->previous_exp_company_type)
			&& self::check($profile->experience_7->previous_exp_job_function)
			&& self::check($profile->experience_7->previous_exp_seniority)
			&& self::check($profile->experience_7->previous_exp_year_started)
			&& self::check($profile->experience_7->previous_exp_year_ended)
			) {

			return true;

		} else {

			return false;
		}

	}
}

function completed_education_one($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->education_1->university)
			&& self::check($profile->education_1->course_name)
			&& self::check($profile->education_1->qualification_type)
			&& self::check($profile->education_1->grade_attained)
			&& self::check($profile->education_1->year_attained) ) {

			return true;

		} else {

			return false;
		}

	}
}

function completed_education_two($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->education_2->university)
			&& self::check($profile->education_2->course_name)
			&& self::check($profile->education_2->qualification_type)
			&& self::check($profile->education_2->grade_attained)
			&& self::check($profile->education_2->year_attained) ) {

			return true;

		} else {

			return false;
		}

	}
}

function completed_education_three($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->education_3->university)
			&& self::check($profile->education_3->course_name)
			&& self::check($profile->education_3->qualification_type)
			&& self::check($profile->education_3->grade_attained)
			&& self::check($profile->education_3->year_attained) ) {

			return true;

		} else {

			return false;
		}

	}
}

function completed_skills($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}
	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->skills->specialist_skill_1)
			|| self::check($profile->skills->specialist_skill_2)
			|| self::check($profile->skills->specialist_skill_3) ) {

			return true;

		} else {

			return false;
		}

	}
}


} // end of opengo class





function output_array_pagination($array = array(), $limit = 10, $var = 'page', $radius = 3) {



  #1. determine number of pages

  if(is_array($array)) {
    $array_length = count($array);
  } elseif(is_numeric($array)) {
    $array_length = $array;
  } else {
    return;
  }




  $number_of_pages = ceil($array_length / $limit);

  #2. Get Current page, first and last pages

  $first_page = 1;
  $last_page = $number_of_pages;

  if(isset($_GET[$var]) && $_GET[$var] > 0 && $_GET[$var] <= $number_of_pages) {
    $current_page = $_GET[$var];
  } else {
    $current_page = 1;
  }


  $first_half_gap = $current_page - $first_page;
  if($first_half_gap < 1) {
    $first_half_gap = 0;
  }

  $second_half_gap = $last_page - $current_page;
  if($second_half_gap < 1) {
    $second_half_gap = 0;
  }


  $first_half_array = array();
  $second_half_array = array();

  if($first_half_gap >= $radius) {
    $sentiel = $radius;
    $i = 0;
    while($sentiel > 0) {
      $sentiel = $sentiel - 1;
      $i++;
      array_push($first_half_array, $current_page - $i);
    }

  } else {

    $sentiel = $first_half_gap;
    $i = 0;
    while($sentiel > 0) {
      $sentiel = $sentiel - 1;
      $i++;
      array_push($first_half_array, $current_page - $i);
    }

  }




  if($second_half_gap >= $radius) {
    $sentiel = $radius;
    $i = 0;
    while($sentiel > 0) {
      $sentiel = $sentiel - 1;
      $i++;
      array_push($second_half_array, $current_page + $i);
    }

  } else {

    $sentiel = $second_half_gap;
    $i = 0;
    while($sentiel > 0) {
      $sentiel = $sentiel - 1;
      $i++;
      array_push($second_half_array, $current_page + $i);
    }

  }


  #3. compose url

  $base_url = strtok($_SERVER["REQUEST_URI"],'?');
  $query_string = array();
  parse_str($_SERVER['QUERY_STRING'], $query_string);

  if(isset($query_string[$var])) {
    unset($query_string[$var]);
  }

  $reconstructed_url = $base_url . '?' . http_build_query($query_string);



  # build output

  $output = '';

$output .= '<nav>
  <ul class="pagination">
    <li>
      <a href="'.$reconstructed_url . '&' . $var . '=1' . '" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>';

    $first_half_array = array_reverse($first_half_array);
    foreach ($first_half_array as $pg) {
      $output .= '<li><a href="'.$reconstructed_url . '&' . $var . '=' . $pg . '">'.$pg.'</a></li>';
    }



    $output .= '<li class="active"><a href="'.$reconstructed_url . '&' . $var . '=' . $current_page . '">'.$current_page.'</a></li>';

    foreach ($second_half_array as $pg) {
      $output .= '<li><a href="'.$reconstructed_url . '&' . $var . '=' . $pg . '">'.$pg.'</a></li>';
    }


$output .= '<li>
      <a href="'.$reconstructed_url . '&' . $var . '=' . $last_page . '" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>';




 return $output;

}
