<?php
/**
 * Aisai functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package Aisai
 */

if ( ! function_exists( 'aisai_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function aisai_setup() {
  /*
   * Make theme available for translation.
   * Translations can be filed in the /languages/ directory.
   * If you're building a theme based on Aisai, use a find and replace
   * to change 'aisai' to the name of your theme in all the template files.
   */
  //load_theme_textdomain( 'aisai', get_template_directory() . '/languages' );

  // Add default posts and comments RSS feed links to head.
  //add_theme_support( 'automatic-feed-links' );

  /*
   * Let WordPress manage the document title.
   * By adding theme support, we declare that this theme does not use a
   * hard-coded <title> tag in the document head, and expect WordPress to
   * provide it for us.
   */
  add_theme_support( 'title-tag' );

  /*
   * Enable support for Post Thumbnails on posts and pages.
   *
   * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
   */
  add_theme_support( 'post-thumbnails' );

  // This theme uses wp_nav_menu() in one location.
  register_nav_menus( array(
    'primary' => esc_html__( 'Primary Menu', 'aisai' ),
  ) );

  /*
   * Switch default core markup for search form, comment form, and comments
   * to output valid HTML5.
   */
  add_theme_support( 'html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ) );

}
endif; // aisai_setup
add_action( 'after_setup_theme', 'aisai_setup' );


/**
 * Enqueue scripts and styles.
 */
function aisai_scripts() {

  $scripts_version = 1;

  wp_enqueue_style( 'aisai-style', get_stylesheet_uri() );
  wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.min.js', array('jquery'), $scripts_version, true );
  wp_enqueue_script( 'html5shiv', get_stylesheet_directory_uri() . '/bower_components/html5shiv/dist/html5shiv.min.js', array(), $scripts_version, false );
  wp_enqueue_script( 'modernizr', get_stylesheet_directory_uri() . '/bower_components/modernizr/modernizr.js', array(), $scripts_version, false );

  //summernote  {#Load on demand#}
  wp_enqueue_style( 'summernote', get_stylesheet_directory_uri() . '/bower_components/summernote/dist/summernote.css' );
  wp_enqueue_script( 'summernote', get_stylesheet_directory_uri() . '/bower_components/summernote/dist/summernote.js', array('jquery', 'bootstrap'), $scripts_version, true );

//Angular js  {#Load on demand#}
wp_enqueue_script( 'angular', get_stylesheet_directory_uri() . '/bower_components/angular/angular.js', array('jquery'), $scripts_version, true );
wp_enqueue_script( 'angular-auto-value', get_stylesheet_directory_uri() . '/bower_components/angular/angular-auto-value.min.js', array('angular'), $scripts_version, true );

// angular summernote
wp_enqueue_script( 'angular-summernote', get_stylesheet_directory_uri() . '/bower_components/angular-summernote/dist/angular-summernote.js', array('angular'), $scripts_version, true );

  // bootstrap multiselect  {#Load on demand#}
  wp_enqueue_style( 'bootstrap-multiselect', get_stylesheet_directory_uri() . '/lib/bootstrap-multiselect/bootstrap-multiselect.css' );
  wp_enqueue_script( 'bootstrap-multiselect', get_stylesheet_directory_uri() . '/lib/bootstrap-multiselect/bootstrap-multiselect.js', array('jquery', 'bootstrap'), $scripts_version, true );


  // jquery ui
  wp_enqueue_script( 'jquery-ui', get_stylesheet_directory_uri() . '/bower_components/jquery-ui/jquery-ui.min.js', array('jquery'), $scripts_version, true );


  wp_enqueue_script( 'aisai-main', get_stylesheet_directory_uri() . '/js/main.js', array(), $scripts_version, true );

  wp_enqueue_script( 'aisai-angular', get_stylesheet_directory_uri() . '/js/app.js', array('angular'), $scripts_version, true );


  //custom scrollbar
  wp_enqueue_style( 'scrollbar', get_stylesheet_directory_uri() . '/lib/scrollbar/jquery.mCustomScrollbar.min.css' );
  wp_enqueue_script( 'scrollbar', get_stylesheet_directory_uri() . '/lib/scrollbar/jquery.mCustomScrollbar.concat.min.js', array('jquery'), $scripts_version, true );
}

add_action( 'wp_enqueue_scripts', 'aisai_scripts' );



// Hide admin bar
show_admin_bar( false );

// Limiting access to wo-admin to non admins
add_action( 'init', 'blockusers_init' );

function blockusers_init() {

  if ( is_admin() && ! current_user_can( 'administrator' ) &&
  ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {

    wp_redirect( home_url() );
    exit;

  }
}

// Routing and redirections

// function custom_login(){
//  global $pagenow;
//  if( 'wp-login.php' == $pagenow ) {
//    $site = get_home_url();
//   wp_redirect($site);
//   exit();
//  }
// }

// add_action('init','custom_login');


// nav menus

register_nav_menu( 'home-footer', 'Home Footer' );

register_nav_menu( 'header', 'Header' );

register_nav_menu( 'footer-left', 'Footer Left' );




function aisai_widgets_init() {

  register_sidebar( array(
    'name' => __( 'Footer Sidebar - logged in', 'aisai' ),
    'id' => 'footer-loggedin',
    'description' => __( 'Footer Sidebar for logged in', 'aisai' ),
    'before_widget' => '<div class="widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ) );

  register_sidebar( array(
    'name' =>__( 'Footer Sidebar - logged out', 'aisai'),
    'id' => 'footer-loggedout',
    'description' => __( 'Footer Sidebar for non logged in users', 'aisai' ),
    'before_widget' => '<div class="widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ) );
  }

add_action( 'widgets_init', 'aisai_widgets_init' );





//Options Pages
if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
    'page_title'  => 'Aisai Settings',
    'menu_title'  => 'Manage Aisai',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));


  acf_add_options_sub_page(array(
    'page_title'  => 'Pre-defined list values',
    'menu_title'  => 'Pre-defined lists',
    'parent_slug' => 'theme-general-settings',
  ));


}



function is_current_item($query, $value) {
  if(isset($_GET[$query]) && $_GET[$query] == $value) {
    echo 'active';
  }
}

function if_is_page_echo($page, $string) {
  if(is_page($page)) {
    echo $string;
  }
}




function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );



function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}


// add_action('init', 'myoverride', 100);
// function myoverride() {
//     remove_action('wp_head', array(visual_composer(), 'addMetaData'));
// }

remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

// Include aisai specific libraries

// API caller class

//this class is already decrlared within a plugin - 'nsauth'

//require_once(get_stylesheet_directory() . '/aisai-functions/apicaller.class.php');

require_once(get_stylesheet_directory() . '/aisai-functions/aisai-functions.php');

require_once(get_stylesheet_directory() . '/aisai-functions/aisai-ajax.php');

require_once(get_stylesheet_directory() . '/aisai-functions/redirects.php');

require_once(get_stylesheet_directory() . '/aisai-functions/shortcodes.php');

require_once(get_stylesheet_directory() . '/aisai-functions/switch-account.php');

require_once(get_stylesheet_directory() . '/braintree/nspayments.php');

require_once(get_stylesheet_directory() . '/aisai-functions/parse-webhook.php');

require_once(get_stylesheet_directory() . '/mailchimp/mailchimp.php');

// TRANSFER GET TO SESSTION

if(isset($_GET)) {

  @session_start();

  if(!isset($_SESSION['gts'])) {
    $_SESSION['gts'] = array();
  }

  $current_session_array = $_SESSION['gts'];
  $current_get_array = $_GET;

  $new_session_arr = array_merge($current_session_array, $current_get_array);

  $_SESSION['gts'] = $new_session_arr;


}
