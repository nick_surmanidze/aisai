<?php
/**
 * Template Name: Admin Browse Recruiters (admin)
 * Custom template.
 */
get_header();


// get all available fields
// gloabl api object is instanciated in aisai-functions/aisai-functions.php
global $api;

  $pagenow = 1;
  $limit = 8;

  if(isset($_GET['pg']) && $_GET['pg'] > 0 && is_numeric($_GET['pg'])) {
    $pagenow = $_GET['pg'];
  }

  $output = $api->sendRequest(array(
      'action'       => 'read',
      'controller'   => 'user',

      'multi'        => true, // this is for determining if we are getting one or more users
      'query_handle' => 'role', // can be: 'email', 'login', 'id', subscription_id
      'query_value'  => 'recruiter',
      'show_recruiter_stats' => true,

      'limit'        => $limit, // how many results to output
      'offset'       => $limit * $pagenow - $limit // offset of the first item
    ));


if(is_numeric($output[0]->recruiter_stats->total_number_of_recruiters)) {

  $total = $output[0]->recruiter_stats->total_number_of_recruiters;
} else {
  $total = 0;
}

?>

  <div id="primary" class="content-area" ng-controller="createSearchController">
    <main id="main" class="site-main" role="main">

      <?php
       while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper find-talent">

          <!-- Matched Profiles - Start -->
          <div class="title-section middle-large-section clearfix">

            <div class="user-query-wrapper">
              <div class="row">
              <div class="user-query-title col-sm-12">Registered Recruiters: <?php echo $total; ?></div>
              </div>


              <div class="row">
              <?php

              if(count($output) > 0) {

                foreach($output as $match) { ?>

                <div class="matched-user-card-wrapper col-sm-3">
                  <div class="user-card-inner">
                    <div class="name" style="background: #ECF0F1;"><?php echo $match->user_display_name; ?></div>
                    <div class="id"  style="background: #ECF0F1;">ID # <?php echo $match->id; ?></div>

                    <div class="caption-line">Registration Date</div>
                    <div class="value-line"><?php echo $match->user_registered; ?></div>

                    <div class="caption-line">Email</div>
                    <div class="value-line"><?php echo $match->user_email; ?></div>

                    <div class="caption-line">Membership</div>
                    <div class="value-line"><?php echo check_plan_by_user_id($match->id); ?></div>

                    <div class="caption-line"  style="background: #ECF0F1;">Jobs Posted</div>
                    <div class="value-line"  style="background: #ECF0F1;">
                    T : <?php echo $match->recruiter_stats->total_jobs->total; ?> /
                    A : <?php echo $match->recruiter_stats->archived_jobs->total; ?> /
                    P : <?php echo $match->recruiter_stats->pending_jobs->total; ?> /
                    L : <?php echo $match->recruiter_stats->live_jobs->total; ?>
                    </div>
                  </div>
                </div>


               <?php }



              } else {

                // no matches
              } ?>

            </div>
            <?php
            echo output_array_pagination($total, 8, 'pg', 4);
            ?>

            </div>

          </div>
          <!-- Matched Profiles - END -->

        </div>

      <?php endwhile; // End of the loop. ?>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
