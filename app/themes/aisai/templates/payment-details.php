<?php
/**
 * Template Name: Payment Details
 * Custom template.
 */


$can_continue = true;

if(isset($_GET['change']) &&
   isset($_GET['plan_before']) &&
   isset($_GET['plan_after']) &&
   isset($_GET['type_before']) &&
   isset($_GET['type_after']) ) {


  if($_GET['change'] == "upgrade" || $_GET['change'] == "downgrade" || $_GET['change'] == "none") {
    $change = $_GET['change'];
  } else {
    $can_continue = false;
  }

  if($_GET['plan_before'] == "smart" || $_GET['plan_before'] == "genius" || $_GET['plan_before'] == "genius_plus") {
    $plan_before = $_GET['plan_before'];
  } else {
    $can_continue = false;
  }

  if($_GET['plan_after'] == "smart" || $_GET['plan_after'] == "genius" || $_GET['plan_after'] == "genius_plus") {
    $plan_after = $_GET['plan_after'];
  } else {
    $can_continue = false;
  }

  if($_GET['type_before'] == "recurring" || $_GET['type_before'] == "one-off") {
    $type_before = $_GET['type_before'];
  } else {
    $can_continue = false;
  }

  if($_GET['type_after'] == "recurring" || $_GET['type_after'] == "one-off") {
    $type_after = $_GET['type_after'];
  } else {
    $can_continue = false;
  }

} else {
  $can_continue = false;
}



if($can_continue == true) {

  // we have all the information from the previous page and can continue..

  // identify the price to be paid

  $price = 0;

  // prices used for calculating remaining payment or first payment


  $genius_price = get_option("nspay_genius_single_price");
  $genius_plus_price = get_option("nspay_genius_plus_single_price");


  $procedure_label = '';

  if($plan_before == 'smart' && $plan_after == 'genius') {
    $price = $genius_price;
    $procedure_label = 'You are upgrading from Smart to Genius plan. Changes will take effect right away.';
  }

  if($plan_before == 'smart' && $plan_after == 'genius_plus') {
    $price = $genius_plus_price;
    $procedure_label = 'You are upgrading from Smart to Genius Plus plan. Changes will take effect right away.';
  }

  if($plan_before == 'genius' && $plan_after == 'genius_plus') {
    // upgrading
    $price = get_upgrade_price($genius_price, $genius_plus_price);

    if($type_after == 'recurring') {
      $procedure_label = 'You are upgrading from Genius to Genius Plus plan. Changes will take effect right away. You will be billed for remaining days of current billing cycle. Then you will be billed on the first day of each billing cycle.';
    } else {
      $procedure_label = 'You are upgrading from Genius to Genius Plus plan. Changes will take effect right away. You will be billed for remaining days of current billing cycle.';
    }
  }

  if($plan_before == 'genius' && $plan_after == 'smart') {
    // downgrading
    $price = 0;
    $procedure_label = 'You are downgrading from Genius to Smart plan. Once the current plan expires, if you have more than allowed number of jobs according to your new plan you will be asked to archive them. You can upgrade anytime.';
  }

  if($plan_before == 'genius_plus' && $plan_after == 'smart') {
    // downgrading
    $price = 0;
    $procedure_label = 'You are downgrading from Genius Plus to Smart plan. Once the current plan expires, if you have more than allowed number of jobs according to your new plan you will be asked to archive them. You can upgrade anytime.';
  }

  if($plan_before == 'genius_plus' && $plan_after == 'genius') {
    // downgrading
    $price = 0;
    $procedure_label = 'You are downgrading from Genius Plus to Genius plan. Once the current plan expires, if you have more than allowed number of jobs according to your new plan you will be asked to archive them. You can upgrade anytime.';
  }

}

if (false !== stripos($_SERVER['HTTP_REFERER'], "membership-settings")) {
// Write everything to session and then grab info from the session
$_SESSION['update_membership']['plan_before'] = $plan_before;
$_SESSION['update_membership']['plan_after']  = $plan_after;
$_SESSION['update_membership']['type_before']  = $type_before;
$_SESSION['update_membership']['type_after']  = $type_after;
$_SESSION['update_membership']['price']  = $price;
}




get_header();

?>

  <div id="primary" class="content-area" >
    <main id="main" class="site-main" role="main">
      <?php while ( have_posts() ) : the_post(); ?>

        <?php

        if(isset($_SESSION['update_membership']['plan_after'])) {

          if($_SESSION['update_membership']['plan_after'] == 'smart') {
            update_to_smart();
          } elseif($_SESSION['update_membership']['plan_after'] == 'genius') {
            update_to_genius();
          } elseif($_SESSION['update_membership']['plan_after'] == 'genius_plus') {
            update_to_genius_plus();
          }

        } else { ?>
          <section class="error-404 not-found">
            <div class="page-content 404-content">
              <h1>No Data Provided</h1>
              <h2>Please try again.</h2>
            </div><!-- .page-content -->
          </section><!-- .error-404 -->
        <?php } ?>

      <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
