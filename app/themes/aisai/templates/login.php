<?php
/**
 * Template Name:Login
 * Custom template.
 */
get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper login-page" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/images/login-page-bg.jpg) no-repeat center center; background-size: cover;">
            <div class="login-wrapper">
              <h2>GET STARTED</h2>
              <div class="description">
                Log-in using your social media account to start recruiting using Aisai. We respect your privacy and will never post content to your social feed.
              </div>

              <div class="buttons">
                <?php echo do_shortcode('[nsauth-buttons]'); ?>
              </div>

              <div class="terms">
                By using Aisai I agree to the <a href="/terms">terms of service</a><br/> and <a href="/privacy">privacy policy</a>
              </div>

            </div>
        </div>

      <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>