<?php
/**
 * Template Name: Talent Pool
 * Custom template.
 */
get_header();

$recruiter_id = aisai::user_logged_in();

if(isset($_SESSION['nsauth']['account']['account_recruiter']) && ($_SESSION['nsauth']['account']['account_recruiter'] > 0)) {
  $recruiter_id = $_SESSION['nsauth']['account']['account_recruiter'];
}

$can_see_pool = true;
if(isset($_SESSION['nsauth']['account']['can_see_pool'])) {
  if($_SESSION['nsauth']['account']['can_see_pool'] == 0) {
    $can_see_pool = false;
  }
}

if($recruiter_id == aisai::user_logged_in()) {
  $can_edit_pool = true;
} else {
  $can_edit_pool = false;
}

// get talent pool
$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
$output = $apicaller->sendRequest(array(
  'action'       => 'read',
  'controller'   => 'pool',
  'id'           => '',
  'multi'        => true,
  'recruiter_id' => $recruiter_id,
  'profile_id'   => '',
  'qstring'      => '',
  'order'        => 'desc',
  'order_by'     => 'name',
  'limit'        => 10,
  'offset'       => 0
));


?>

  <div id="primary" class="content-area" ng-controller="createSearchController">
    <main id="main" class="site-main" role="main">
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper talent-pool">
          <div class="middle-large-section clearfix">
            <div class="menu-wrapper">
              <div class="menu">
                <div class="list-group">
                  <a href="/dashboard" class="list-group-item">Job Management</a>
                  <a href="/inbox?show=inbox" class="list-group-item">Inbox
                  <?php $new = aisai::get_number_of_new_messages();
                    if($new > 0) { ?>
                      <span class="badge badge-red"><?php echo $new; ?></span>
                    <?php } ?>
                  </a>
                  <a href="/talent-pool" class="list-group-item active">Talent Pool</a>
                </div>
              </div>
            </div>

            <div class="right-content clearfix">
              <div class="content">
              <?php
              if($can_see_pool == true) {
              if($output->total > 0) { ?>


                <div class="toolbar">
                  <div class="sort">

                    <div class="btn-group btn-group-sm" role="group">
                      <button type="button" class="btn btn-default label-item"><strong>Sort By:</strong></button>
                      <button type="button" class="btn btn-default sorter" data-factor="name"><span>Talent Name</span></button>
                      <button type="button" class="btn btn-default sorter" data-factor="id"><span>Date Added</span></button>

                    </div>

                  </div>
                  <div class="search">

                    <div class="input-group  btn-group-sm">
                      <input type="text" class="form-control" id="search-pool" placeholder="Search for...">
                      <span class="input-group-btn">
                        <button class="btn btn-success" type="button" id="search-pool-trigger"><span class="search-button-label">Search  </span><i class="fa fa-search"></i></button>
                      </span>
                    </div>

                  </div>
                </div>

                <div class="cards-wrapper clearfix">

                  <div class="loader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>

                  <div class="list-group">

                  <?php

                   if($output->current > 0) {

                    foreach ((array) $output->pool as $talent) {

                      if(strlen($talent->current_job_function) > 0 && $talent->current_job_function != 'Nothing Selected') {
                        $job = $talent->current_job_function;
                      } else {
                        $job = 'n/a';
                      }

                      if(strlen($talent->current_job_organisation_name) > 0 && $talent->current_job_organisation_name != 'Nothing Selected') {
                        $company = $talent->current_job_organisation_name;
                      } else {
                        $company = 'n/a';
                      }

                      if((strlen($talent->first_name) > 0) && (strlen($talent->first_name) > 0)) {
                        $name = $talent->first_name . " " . $talent->last_name;
                      } else {
                        $name = $talent->user_display_name;
                      }

                      $tags = '';

                      foreach((array) $talent->tags as $tag) {
                        if(strlen($tag) > 0) {
                          $tags .= "<span class='tag'>" . $tag . "</span>";
                        }
                      }

                      ?>

                        <div  class="list-group-item pool-item pool-<?php echo $talent->id; ?>">
                          <h4 class="list-group-item-heading"><?php echo $name; ?></h4>
                          <div class="buttons">
                            <div class="btn-group btn-group-sm" role="group" aria-label="...">

                              <button type="button" class="btn btn-default show-pool-profile" data-profile-id="<?php echo $talent->profile_id; ?>" data-pool-slug="<?php echo "pool-".$talent->id; ?>"><i class="fa fa-eye"></i> View</button>
                              <?php if($can_edit_pool == true) { ?>
                                <button type="button" class="btn btn-default edit-pool-tags" data-profile-id="<?php echo $talent->profile_id; ?>" data-pool-slug="<?php echo "pool-".$talent->id; ?>"  data-pool-id="<?php echo $talent->id; ?>" data-recruiter-id="<?php echo $recruiter_id; ?>"><i class="fa fa-pencil"></i> Edit</button>
                              <?php } ?>

                            </div>
                          </div>
                          <h5><?php echo $job; ?> at <?php echo $company; ?></h5>
                          <span class="tags-label">Tags:</span><span class='tags-container'><?php echo $tags; ?></span>
                        </div>

                   <?php }

                  } else { ?>


                  <div class="candidate-placeholder">
                  <img src="<?php echo get_stylesheet_directory_uri() . '/images/logo.png'; ?>"><br>
                    Your search did not give results.
                  </div>


                 <?php } ?>

                  </div>


                </div>


                  <div class="load-more-btn-wrapper">
                  <span class='counter'><span class='loaded'><?php echo $output->current; ?></span> out of <span class='total'><?php echo $output->total; ?></span></span>
                  <?php if ($output->total > $output->current) { ?>
                    <button class="btn btn-sm btn-success" id='load-more-pool'>Load More ...</button>
                    <?php } ?>
                  </div>


                 <?php } else { ?>


                  <div class="candidate-placeholder">
                  <img src="<?php echo get_stylesheet_directory_uri() . '/images/logo.png'; ?>"><br>
                    <?php if($can_edit_pool == true) { ?>
                    You don't have talents in your pool.
                    <?php } else { ?>
                     Account does not have any talents in the pool.
                    <?php } ?>
                  </div>


                 <?php } } else { ?>

                  <div class="candidate-placeholder">
                  <img src="<?php echo get_stylesheet_directory_uri() . '/images/logo.png'; ?>"><br>
                    Talent Pool is Closed.
                  </div>

                  <?php } ?>

              </div>
            </div>
          </div>

          <!-- Modal Begin -->
          <div class="modal fade edit_candidate_tags"  tabindex="-1" role="dialog" aria-labelledby="edit_candidate_tags">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="edit_candidate_tags">Add / Edit talent tags</h4>
                </div>
                <div class="tags-modal-body modal-body clearfix">
                  <div class="loader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>
                  <div class="updated clearfix">Your talent pool was updated!</div>

                  <div class="form-group tags-form">
                    <label class="col-sm-12 control-label">Tag</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control" id='tag-one' placeholder="" maxlength="60">
                    </div>

                    <label class="col-sm-12 control-label">Tag</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control" id='tag-two' placeholder="" maxlength="60">
                    </div>

                    <label class="col-sm-12 control-label">Tag</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control" id='tag-three' placeholder="" maxlength="60">
                    </div>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger remove-from-pool">remove</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="update_pool_tags">Save changes</button>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
          <!-- Modal END -->



          <!-- Modal Begin -->
          <div class="modal fade load-talent-profile"  tabindex="-1" role="dialog" aria-labelledby="load-talent-profile">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="load-talent-profile">Talent Details</h4>
                </div>
                <div class="talent-details-modal-body modal-body clearfix">
                  <div class="loader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>
                  <div class="manage-job">
                    <div class="columns-wrapper">
                    <div class="candidate-details-wrapper clearfix">
                      <div id="candaidate-details" class="candidate-details talent-details-wrapper">
                      Loading talent cv.
                      </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
          <!-- Modal END -->


        </div>
      <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
