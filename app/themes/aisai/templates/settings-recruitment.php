<?php
/**
 * Template Name: Recruitment Settings
 * Custom template.
 */
get_header();


if(isset($_SESSION['nsauth']['pipeline'])) {

   $pipeline = array(
  'shortlisted_label'  => $_SESSION['nsauth']['pipeline']->shortlisted_label,
  'phone_screen_label' => $_SESSION['nsauth']['pipeline']->phone_screen_label,
  'interview_label'    => $_SESSION['nsauth']['pipeline']->interview_label,
  'offer_label'        => $_SESSION['nsauth']['pipeline']->offer_label,
  'hired_label'        => $_SESSION['nsauth']['pipeline']->hired_label,
  'rejected_label'     => $_SESSION['nsauth']['pipeline']->rejected_label,
  );

} else {

  $pipeline = array(
    'shortlisted_label'  => 'Shortlisted',
    'phone_screen_label' => 'Phone Screen',
    'interview_label'    => 'Interview',
    'offer_label'        => 'Offer',
    'hired_label'        => 'Hired',
    'rejected_label'     => 'Rejected',
    );
}


?>

  <div id="primary" class="content-area" ng-controller="createSearchController">
    <main id="main" class="site-main" role="main">
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper settings">
          <div class="middle-large-section clearfix">
            <div class="menu-wrapper">
              <div class="menu">
                <div class="list-group">
                  <a href="/settings" class="list-group-item">My Info</a>
                  <?php if(isset($_SESSION['nsauth']['plan']) && $_SESSION['nsauth']['plan'] == 'genius_plus') { ?>
                  <a href="/my-account" class="list-group-item">My Account</a>
                  <?php } ?>
                  <a href="/shared-accounts" class="list-group-item">Shared Accounts</a>
                  <a href="/recruitment-settings" class="list-group-item  active">Recruitment Settings</a>
                  <a href="/membership-settings" class="list-group-item">Membership Settings</a>
                </div>
              </div>
            </div>

            <div class="right-content clearfix">
              <div class="content">

                <div class="cards-wrapper" id="rec_pipeline">

                  <ul class="list-group pipeline">
                    <li class="list-group-item"><strong>Edit How You Manage Your Recruitment Pipeline</strong></li>

                    <li class="list-group-item clearfix">
                      <div class="title">Shortlisted</div>
                      <div class="value"><input type="text" class="form-control" id="shortlisted_label" value="<?php echo $pipeline['shortlisted_label']; ?>" disabled>
                      <div class="error-message">Value should be between 1 and 15 characters long and should contain only alphanumeric values.</div></div>
                    </li>

                    <li class="list-group-item clearfix">
                      <div class="title">Phone Screen</div>
                      <div class="value"><input type="text" class="form-control" id="phone_screen_label" value="<?php echo $pipeline['phone_screen_label']; ?>"><div class="error-message">Value should be between 1 and 15 characters long and should contain only alphanumeric values.</div></div>
                    </li>

                    <li class="list-group-item clearfix">
                      <div class="title">Interview</div>
                      <div class="value"><input type="text" class="form-control" id="interview_label" value="<?php echo $pipeline['interview_label']; ?>"><div class="error-message">Value should be between 1 and 15 characters long and should contain only alphanumeric values.</div></div>
                    </li>

                    <li class="list-group-item clearfix">
                      <div class="title">Offered</div>
                      <div class="value"><input type="text" class="form-control" id="offer_label" value="<?php echo $pipeline['offer_label']; ?>"><div class="error-message">Value should be between 1 and 15 characters long and should contain only alphanumeric values.</div></div>
                    </li>

                    <li class="list-group-item clearfix">
                      <div class="title">Hired</div>
                      <div class="value"><input type="text" class="form-control" id="hired_label" value="<?php echo $pipeline['hired_label']; ?>"><div class="error-message">Value should be between 1 and 15 characters long and should contain only alphanumeric values.</div></div>
                    </li>

                    <li class="list-group-item clearfix">
                      <div class="title">Rejected</div>
                      <div class="value"><input type="text" class="form-control" id="rejected_label" value="<?php echo $pipeline['rejected_label']; ?>" disabled><div class="error-message">Value should be between 1 and 15 characters long and should contain only alphanumeric values.</div></div>
                    </li>

                  </ul>

                </div>


                <div class="btn-wrapper">
                  <span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"> Updating..</span>
                  <button class="btn btn-sm btn-success" id="update_pipeline"><i class="fa fa-refresh"></i> Update</button>
                </div>


              </div>
            </div>
          </div>

        </div>
      <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
