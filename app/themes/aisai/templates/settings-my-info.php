<?php
/**
 * Template Name: Settings My Info
 * Custom template.
 */
get_header();

if(isset($_SESSION['nsauth']['my_info'])) {

   $my_info = array(
  'name'         => $_SESSION['nsauth']['my_info']->name,
  'surname'      => $_SESSION['nsauth']['my_info']->surname,
  'display_name' => $_SESSION['nsauth']['my_info']->display_name,
  'employer_name'=> $_SESSION['nsauth']['my_info']->employer_name,
  'linkedin'     => $_SESSION['nsauth']['my_info']->linkedin,
  'recruiter_email_frequency' => $_SESSION['nsauth']['my_info']->recruiter_email_frequency,
  );

} else {

  $my_info = array(
  'name'         => 'n/a',
  'surname'      => 'n/a',
  'display_name' => 'n/a',
  'employer_name'=> 'n/a',
  'linkedin'     => 'n/a',
  'recruiter_email_frequency' => 'daily',
  );
}

if(strlen($_SESSION['nsauth']['my_info']->display_name) < 1) {
  $my_info['display_name'] = $_SESSION['nsauth']['user_display_name'];
}




?>

  <div id="primary" class="content-area" ng-controller="createSearchController">
    <main id="main" class="site-main" role="main">
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper settings">
          <div class="middle-large-section clearfix">
            <div class="menu-wrapper">
              <div class="menu">
                <div class="list-group">
                  <a href="/settings" class="list-group-item  active">My Info</a>
                  <?php if(isset($_SESSION['nsauth']['plan']) && $_SESSION['nsauth']['plan'] == 'genius_plus') { ?>
                  <a href="/my-account" class="list-group-item">My Account</a>
                  <?php } ?>
                  <a href="/shared-accounts" class="list-group-item">Shared Accounts</a>
                  <a href="/recruitment-settings" class="list-group-item">Recruitment Settings</a>
                  <a href="/membership-settings" class="list-group-item">Membership Settings</a>
                </div>
              </div>
            </div>

            <div class="right-content clearfix">
              <div class="content">

                <div class="cards-wrapper">

                  <form class="form-horizontal" id="my_info">


                    <div class="form-group">
                      <label class="col-sm-4 control-label">Name *</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id='name'  placeholder="Your Name" value="<?php echo $my_info['name']; ?>">
                        <div class="error-message">Value should be between 1 and 40 characters long and should contain only alphanumeric values.</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Surname *</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id='surname' placeholder="Your Surname" value="<?php echo $my_info['surname']; ?>">
                        <div class="error-message">Value should be between 1 and 40 characters long and should contain only alphanumeric values.</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Display Name *</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id='display_name' placeholder="Display Name" value="<?php echo $my_info['display_name']; ?>">
                        <div class="error-message">Value should be between 1 and 40 characters long and should contain only alphanumeric values.</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Employer Name *</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="employer_name" placeholder="Employer Name" value="<?php echo $my_info['employer_name']; ?>">
                        <div class="error-message">Value should be between 1 and 40 characters long and should contain only alphanumeric values.</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Linkedin Profile Url </label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="linkedin" placeholder="http://linkedin.com/linkedinhandle" value="<?php echo $my_info['linkedin']; ?>">
                        <div class="error-message">Please enter a valid linkedin profile url or leave empty.</div>
                      </div>
                    </div>


                    <div class="form-group">
                      <label class="col-sm-4 control-label">Email Frequency </label>
                      <div class="col-sm-8">
                        <select class="multiselect-single" style="visibility:hidden;" title="Email Frequency" id='email-frequency'>
                            <option value="daily" <?php if($my_info['recruiter_email_frequency'] == 'daily') {echo 'selected';}?>>Daily</option>
                            <option value="weekly" <?php if($my_info['recruiter_email_frequency'] == 'weekly') {echo 'selected';}?>>Weekly</option>
                            <option value="none" <?php if($my_info['recruiter_email_frequency'] == 'none') {echo 'selected';}?>>No Emails</option>
                        </select>
                      </div>
                    </div>

                  </form>

                </div>


                <div class="btn-wrapper">
                <span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"> Updating..</span>
                  <button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete-account-modal"><i class="fa fa-trash" ></i> Delete Account</button>
                  <button class="btn btn-sm btn-success" id="update_my_info"><i class="fa fa-refresh" ></i> Update</button>
                </div>


                  <!-- Modal Begin -->
                    <div class="modal fade delete-account-modal" id="delete-account-modal" tabindex="-1" role="dialog" aria-labelledby="delete-account-modal">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="delete-account-modal">Delete my account</h4>
                          </div>
                          <div class="modal-body clearfix">

                            <div class="new-shared-account">
                              Are you sure? You cannot reverse this action.
                            </div>
                            <div class="account-switch-btn">
                              <span class="item-preloader deleting-data-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"> Deleting your data...</span>
                            </div>
                            <div class="account-switch-btn">
                              <button class="btn btn-danger" id="delete_my_account">Delete</button>
                              <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>

                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    <!-- Modal END -->


              </div>
            </div>
          </div>

        </div>
      <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
