<?php
/**
 * Template Name: Create Search
 * Custom template.
 */
get_header();
?>

  <div id="primary" class="content-area" ng-controller="createSearchController">
    <main id="main" class="site-main" role="main">
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper find-talent">

          <div class="title-section middle-large-section clearfix">
            <h2>Find Talent</h2>
            <h4>Tell me about the talent search you need help with</h4>
          </div>

          <div class="about-the-role middle-large-section clearfix">
            <h3>About The Role</h3>
            <div class="inner-area-wrapper">
              <div class="col col-33">
                <div class="info-area">What type of role are your recruiting for?</div>

                <form class="form-horizontal">

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" id="industry_ms" style="visibility:hidden;" multiple="multiple" ng-model="ideal_industry" title="Industry">
                        <?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" id="seniority_ms" style="visibility:hidden;" multiple="multiple" ng-model="ideal_seniority" title="Seniority">
                        <?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" id="function_ms" style="visibility:hidden;" multiple="multiple" ng-model="ideal_job_function" title="Job Function">
                      <?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), "Nothing Selected", false, false); ?>

                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" id="location_ms" style="visibility:hidden;" multiple="multiple" ng-model="ideal_job_location" title="Location">
                        <?php echo list_options_from_textarea(get_field('pdl_ideal_job_location', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" id="pay_ms" style="visibility:hidden;" multiple="multiple" ng-model="ideal_pay" title="Pay">
                       <?php echo list_options_from_textarea(get_field('pdl_salary', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>


                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" id="type_ms" style="visibility:hidden;" multiple="multiple" ng-model="ideal_company_type" title="Company Type">
                        <?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" id="contract_ms" style="visibility:hidden;" multiple="multiple" ng-model="ideal_contract" title="Contract">
                        <?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>
                </form>

              </div>





              <div class="col col-33">
                <div class="info-area">What work experience does the candidate need?</div>

                <form class="form-horizontal">

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="industry_experience" title="Industry Experience">
                        <?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="role_experience" title="Role Experience">
                        <?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="seniority" title="Seniority">
                        <?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="current_pay" title="Current Pay">
                        <?php echo list_options_from_textarea(get_field('pdl_salary', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="contract" title="Contract">
                        <?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>

                <label class="radio-inline radio-label">
                  Currency:
                </label>
                <label class="radio-inline">
                  <input ng-model="currency" name="currency" id="gbp" value="gbp" checked="true" type="radio"> £
                </label>
                <label class="radio-inline">
                  <input ng-model="currency" name="currency" id="usd" value="usd"  type="radio"> $
                </label>
                <label class="radio-inline">
                  <input ng-model="currency" name="currency" id="eur" value="eur" type="radio"> €
                </label>


                </form>

              </div>


              <div class="col col-33">
                <div class="info-area">What qualifications does the candidate need?</div>

                <form class="form-horizontal">

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="qualification_type" title="Qualification Type">
                        <?php echo list_options_from_textarea(get_field('pdl_qualification', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="course_name" title="Course Name">
                        <?php echo list_options_from_textarea(get_field('pdl_course_name', 'option'), "Nothing Selected", false, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="grade_attained" title="Grade Attained">
                      <?php echo list_options_from_textarea(get_field('pdl_grade', 'option'), "Nothing Selected", false, false); ?>
                        <?php //echo list_options_from_array($fields->grade_attained, "Nothing Selected" , true, false); ?>
                      </select>
                    </div>
                  </div>
                </form>

              </div>
            </div>
          </div>

          <div class="job-details middle-large-section clearfix">
            <h3>Job Details</h3>
            <div class="inner-area-wrapper">


              <div class="col col-33">


                <div class="form-group clearfix">
                  <label class="col-sm-12 control-label">Job Title *</label>
                  <div class="col-sm-12"  ng-cloak>
                    <input type="text" class="form-control" placeholder="Job Title" ng-model="job_title" value="">

                    <div class="error-message" ng-cloak ng-show="!is_alpha(job_title) &amp;&amp; job_title.length > 0">This field should not contain special characters.</div>
                    <div class="error-message" ng-cloak ng-show="empty_title &amp;&amp; job_title.length == 0">This field is required.</div>

                  </div>
                </div>

                <div class="form-group clearfix">
                  <label class="col-sm-12 control-label">Employer *</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" placeholder="Employer" ng-model="employer" value="">

                    <div class="error-message" ng-cloak ng-show="!is_alpha(employer) &amp;&amp; employer.length > 0">This field should not contain special characters.</div>
                    <div class="error-message" ng-cloak ng-show="empty_employer &amp;&amp; employer.length == 0">This field is required.</div>

                  </div>
                </div>

              </div>
              <div class="col col-66">

                <div class="form-group clearfix">
                  <label class="col-sm-12 control-label">Description *</label>
                  <div class="col-sm-12">
                    <summernote config="summernoteOptions" ng-cloak ng-model="job_description" ><p><span style="font-weight: bold;color: #808080;">Job Description</span></p><p><br></p><p><br></p><p><br></p><p><span style="font-weight: bold;color: #808080;">Employer Description</span></p><p><br></p><p><br></p><p><br></p><p><span style="font-weight: bold;color: #808080;">Job Benefits</span></p><p><br></p><p><br></p></summernote>

                    <div class="error-message" ng-cloak ng-show="job_description.length > 2500">This field can contain maximu 2500 characters.</div>
                    <div class="error-message" ng-cloak ng-show="empty_description &amp;&amp; job_description.length < 500">This field is required and should contain minimum 500 characters.</div>

                  </div>
                </div>

              </div>
            </div>
          </div>

          <div class="section-white">
            <div class="specific-capabilities middle-large-section clearfix">
              <h3>Bespoke Tests</h3>
              <div class="inner-area-wrapper">
                <div class="col">
                  <div class="info-area"> Bespoke tests are the best way to save time because they automatically reject unqualified candidates. Add up to 10 test questions of your own choice to include even stricter criteria that job applicants have to pass before they get to you.</div>

                  <?php if(can_indicate_capabilities()) { ?>
                  <div class="test-node-container">
                    <div class="test-node">
                      <input type="text" class="form-control capability" placeholder="Enter Capability Required"  value="">
                      <select class="multiselect-single" style="visibility:hidden;"  title="Level Required">
                        <option value="">Level Required</option>
                        <option value="4">Expert</option>
                        <option value="3">Proficient</option>
                        <option value="2">Competent</option>
                        <option value="1">Beginner</option>
                      </select>
                      <button class="btn btn-danger remove-node"><i class="fa fa-times"></i> Remove</button>
                      <div class="error-message">Please enter capability value and indicate required level.</div>
                    </div>
                  </div>

                  <div class="add-more-button-wrapper">
                    <button class="btn btn-info" id="add-more" ng-click="add()" ng-show="testCount < 10"><i class="fa fa-plus"></i> Add More</button> <span class="node-counter" ng-cloak>{{testCount}} out of 10 available</span>
                  </div>

                  <?php } else { ?>
                    <div class='premium-feature-box show-pricing-modal'>Bespoke Tests can be added when you have a Genius and Genius+ plan.<br> You can <a href='/membership-settings'>upgrade your membership here >> </a></div>

                  <?php } ?>

                </div>
              </div>
            </div>
          </div>

          <div class="create-job-wrapper middle-large-section clearfix">
              <button class="btn btn-success" ng-click="create()">Create Search</button>
              <span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"> Updating..</span>
              <span class="item-success-msg"><i class="fa fa-check-circle"></i> Done </span>
              <span class="item-error-msg"><i class="fa fa-times-circle"></i> Failed </span>
          </div>

        </div>
      <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
