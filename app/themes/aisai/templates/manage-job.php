<?php
/**
 * Template Name: Manage Single Job
 * Custom template.
 */
get_header();

// variable for checking if the job is valid
$valid_job = true;

if(isset($_GET['id']) && $_GET['id'] > 0) {

  // check if the job belongs
  global $api;
  $user_id = aisai::user_logged_in();

  if(isset($_SESSION['nsauth']['account']['account_recruiter']) && ($_SESSION['nsauth']['account']['account_recruiter'] > 0)) {
    $user_id = $_SESSION['nsauth']['account']['account_recruiter'];
  }

  $job_details = $api->sendRequest(array(
    'action'       => 'read',
    'controller'   => 'job',
    'id'           => $_GET['id'],
    'read_by_profile_id' => 0
    ));

  if($job_details->job->job_author == $user_id) {

    //continue

    $job = $job_details->job;

    $job->pipeline = $job_details->pipeline;

    // get the pipeline
    if(aisai::user_logged_in() == $user_id) {
      // if account owner is equal to logged in user

      if(isset($_SESSION['nsauth']['pipeline'])) {

         $pipeline = array(
        'shortlisted_label'  => $_SESSION['nsauth']['pipeline']->shortlisted_label,
        'phone_screen_label' => $_SESSION['nsauth']['pipeline']->phone_screen_label,
        'interview_label'    => $_SESSION['nsauth']['pipeline']->interview_label,
        'offer_label'        => $_SESSION['nsauth']['pipeline']->offer_label,
        'hired_label'        => $_SESSION['nsauth']['pipeline']->hired_label,
        'rejected_label'     => $_SESSION['nsauth']['pipeline']->rejected_label,
        );

      } else {

        $pipeline = array(
          'shortlisted_label'  => 'Shortlisted',
          'phone_screen_label' => 'Phone Screen',
          'interview_label'    => 'Interview',
          'offer_label'        => 'Offer',
          'hired_label'        => 'Hired',
          'rejected_label'     => 'Rejected',
          );
      }


    } else {

         $pipeline = array(
        'shortlisted_label'  => $_SESSION['nsauth']['account']['pipeline']['shortlisted_label'],
        'phone_screen_label' => $_SESSION['nsauth']['account']['pipeline']['phone_screen_label'],
        'interview_label'    => $_SESSION['nsauth']['account']['pipeline']['interview_label'],
        'offer_label'        => $_SESSION['nsauth']['account']['pipeline']['offer_label'],
        'hired_label'        => $_SESSION['nsauth']['account']['pipeline']['hired_label'],
        'rejected_label'     => $_SESSION['nsauth']['account']['pipeline']['rejected_label'],
        );


    }

  // Get lists of candidacies: live and rejected with all the additional data
  $candidates = $api->sendRequest(array(
    'action'     => 'read',
    'controller' => 'interest',
    'profile_id' => 0,
    'job_id'     => $_GET['id'],
    'result'      => 'pass',
    'limit' => 10,
    'offset' => 0,
    'show' => 'company',
    ));


} else {
  $valid_job = false;
}

} else {
  $valid_job = false;
}


$can_change_status = true;
if(isset($_SESSION['nsauth']['account']['can_change_status'])) {
  if($_SESSION['nsauth']['account']['can_change_status'] == 0) {
    $can_change_status = false;
  }
}

$can_edit_details = true;
if(isset($_SESSION['nsauth']['account']['can_change_details'])) {
  if($_SESSION['nsauth']['account']['can_change_details'] == 0) {
    $can_edit_details = false;
  }
}

$can_publish = 1;

if(isset($_SESSION['nsauth']['can']['can_publish'])) {
  if($_SESSION['nsauth']['can']['can_publish'] != 1) {
    $can_publish = 0;
  }
}

?>

<div id="primary" class="content-area"  ng-controller="createSearchController">
  <main id="main" class="site-main" role="main">
    <?php while ( have_posts() ) : the_post(); ?>
      <div class="content-wrapper manage-job">
        <div class="middle-large-section clearfix">
          <?php if($valid_job == true) { ?>

          <!-- Job Card - START -->
          <div class="job-card">
            <div class="title-row">
              <span class="title" id="job-title"><?php echo  $job->job_title; ?></span><span class="company-name"><?php echo  $job->company; ?></span>
              <div class="btn-wrapper" data-can="<?php echo $can_publish; ?>">
              <?php if($can_edit_details == true) { ?>
                <div class="loader status-update"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>
                <button class="btn btn-xs btn-info" data-id="<?php echo  $job->id; ?>" id="edit_job_details_trigger"><i class="fa fa-pencil"></i> Edit</button>
                <?php } ?>
                <?php

                if($can_change_status == true) {
                  if($job->job_status == 'archive') {
                    if($can_publish == true) {
                  ?>
                <button class="btn btn-xs btn-success" data-id="<?php echo  $job->id; ?>" id="move_to_live"><i class="fa fa-rocket"></i> Make Live</button>
                <?php } else { ?>
                <a href="/membership-settings/" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="left" title="You have reached your live job limit under this plan."><i class="fa fa-star-o"></i> Upgrade</a>
                 <?php }} else { ?>
                <button class="btn btn-xs btn-success" data-id="<?php echo  $job->id; ?>" id="move_to_archive"><i class="fa fa-archive"></i> Move to Archive</button>
                <?php } }  ?>


              </div>

            </div>
            <div class="columns">

              <div class="col-1-6 shortlisted_cell">
                <div class="line title"><?php echo $pipeline['shortlisted_label']; ?></div>
                <div class="line value"><?php echo $job->pipeline->shortlisted; ?></div>
              </div>

              <div class="col-1-6 phone_screen_cell">
                <div class="line title"><?php echo $pipeline['phone_screen_label']; ?></div>
                <div class="line value"><?php echo $job->pipeline->phone_screen; ?></div>
              </div>

              <div class="col-1-6 interview_cell">
                <div class="line title"><?php echo $pipeline['interview_label']; ?></div>
                <div class="line value"><?php echo $job->pipeline->interview; ?></div>
              </div>

              <div class="col-1-6 offer_cell">
                <div class="line title"><?php echo $pipeline['offer_label']; ?></div>
                <div class="line value"><?php echo $job->pipeline->offer; ?></div>
              </div>

              <div class="col-1-6 hired_cell">
                <div class="line title"><?php echo $pipeline['hired_label']; ?></div>
                <div class="line value"><?php echo $job->pipeline->hired; ?></div>
              </div>

              <div class="col-1-6 rejected_cell">
                <div class="line title"><?php echo $pipeline['rejected_label']; ?></div>
                <div class="line value"><?php echo $job->pipeline->rejected; ?></div>
              </div>

            </div>
            <div class="footer-line">

              <span class="created-on">Created on: <?php echo $job->date_created; ?></span>

              <div class="status-wrapper">
                <span class="title">Status: </span>
                <span class="status"><?php echo  $job->job_status; ?></span>
              </div>

            </div>

          </div>
          <!-- Job Card - END -->

          <!-- Modal Begin -->
          <div class="modal fade edit_job_details"  tabindex="-1" role="dialog" aria-labelledby="edit_job_details">
            <div class="modal-dialog  modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="edit_job_details">Edit Job Details</h4>
                </div>
                <div class="modal-body">
                  <div class="loader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>
                  <div class="updated">Job Details Updated!</div>
                  <summernote config="summernoteOptions"><p><span style="font-weight: bold;color: #808080;">Job Description</span></p><p><br></p><p><br></p><p><br></p><p><span style="font-weight: bold;color: #808080;">Employer Description</span></p><p><br></p><p><br></p><p><br></p><p><span style="font-weight: bold;color: #808080;">Job Benefits</span></p><p><br></p><p><br></p></summernote>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="update_job_details">Save changes</button>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
          <!-- Modal END -->




          <!-- Modal Begin -->
          <div class="modal fade edit_candidate_tags"  tabindex="-1" role="dialog" aria-labelledby="edit_candidate_tags">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="edit_candidate_tags">Add / Edit talent tags</h4>
                </div>
                <div class="tags-modal-body modal-body clearfix">
                  <div class="loader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>
                  <div class="updated">Your talent pool was updated!</div>

                  <div class="form-group tags-form">
                    <label class="col-sm-12 control-label">Tag</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control" id='tag-one' placeholder="" maxlength="60">
                    </div>

                    <label class="col-sm-12 control-label">Tag</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control" id='tag-two' placeholder="" maxlength="60">
                    </div>

                    <label class="col-sm-12 control-label">Tag</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control" id='tag-three' placeholder="" maxlength="60">
                    </div>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="update_tags">Save changes</button>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
          <!-- Modal END -->



          <div class="columns-wrapper">
      <?php if ($candidates->total_live_candidates > 0 || $candidates->total_rejected_candidates > 0 ) { ?>
            <!-- Candidates List - Begin -->
            <div class="candidates-list-wrapper col-25">
              <div class="candidates">
               <!-- Nav tabs -->
               <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#live" aria-controls="live" role="tab" data-toggle="tab">Live Candidates</a></li>
                <li role="presentation"><a href="#rejected" aria-controls="rejected" role="tab" data-toggle="tab">Rejected Candidates</a></li>
              </ul>


              <div class="tab-content candidate-lists">

                <div role="tabpanel" class="tab-pane active" id="live">

                  <div class="list-group" id="live-list">

                  <?php if($candidates->total_live_candidates > 0) {
                      foreach ((array) $candidates->live_candidates as $applicant) {
                      $candidate_details = array(
                        'name_surname' => 'n/a',
                        'job' => '---',
                        'company' => '---',
                        'profile' => 0,
                        'user' => 0
                      );

                      if($applicant->first_name !== "Nothing Selected" && $applicant->last_name != "Nothing Selected") {
                        $candidate_details['name_surname'] = $applicant->first_name . " " . $applicant->last_name;
                      } else {
                        $candidate_details['name_surname'] = $applicant->user_display_name;
                      }

                      if($applicant->current_job_function !== "Nothing Selected") {
                        $candidate_details['job'] = $applicant->current_job_function;
                      }

                      if($applicant->current_job_organisation_name !== "Nothing Selected") {
                        $candidate_details['company'] = $applicant->current_job_organisation_name;
                      }

                      $candidate_details['profile'] = $applicant->profile_id;
                      $candidate_details['user'] = $applicant->user_id;



                      ?>

                      <a href="#" data-profile="<?php echo $candidate_details['profile']; ?>" data-user="<?php echo $candidate_details['user']; ?>" class="list-group-item candidate-card">
                        <h4 class="list-group-item-heading"><?php echo $candidate_details['name_surname']; ?></h4>
                        <p class="list-group-item-text"><?php echo $candidate_details['job']; ?> at <?php echo $candidate_details['company']; ?></p>
                      </a>

                      <?php }
                    } else { ?>

                    <div class="candidate-placeholder">
                      You have 0 live candidates.
                    </div>

                    <?php } ?>

                  </div>

                </div>


                <div role="tabpanel" class="tab-pane" id="rejected">

                  <div class="list-group" id="rejected-list">
                  <?php if($candidates->total_rejected_candidates > 0) {
                      foreach ((array) $candidates->rejected_candidates as $applicant) {
                      $candidate_details = array(
                        'name_surname' => 'n/a',
                        'job' => '---',
                        'company' => '---',
                        'profile' => 0,
                        'user' => 0
                      );

                      if($applicant->first_name !== "Nothing Selected" && $applicant->last_name != "Nothing Selected") {
                        $candidate_details['name_surname'] = $applicant->first_name . " " . $applicant->last_name;
                      } else {
                        $candidate_details['name_surname'] = $applicant->user_display_name;
                      }

                      if($applicant->current_job_function !== "Nothing Selected") {
                        $candidate_details['job'] = $applicant->current_job_function;
                      }

                      if($applicant->current_job_organisation_name !== "Nothing Selected") {
                        $candidate_details['company'] = $applicant->current_job_organisation_name;
                      }

                      $candidate_details['profile'] = $applicant->profile_id;
                      $candidate_details['user'] = $applicant->user_id;



                      ?>

                      <a href="#" data-profile="<?php echo $candidate_details['profile']; ?>" data-user="<?php echo $candidate_details['user']; ?>" class="list-group-item candidate-card">
                        <h4 class="list-group-item-heading"><?php echo $candidate_details['name_surname']; ?></h4>
                        <p class="list-group-item-text"><?php echo $candidate_details['job']; ?> at <?php echo $candidate_details['company']; ?></p>
                      </a>

                      <?php }
                    } else { ?>

                    <div class="candidate-placeholder">
                      You have 0 rejected candidates.
                    </div>

                    <?php } ?>

                  </div>

                </div>

                <div class="load-more-button-wrapper">
                  <?php
                    $button_data = array (
                      'live_loaded'     => count($candidates->live_candidates),
                      'live_total'      => $candidates->total_live_candidates,
                      'rejected_loaded' => count($candidates->rejected_candidates),
                      'rejected_total'  => $candidates->total_rejected_candidates,
                    );
                   ?>


                  <div class="loader loader-horisontal"><img src="<?php echo get_stylesheet_directory_uri() . '/images/loader-horisontal.gif'; ?>"></div>
                  <?php if(($button_data['live_loaded'] < $button_data['live_total']) || ($button_data['rejected_loaded'] < $button_data['rejected_total']) ) { ?>
                  <button class="btn btn-success btn-sm" data-live-loaded="<?php echo $button_data['live_loaded']; ?>" data-live-total="<?php echo $button_data['live_total']; ?>" data-rejected-loaded="<?php echo $button_data['rejected_loaded']; ?>" data-rejected-total="<?php echo $button_data['rejected_total']; ?>" id="load-more-candidates">Load More ...</button>
                  <?php } ?>

                </div>
              <script>
                  (function($){
                      $(window).load(function(){
                          $(".candidate-lists .list-group").mCustomScrollbar({
                              theme:"minimal-dark"
                          });
                           });
                  })(jQuery);
              </script>
              </div>

            </div>
            </div>
            <!-- Candidates List - END -->


            <div class="candidate-details-wrapper col-75">

              <div class="loader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>

              <div id="candaidate-details" class="candidate-details">

              </div>

              <div class="candidate-placeholder">
              <img src="<?php echo get_stylesheet_directory_uri() . '/images/logo.png'; ?>"><br>
                Please select a candidate from the list to show the data.
              </div>


        <?php } else { ?>
                <div class="candidate-placeholder">
                <img src="<?php echo get_stylesheet_directory_uri() . '/images/logo.png'; ?>"><br>
                  This job did not receive any applications yet.
                </div>
        <?php } ?>

            </div>
          </div>



          <?php } else { ?>
          <section class="error-404 not-found">
            <div class="page-content 404-content">
              <h1>ERROR 404</h1>
              <h2>NOT FOUND</h2>
            </div><!-- .page-content -->
          </section><!-- .error-404 -->
          <?php } ?>

        </div>
      </div>



    <?php endwhile; // End of the loop. ?>
  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
