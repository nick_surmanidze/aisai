<?php
/**
 * Template Name: Settings Shared Accounts
 * Custom template.
 */
get_header();

$accounts_i_m_in = get_accounts_i_m_in();

$accounts = array();

if($accounts_i_m_in) {
  $accounts = $accounts_i_m_in;
}

?>

  <div id="primary" class="content-area" ng-controller="createSearchController">
    <main id="main" class="site-main" role="main">
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper settings shared-accounts">
          <div class="middle-large-section clearfix">
            <div class="menu-wrapper">
              <div class="menu">
                <div class="list-group">
                  <a href="/settings" class="list-group-item">My Info</a>
                  <?php if(isset($_SESSION['nsauth']['plan']) && $_SESSION['nsauth']['plan'] == 'genius_plus') { ?>
                  <a href="/my-account" class="list-group-item">My Account</a>
                  <?php } ?>
                  <a href="/shared-accounts" class="list-group-item  active">Shared Accounts</a>
                  <a href="/recruitment-settings" class="list-group-item">Recruitment Settings</a>
                  <a href="/membership-settings" class="list-group-item">Membership Settings</a>
                </div>
              </div>
            </div>

            <div class="right-content clearfix">
              <div class="content">

                <div class="cards-wrapper">

                 <div class="list-group shared-account">

                  <?php if(count($accounts) > 0) {
                    foreach($accounts as $account) { ?>

                   <div  class="list-group-item">
                      <h4 class="list-group-item-heading"><?php echo $account->account_title; ?><span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></span></h4>
                        <button type="button" class="btn btn-danger leave-account" data-account-id="<?php echo $account->id; ?>"><i class="fa fa-sign-out"></i> Leave Account </button>
                    </div>

                    <?php }
                  } else { ?>
                   <div  class="list-group-item">
                      You do not have any shared accounts.
                    </div>
                  <?php } ?>


                 </div>

                </div>


              </div>
            </div>
          </div>

        </div>
      <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
