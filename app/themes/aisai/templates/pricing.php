<?php
/**
 * Template Name:Pricing
 * Custom template.
 */
get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper">
          <div class="full-pricing-table-wrapper middle-large-section clearfix">
            <div class="main-title">Choose your plan</div>
            <div class="pricing-tabs">
              <div class="col-25 description">A better way to hire people for your business starts here. Whether you recruit occasionally or are always hiring for multiple positions, there's an Aisai plan that is right for you.</div>

              <div class="col-75">

                <div class="pricing-table-small">

                <div class="plan smart">
                  <div class="title">
                    SMART
                  </div>
                  <div class="description">
                  Better hiring starts here. All of the tools you need to hire people and save on unnecessary recruitment costs.            
                  </div>
                  <div class="price">
                    FREE
                  </div>
                  <div class="button-wrapper">
                    <a href="/get-started" class="btn btn-primary">SIGN UP</a>
                  </div>
                </div>

                <div class="plan genius">
                  <div class="title">
                    GENIUS
                  </div>
                  <div class="description">
                  Eliminate wasted time by adding bespoke tests so you only see applications that meet your strictest criteria.
                  </div>
                  <div class="price">
                   £89 per month
                  </div>
                  <div class="button-wrapper">
                    <a href="/get-started/?next=membership" class="btn btn-primary">GET GENIUS</a>
                  </div>
                </div>

                <div class="plan genius-plus">
                  <div class="title">
                    GENIUS +
                  </div>
                  <div class="description">
                  Perfect for fast growing businesses and professional recruiters. Run 10 jobs and assign two extra users.
                  </div>
                  <div class="price">
                    £269 per month
                  </div>
                  <div class="button-wrapper">
                    <a href="/get-started/?next=membership" class="btn btn-primary">GET GENIUS+</a>
                  </div>
                </div>


                </div>

              </div>
            </div>
            <div class="full-table">

              <div class="line greyline">
                <div class="col-25 titlerow">Plans</div>
                <div class="col-25 smart">Smart</div>
                <div class="col-25 genius">Genius</div>
                <div class="col-25 genius-plus">Genius+</div>
              </div>

              <div class="line">
                <div class="col-25 titlerow">Active Job Positions</div>
                <div class="col-25 smart">1</div>
                <div class="col-25 genius">1</div>
                <div class="col-25 genius-plus">10</div>
              </div>

	<div class="line greyline">
                <div class="col-25 titlerow">Application tracking</div>
                <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

<div class="line line">
                <div class="col-25 titlerow">Candidate messaging</div>
                <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line greyline">
                <div class="col-25 titlerow">Interest matching</div>
                <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line">
                <div class="col-25 titlerow">Work experience matching</div>
                <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line greyline">
                <div class="col-25 titlerow">Qualifications matching</div>
                <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line">
                <div class="col-25 titlerow">Industry matching</div>
                <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line greyline">
                <div class="col-25 titlerow">Bespoke tests</div>
                <div class="col-25 smart"><i class="fa fa-times"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line">
                <div class="col-25 titlerow">Create applicant talent pool</div>
                <div class="col-25 smart"><i class="fa fa-times"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line greyline">
                <div class="col-25 titlerow">Add two extra users</div>
                <div class="col-25 smart"><i class="fa fa-times"></i></div>
                <div class="col-25 genius"><i class="fa fa-times"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

            </div>
          </div>
          <?php the_content(); ?>
        </div>

      <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>