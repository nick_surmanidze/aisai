<?php
/**
 * Template Name: Membership Settings
 * Custom template.
 */
get_header();


// That's gonna be interesting... Let's begin!

//

// Get list of my payments
// Select last 5 payments

global $api;

$payments = $api->sendRequest(array(
  'action'         => 'read',
  'controller'     => 'payment',
  'account_id'     => '',
  'user_id'        => $user_id
));

$my_current_plan = read_plan();

$paid_plan = $my_current_plan;

$subscription_meta = get_user_subscription();

if($subscription_meta['has_subscription'] == 1) {
  $subscription_slug = 'recurring';
  $paid_plan = $subscription_meta['plan_slug'];
} else {
  $subscription_slug = 'one-off';
}



//////////////////////////////////////////////
// Get current plan details


?>

  <div id="primary" class="content-area" ng-controller="membershipSettingsController">
    <main id="main" class="site-main" role="main">
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper settings">
          <div class="middle-large-section clearfix">
            <div class="menu-wrapper">
              <div class="menu">
                <div class="list-group">
                  <a href="/settings" class="list-group-item">My Info</a>
                  <?php if(isset($_SESSION['nsauth']['plan']) && $_SESSION['nsauth']['plan'] == 'genius_plus') { ?>
                  <a href="/my-account" class="list-group-item">My Account</a>
                  <?php } ?>
                  <a href="/shared-accounts" class="list-group-item">Shared Accounts</a>
                  <a href="/recruitment-settings" class="list-group-item">Recruitment Settings</a>
                  <a href="/membership-settings" class="list-group-item   active">Membership Settings</a>
                </div>
              </div>
            </div>

            <div class="right-content clearfix">
              <div class="content">

                <div class="cards-wrapper update-membership">



                  <H3>Upgrade Your Membership</H3>



                  <div class="form-group">
                    <h5><span class="membership-label">Current Plan:</span> <span class="plan <?php echo $my_current_plan; ?>"><?php echo get_plan_label($my_current_plan); ?></span></h5>
                  </div>




                  <div class="form-group">
                  <?php if($subscription_meta['has_subscription'] == 1) { ?>
                    <h5 class='next-plan-span'><span class="membership-label">Next Plan:</span> <span class="plan <?php echo $subscription_meta['plan_slug']; ?>"><?php echo get_plan_label($subscription_meta['plan_slug']); ?></span></h5>
                  <?php } else { ?>
                    <h5 class='next-plan-span'><span class="membership-label">Next Plan:</span> <span class="plan smart">Smart</span></h5>
                  <?php } ?>
                  </div>



                  <div class="form-group">
                    <span class="switch-to"><span class="membership-label">Switch Plan To:</span> </span>

                    <div class="btn-group btn-group-sm" data-toggle="buttons" id='plan-switch' data-current="<?php echo $paid_plan; ?>">
                      <label class="btn btn-default <?php if($paid_plan == 'smart') {echo 'active'; }?>">
                        <input type="radio" name="plan_slug" value='smart' id="smart" autocomplete="off"  <?php if($paid_plan == 'smart') {echo 'checked'; }?>> Smart
                      </label>
                      <label class="btn btn-default <?php if($paid_plan == 'genius') {echo 'active'; }?>">
                        <input type="radio" name="plan_slug" value='genius' id="genius" autocomplete="off"  <?php if($paid_plan == 'genius') {echo 'checked'; }?>> Genius
                      </label>
                      <label class="btn btn-default  <?php if($paid_plan == 'genius_plus') {echo 'active'; }?>">
                        <input type="radio" name="plan_slug" value='genius_plus' id="genius_plus" autocomplete="off"  <?php if($paid_plan == 'genius_plus') {echo 'checked'; }?>> Genius+
                      </label>
                    </div>
                  </div>



                  <div class="form-group plan-type-selector" <?php if($my_current_plan == 'smart') {echo 'style="display:none"'; }?> >
                    <span class="switch-to"><span class="membership-label">Membership Type: </span></span>

                    <div class="btn-group btn-group-sm" data-toggle="buttons" id='plan-type'  data-current="<?php echo $subscription_slug; ?>">
                      <label class="btn btn-default <?php if($subscription_meta['has_subscription'] == 1) {echo 'active'; }?>">
                        <input type="radio" name="plan_type" value='recurring' id="recurring" autocomplete="off"  <?php if($subscription_meta['has_subscription'] == 1) {echo 'checked'; }?>> Recurring
                      </label>
                      <label class="btn btn-default <?php if($subscription_meta['has_subscription'] == 0) {echo 'active'; }?>">
                        <input type="radio" name="plan_type" value='one-off' id="one-off" autocomplete="off" <?php if($subscription_meta['has_subscription'] == 0) {echo 'checked'; }?>> One-off
                      </label>
                    </div>

                  </div>


                <div class="form-group">
                  <div class='premium-feature-box'>Upgrades will happen immediately. Downgrades will take effect once the current billing cycle ends.</div>
                </div>


                <div class="btn-wrapper">
                  <button class="btn btn-sm btn-success" id="update-membership" disabled='true'><i class="fa fa-chevron-right"></i> Update My Membership</button>
                </div>






                </div>
              </div>
            </div>
          </div>



          <div class="section-white">

            <div class="add-users middle-large-section clearfix">
              <H4>Recent Transactions</H4>
              <?php if (count($payments->payments) > 0) { ?>

              <div class="timeline-wrapper  col-md-12">

                <?php
                $sentiel = 0;
                foreach($payments->payments as $transaction) {
                  if($sentiel < 6) {
                    $sentiel++;
                  ?>

                <div class="item">
                  <div class="timeline">
                    <div class="circle"></div>
                    <div class="line"></div>
                  </div>
                  <div class="item-content">
                    <div class="panel panel-default">
                      <div class="panel-body">
                        <h5 class="list-group-item-heading">
                            <span>Plan : <?PHP echo get_plan_label($transaction->payment_plan); ?> | </span>
                            <span>Amount : <?PHP  echo money_format('%i', $transaction->payment_amount); ?> GBP</span>
                            <br>
                        </h5>
                        <?php $plan_status = '';

                        $payment_date = strtotime($transaction->payment_date);
                        if( $payment_date < strtotime('now -1 month')) {
                          $plan_status = 'Expired';
                        } elseif ( $payment_date > strtotime('now')) {
                          $plan_status = 'Pre-paid';
                        } elseif($payment_date < strtotime('now') && $payment_date > strtotime('now -1 month')) {
                          $plan_status = 'Active';
                        } else {
                          $plan_status = 'Undefined Status';
                        }
                        ?>

                        <span>Status : <?php echo $plan_status; ?></span><br>

                        <?php if($transaction->type == 'one-off') {
                          $transaction_type = 'One off payment.';
                          } elseif ($transaction->type == 'subscription') {
                          $transaction_type = 'Monthly Subscription.';
                          }?>

                        <span>Type : <?php echo $transaction_type; ?></span><br>


                        <?php  if($plan_status == 'Active') { ?>

                        <small>This is your active membership plan. It started on <strong><?php echo date('jS F, Y',$payment_date); ?></strong> and expires on <strong><?php echo date('jS F, Y',strtotime($transaction->payment_date . '+1 month')); ?></strong></small>

                        <?php } elseif($plan_status == 'Expired') { ?>
                        <small>Expired on <strong><?php echo date('jS F, Y',strtotime($transaction->payment_date . '+1 month')); ?>.</strong></small>
                        <?php  } elseif($plan_status == 'Pre-paid') { ?>

                        <small>This is your advance payment for next billing period. It will get active once your current billing cycle is over. It will get active on <strong><?php echo date('jS F, Y',strtotime($transaction->payment_date)); ?></strong> and will be valid during that billing cycle, which ends on <strong><?php echo date('jS F, Y',strtotime($transaction->payment_date . '+1 month')); ?></strong>.</small>

                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>

                <?php }} ?>

              </div>

              <?php } else { ?>

                <div class="nothing-found">You have no payments.</div>

              <?php } ?>


            </div>
          </div>


          <div class="price-plan middle-large-section full-pricing-table-wrapper clearfix">


<div class="full-table">

              <div class="line greyline">
                <div class="col-25 titlerow">Plans</div>
                <div class="col-25 smart">Smart</div>
                <div class="col-25 genius">Genius</div>
                <div class="col-25 genius-plus">Genius+</div>
              </div>

              <div class="line">
                <div class="col-25 titlerow">Active Job Positions</div>
                <div class="col-25 smart">1</div>
                <div class="col-25 genius">1</div>
                <div class="col-25 genius-plus">10</div>
              </div>

	<div class="line greyline">
                <div class="col-25 titlerow">Application tracking</div>
                <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

<div class="line line">
                <div class="col-25 titlerow">Candidate messaging</div>
                <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line greyline">
                <div class="col-25 titlerow">Interest matching</div>
                <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line">
                <div class="col-25 titlerow">Work experience matching</div>
                <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line greyline">
                <div class="col-25 titlerow">Qualifications matching</div>
                <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line">
                <div class="col-25 titlerow">Industry matching</div>
                <div class="col-25 smart"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line greyline">
                <div class="col-25 titlerow">Bespoke tests</div>
                <div class="col-25 smart"><i class="fa fa-times"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line">
                <div class="col-25 titlerow">Create applicant talent pool</div>
                <div class="col-25 smart"><i class="fa fa-times"></i></div>
                <div class="col-25 genius"><i class="fa fa-check-circle"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>

              <div class="line greyline">
                <div class="col-25 titlerow">Add two extra users</div>
                <div class="col-25 smart"><i class="fa fa-times"></i></div>
                <div class="col-25 genius"><i class="fa fa-times"></i></div>
                <div class="col-25 genius-plus"><i class="fa fa-check-circle"></i></div>
              </div>


           

            </div>


          </div>


        </div>
      <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
