<?php
/**
 * Template Name: Settings My Account
 * Custom template.
 */
get_header();




// get user account

global $api;

$user_id = aisai::user_logged_in();

$result = $api->sendRequest(array(
  'action'         => 'read',
  'controller'     => 'account',
  'read_by'        => 'recruiter_id', // id, recruiter_id, guest_id
  'recruiter_id'   => $user_id
));


if($result->accounts[0]->id > 0) {

  $account_data = array(
  'account_name'       => $result->accounts[0]->account_title,
  'account_id'         => $result->accounts[0]->id,
  'can_change_status'  => $result->accounts[0]->can_change_status,
  'can_see_pool'       => $result->accounts[0]->can_see_pool,
  'can_change_details' => $result->accounts[0]->can_change_details,
  'can_create_new'     => $result->accounts[0]->can_create_new,
  'guests'             => $result->accounts[0]->guests,
  );

} else {
  $account_data = array(
  'account_name'       => $_SESSION['nsauth']['my_info']->employer_name,
  'account_id'         => 0,
  'can_change_status'  => 1,
  'can_see_pool'       => 1,
  'can_change_details' => 1,
  'can_create_new'     => 1,
  'guests'             => array(''),
  );
}


$pending_invitation = $api->sendRequest(array(
    'action'       => 'read',
    'controller'   => 'meta',
    'data_type'    => 'user',
    'data_id'      => $user_id,
    'meta_key'     => 'invited_tokens'
  ));

if(strlen($pending_invitation) > 0 && count(unserialize($pending_invitation)) > 0) {
  $pending_invitation = unserialize($pending_invitation);
} else {
  $pending_invitation = array();
}


?>

  <div id="primary" class="content-area"  ng-controller="membershipSettingsController">
    <main id="main" class="site-main" role="main">
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper settings">


          <div class="middle-large-section clearfix">
            <div class="menu-wrapper">
              <div class="menu">
                <div class="list-group">
                  <a href="/settings" class="list-group-item">My Info</a>
                  <?php if(isset($_SESSION['nsauth']['plan']) && $_SESSION['nsauth']['plan'] == 'genius_plus') { ?>
                  <a href="/my-account" class="list-group-item   active">My Account</a>
                  <?php } ?>
                  <a href="/shared-accounts" class="list-group-item">Shared Accounts</a>
                  <a href="/recruitment-settings" class="list-group-item">Recruitment Settings</a>
                  <a href="/membership-settings" class="list-group-item">Membership Settings</a>
                 </div>
               </div>
             </div>

            <div class="right-content my-account-settings clearfix">
              <div class="content">

                <div class="cards-wrapper">

                  <form class="form-horizontal" id="my_account">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Account Name: </label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="account_name" placeholder="Account Name" value="<?php echo $account_data['account_name']; ?>">
                        <div class="error-message">Account name should be from 3 to 20 characters long. Preferably your name or your company name.</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Can View Talent Pool: </label>
                      <div class="col-sm-8">

                        <div class="btn-group btn-group-sm bool-toggle" data-toggle="buttons">
                          <label class="btn btn-default <?php if($account_data['can_see_pool'] == 1) { echo 'active'; } ?>">
                            <input value='1' type="radio" name="can-view-pool" autocomplete="off" <?php if($account_data['can_see_pool'] == 1) { echo 'checked'; } ?>> Yes
                          </label>
                          <label class="btn btn-default <?php if($account_data['can_see_pool'] == 0) { echo 'active'; } ?>">
                            <input value='0' type="radio" name="can-view-pool" autocomplete="off" <?php if($account_data['can_see_pool'] == 0) { echo 'checked'; } ?>> No
                          </label>
                        </div>

                      </div>
                    </div>


                    <div class="form-group">
                      <label class="col-sm-4 control-label">Can Change Job Status: </label>
                      <div class="col-sm-8">

                        <div class="btn-group btn-group-sm bool-toggle" data-toggle="buttons">
                          <label class="btn btn-default <?php if($account_data['can_change_status'] == 1) { echo 'active'; } ?>">
                            <input value='1' type="radio" name="can-change-status" autocomplete="off" <?php if($account_data['can_change_status'] == 1) { echo 'checked'; } ?>> Yes
                          </label>
                          <label class="btn btn-default <?php if($account_data['can_change_status'] == 0) { echo 'active'; } ?>">
                            <input value='0' type="radio" name="can-change-status" autocomplete="off" <?php if($account_data['can_change_status'] == 0) { echo 'checked'; } ?>> No
                          </label>
                        </div>

                      </div>
                    </div>


                    <div class="form-group">
                      <label class="col-sm-4 control-label">Can Create New Jobs: </label>
                      <div class="col-sm-8">

                        <div class="btn-group btn-group-sm bool-toggle" data-toggle="buttons">
                          <label class="btn btn-default <?php if($account_data['can_create_new'] == 1) { echo 'active'; } ?>">
                            <input value='1' type="radio" name="can-create-new" autocomplete="off" <?php if($account_data['can_create_new'] == 1) { echo 'checked'; } ?>> Yes
                          </label>
                          <label class="btn btn-default <?php if($account_data['can_create_new'] == 0) { echo 'active'; } ?>">
                            <input value='0' type="radio" name="can-create-new" autocomplete="off" <?php if($account_data['can_create_new'] == 0) { echo 'checked'; } ?>> No
                          </label>
                        </div>

                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Can Change Job Details: </label>
                      <div class="col-sm-8">

                        <div class="btn-group btn-group-sm bool-toggle" data-toggle="buttons">
                          <label class="btn btn-default <?php if($account_data['can_change_details'] == 1) { echo 'active'; } ?>">
                            <input value='1' type="radio" name="can-change-details" autocomplete="off" <?php if($account_data['can_change_details'] == 1) { echo 'checked'; } ?>> Yes
                          </label>
                          <label class="btn btn-default <?php if($account_data['can_change_details'] == 0) { echo 'active'; } ?>">
                            <input value='0' type="radio" name="can-change-details" autocomplete="off" <?php if($account_data['can_change_details'] == 0) { echo 'checked'; } ?>> No
                          </label>
                        </div>

                      </div>
                    </div>



                  </form>

                </div>





              </div>
            </div>

          </div>

          <div class="section-white">
            <div class="add-users middle-large-section clearfix">

            <div class="col-50 add-users-description">

              <div class="alert alert-info" role="alert">
                The naming committee executes on priorities. An innovativeness fosters scalings in the marketplace, while the standard-setters stay ahead. There can be no measured increase in margins until we can achieve a breakout gain in task efficiency. On-message onboarding solutions conservatively empower the innovators, while supply-chains enforce an inter-company trigger event. A pyramid quickly promotes the Chief Visionary Officer, while the naming committee culturally innovates our actionable client needs. Our results-oriented feedbacks invigorate the partners.
              </div>
            </div>
            <div class="col-50">
              <h4>Share account with users:</h4>

              <?php if(count($pending_invitation) > 0) { ?>
                  <h5>You have <?php echo count($pending_invitation); ?> pending invitation(s).</h5>
              <?php } ?>

              <div class="user-node-container">
              <?php if(count((array)$account_data['guests']) > 0) {
                foreach((array) $account_data['guests'] as $guest) { ?>

                <div class="user-node">
                  <input type="text" class="form-control user-email" placeholder="user email address" value="<?php echo $guest->user->user_login; ?>">
                  <button class="btn btn-danger remove-node"><i class="fa fa-times"></i> Remove</button>
                </div>

               <?php }

                } ?>


              </div>

              <div class="add-more-button-wrapper">
                <button class="btn btn-info" id="add-more" ng-click="add()" ng-show="userCount < 2"><i class="fa fa-plus"></i> Add User</button> <span class="node-counter" ng-cloak>{{userCount}} out of 2 users</span>
              </div>

              </div>

            </div>

          </div>

          <div class="middle-large-section clearfix">
            <div class="btn-wrapper">
              <span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"> Updating..</span>
              <button class="btn btn-sm btn-success" id="update_my_account"><i class="fa fa-refresh" ></i> Update</button>
            </div>
          </div>

        </div>
      <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
