<?php
/**
 * Template Name: Admin Manage Job (admin)
 * Custom template.
 */
get_header();


// get all available fields
// gloabl api object is instanciated in aisai-functions/aisai-functions.php
global $api;

if(isset($_GET['id']) && $_GET['id'] > 0) {


  $pagenow = 1;
  $limit = 12;

  if(isset($_GET['pg']) && $_GET['pg'] > 0 && is_numeric($_GET['pg'])) {
    $pagenow = $_GET['pg'];
  }

  $output = $api->sendRequest(array(
    'action'             => 'read',
    'controller'         => 'job',
    'mark_read'          => false, // or jobs
    'id'                 => $_GET['id'],
    'read_by_profile_id' => 0, // profile id goes here
    'admin_page'         => true,  // if admin_page is true than request comes from admin page and should output some additonal params
    'match_offset'       => $limit * $pagenow - $limit,
    'match_limit'        => $limit,
    ));


  if($output->job->id > 0) {

    $load = true;
    $job = $output->job;
    $job->pipeline = $output->pipeline;

  } else {
    $load = false;
  }

} else {
  $load = false;
}


$pipeline = array(
  'shortlisted_label'  => 'Shortlisted',
  'phone_screen_label' => 'Phone Screen',
  'interview_label'    => 'Interview',
  'offer_label'        => 'Offer',
  'hired_label'        => 'Hired',
  'rejected_label'     => 'Rejected',
  );

$url_array = array();

$url_array['id'] = $_GET['id'];
$url_array['signup-for-job'] = $_GET['id'];


if(aisai::check_against_nothing_selected($job->og_job_query_industry)) {
  $url_array['industry'] = (array) $job->og_job_query_industry;
}
if(aisai::check_against_nothing_selected($job->og_job_query_seniority)) {
  $url_array['seniority'] = (array) $job->og_job_query_seniority;
}
if(aisai::check_against_nothing_selected($job->og_job_query_function)) {
  $url_array['function'] = (array) $job->og_job_query_function;
}
if(aisai::check_against_nothing_selected($job->og_job_query_location)) {
  $url_array['location'] = (array) $job->og_job_query_location;
}
if(aisai::check_against_nothing_selected($job->og_job_query_pay)) {
  $url_array['pay'] = (array) $job->og_job_query_pay;
}
if(aisai::check_against_nothing_selected($job->og_job_query_currency)) {
  $url_array['cur'] = (array) $job->og_job_query_currency;
}
if(aisai::check_against_nothing_selected($job->og_job_query_company_type)) {
  $url_array['company-type'] = (array) $job->og_job_query_company_type;
}
if(aisai::check_against_nothing_selected($job->og_job_query_contract)) {
  $url_array['contract'] = (array) $job->og_job_query_contract;
}

$url_params = http_build_query($url_array);

$membership = check_plan_by_user_id($job->job_author);

  $live_jobs = $api->sendRequest(array(
    'action'       => 'read',
    'controller'   => 'job',
    'id'           => '',
    'recruiter_id' => $job->job_author,
    'multiple'     => true,
    'query_string' => '',
    'limit'        => 100,
    'offset'       => 0,
    'sort_by'      => 'date', // date, title
    'order'        => 'desc',
    'status'       => 'live', //all, live, pending, archive
  ));

  $pending_jobs = $api->sendRequest(array(
    'action'       => 'read',
    'controller'   => 'job',
    'id'           => '',
    'recruiter_id' => $job->job_author,
    'multiple'     => true,
    'query_string' => '',
    'limit'        => 100,
    'offset'       => 0,
    'sort_by'      => 'date', // date, title
    'order'        => 'desc',
    'status'       => 'pending', //all, live, pending, archive
  ));

  $archived_jobs = $api->sendRequest(array(
    'action'       => 'read',
    'controller'   => 'job',
    'id'           => '',
    'recruiter_id' => $job->job_author,
    'multiple'     => true,
    'query_string' => '',
    'limit'        => 100,
    'offset'       => 0,
    'sort_by'      => 'date', // date, title
    'order'        => 'desc',
    'status'       => 'archive', //all, live, pending, archive
  ));

?>

  <div id="primary" class="content-area" ng-controller="createSearchController">
    <main id="main" class="site-main" role="main">

      <?php

      if($load == true) {
       while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper find-talent">

          <div class="title-section middle-large-section clearfix">

                  <!-- Job Card - START -->
                    <div class="job-card">
                      <div class="title-row" data-job-id="<?php echo $output->job->id; ?>">
                        <span class="title"><?php echo  $job->job_title; ?></span><span class="company-name"><?php echo  $job->company; ?></span>
                        <div class="btn-wrapper">

                        </div>

                      </div>
                      <div class="columns">

                          <div class="col-1-6">
                            <div class="line title"><?php echo $pipeline['shortlisted_label']; ?></div>
                            <div class="line value"><?php echo $job->pipeline->shortlisted; ?></div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title"><?php echo $pipeline['phone_screen_label']; ?></div>
                            <div class="line value"><?php echo $job->pipeline->phone_screen; ?></div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title"><?php echo $pipeline['interview_label']; ?></div>
                            <div class="line value"><?php echo $job->pipeline->interview; ?></div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title"><?php echo $pipeline['offer_label']; ?></div>
                            <div class="line value"><?php echo $job->pipeline->offer; ?></div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title"><?php echo $pipeline['hired_label']; ?></div>
                            <div class="line value"><?php echo $job->pipeline->hired; ?></div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title"><?php echo $pipeline['rejected_label']; ?></div>
                            <div class="line value"><?php echo $job->pipeline->rejected; ?></div>
                          </div>

                      </div>
                      <div class="footer-line">

                      <span class="created-on">Created on: <?php echo $job->date_created; ?></span>
                      <span class="admin-info"> Matches: <?php echo $output->total_matches->profiles; ?></span>

                      <div class="status-wrapper">
                        <span class="title">Status: </span>
                        <span class="status"><?php echo  $job->job_status; ?></span>
                      </div>

                      </div>

                    </div>
                  <!-- Job Card - END -->

          </div>






          <!-- Matched Profiles - Start -->
          <div class="title-section middle-large-section clearfix">

            <div class="user-query-wrapper">
              <div class="row">
              <div class="user-query-title col-sm-12">Matched Profiles</div>
              </div>


              <div class="row">
              <?php

              if($output->total_matches->profiles > 0) {

                foreach($output->matched_profiles as $match) { ?>

                <div class="matched-user-card-wrapper col-sm-3">
                  <div class="user-card-inner">
                    <div class="name"><?php echo $match->user_name; ?></div>
                    <div class="id">User ID: <?php echo $match->user_id; ?> / Profile ID: <?php echo $match->profile_id; ?></div>
                    <div class="button-wrapper">
                      <button class="btn btn-info btn-sm admin-view-profile-button" data-profile-id="<?php echo $match->profile_id; ?>" >View Profile</button>
                    </div>
                  </div>
                </div>


               <?php }



              } else {

                // no matches
              } ?>


            </div>
            <?php

            echo output_array_pagination($output->total_matches->profiles, 12, 'pg', 4);

            ?>

            </div>







          </div>
          <!-- Matched Profiles - END -->











          <div class="about-the-role middle-large-section clearfix">
            <h3>About The Role (read only access)</h3>
            <div class="inner-area-wrapper">
              <div class="col col-33">
                <div class="info-area">What type of role are your recruiting for?</div>

                <form class="form-horizontal">

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" style="visibility:hidden;" multiple="multiple" ng-model="ideal_industry" title="Industry">
                        <?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), (array) $job->og_job_query_industry, true, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" style="visibility:hidden;" multiple="multiple" ng-model="ideal_seniority" title="Seniority">
                        <?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), (array) $job->og_job_query_seniority, true, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" style="visibility:hidden;" multiple="multiple" ng-model="ideal_job_function" title="Job Function">
                        <?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), (array) $job->og_job_query_function, true, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" style="visibility:hidden;" multiple="multiple" ng-model="ideal_job_location" title="Location">
                        <?php echo list_options_from_textarea(get_field('pdl_ideal_job_location', 'option'), (array) $job->og_job_query_location, true, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" style="visibility:hidden;" multiple="multiple" ng-model="ideal_pay" title="Pay">
                       <?php echo list_options_from_textarea(get_field('pdl_salary', 'option'), (array) $job->og_job_query_pay, true, false); ?>
                      </select>
                    </div>
                  </div>


                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" style="visibility:hidden;" multiple="multiple" ng-model="ideal_company_type" title="Company Type">
                        <?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), (array) $job->og_job_query_company_type, true, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect-limit-three" style="visibility:hidden;" multiple="multiple" ng-model="ideal_contract" title="Contract">
                        <?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), (array) $job->og_job_query_contract, true, false); ?>
                      </select>
                    </div>
                  </div>
                </form>

              </div>





              <div class="col col-33">
                <div class="info-area">What work experience does the candidate need?</div>

                <form class="form-horizontal">

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="industry_experience" title="Industry Experience">
                        <?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), (array) $job->og_job_query_industry_experience, true, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="role_experience" title="Role Experience">
                        <?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), (array) $job->og_job_query_role_experience, true, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="seniority" title="Seniority">
                        <?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), (array) $job->og_job_query_seniority_experience, true, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="current_pay" title="Current Pay">
                        <?php echo list_options_from_textarea(get_field('pdl_salary', 'option'), (array) $job->og_job_query_current_pay, true, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="contract" title="Contract">
                        <?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), (array) $job->og_job_query_current_contract, true, false); ?>
                      </select>
                    </div>
                  </div>

                <label class="radio-inline radio-label">
                  Currency:
                </label>
                <label class="radio-inline">
                  <input ng-model="currency" name="currency" id="gbp" value="gbp"  type="radio" <?php if($job->og_job_query_currency[0] == 'gbp') {echo 'selected';}?>> £
                </label>
                <label class="radio-inline">
                  <input ng-model="currency" name="currency" id="usd" value="usd"  type="radio" <?php if($job->og_job_query_currency[0] == 'usd') {echo 'selected';}?>> $
                </label>
                <label class="radio-inline">
                  <input ng-model="currency" name="currency" id="eur" value="eur" type="radio" <?php if($job->og_job_query_currency[0] == 'eur') {echo 'selected';}?>> €
                </label>


                </form>

              </div>


              <div class="col col-33">
                <div class="info-area">What qualifications does the candidate need?</div>

                <form class="form-horizontal">

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="qualification_type" title="Qualification Type">
                        <?php echo list_options_from_textarea(get_field('pdl_qualification', 'option'), (array) $job->og_job_query_qualification_type, true, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="course_name" title="Course Name">
                        <?php echo list_options_from_textarea(get_field('pdl_course_name', 'option'), (array) $job->og_job_query_course_name, true, false); ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12 multiselect-wrapper">
                      <select class="multiselect" style="visibility:hidden;" multiple="multiple" ng-model="grade_attained" title="Grade Attained">
                        <?php echo list_options_from_textarea(get_field('pdl_grade', 'option'), (array) $job->og_job_query_grade_attained, true, false); ?>
                      </select>
                    </div>
                  </div>
                </form>

              </div>
            </div>
          </div>

          <div class="job-details middle-large-section clearfix">
            <h3>Job Details</h3>
            <div class="inner-area-wrapper">


              <div class="col col-33">


                <div class="form-group clearfix">
                  <label class="col-sm-12 control-label">Job Title *</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" placeholder="Job Title" ng-model="job_title" id='job-title' value="<?php echo $job->job_title; ?>">
                  </div>
                </div>

                <div class="form-group clearfix">
                  <label class="col-sm-12 control-label">Employer *</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" placeholder="Employer" ng-model="employer" id='company' value="<?php echo $job->company; ?>">
                  </div>
                </div>


                <h4>Recruiter Info</h4>

                <h5>ID : <span class="value"><?php echo $output->recruiter->id; ?></span></h5>
                <h5>Name : <span class="value"><?php echo $output->recruiter->user_display_name; ?></span></h5>
                <h5>Email : <span class="value"><?php echo $output->recruiter->user_email; ?></span></h5>
                <h5>Registration date : <span class="value"><?php echo $output->recruiter->user_registered; ?></span></h5>
                <h5>Membership : <span class="value"><?php echo $membership; ?></span></h5>
                <h5>Live Jobs : <span class="value"><?php echo $live_jobs->total; ?></span></h5>
                <h5>Pending Jobs : <span class="value"><?php echo $pending_jobs->total; ?></span></h5>
                <h5>Archived Jobs : <span class="value"><?php echo $archived_jobs->total; ?></span></h5>
                <h5>Job URL : <span class="value" style="word-wrap: break-word;">http://opengoglobal.com/search/?<?php echo $url_params; ?></span></h5>

              </div>
              <div class="col col-66">

                <div class="form-group clearfix">
                  <label class="col-sm-12 control-label">Description *</label>
                  <div class="col-sm-12">
                    <summernote config="summernoteOptions" ng-cloak ng-model="job_description" id="job-description"><?php echo stripslashes($job->job_description); ?></summernote>
                  </div>
                </div>

              </div>
            </div>


          </div>

          <div class="section-white">
            <div class="specific-capabilities middle-large-section clearfix">
              <h3>Specific Capabilities</h3>
              <div class="inner-area-wrapper">
                <div class="col">
                  <div class="test-node-container">

  <?php if(count((array) $job->og_job_test_requirements) > 0) {

    foreach((array) $job->og_job_test_requirements as $test) { ?>

                    <div class="test-node">
                      <input type="text" class="form-control capability" placeholder="Enter Capability Required" value="<?php echo $test[0]; ?>" data-capability-id='<?php echo $test[2]; ?>'>
                      <select class="multiselect-single" style="visibility:hidden;" title="Level Required" disabled>
                        <option value="">Level Required</option>
                        <option value="4" <?php if($test[1] == 4) {echo 'selected';}?>>Expert</option>
                        <option value="3" <?php if($test[1] == 3) {echo 'selected';}?>>Proficient</option>
                        <option value="2" <?php if($test[1] == 2) {echo 'selected';}?>>Competent</option>
                        <option value="1" <?php if($test[1] == 1) {echo 'selected';}?>>Beginner</option>
                      </select>

                    </div>

  <?php  }


  } ?>





                  </div>

                  <div class="add-more-button-wrapper">
                     <span class="node-counter" ng-cloak>{{testCount}} out of 10 available</span>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div class="create-job-wrapper middle-large-section clearfix">

              <button class="btn btn-success" id='make_job_live'>Save and Publish</button>
              <div class="status-dropdown">
                <select class="multiselect-single" style="visibility:hidden;" title="Level Required" id='status-update'>
                    <option value="live" selected>Live</option>
                    <option value="pending">Pending</option>
                    <option value="archive">Archive</option>
                </select>
              </div>
          </div>

        </div>


          <!-- Modal Begin -->
          <div class="modal fade load-talent-profile talent-pool"  tabindex="-1" role="dialog" aria-labelledby="load-talent-profile">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="load-talent-profile">Talent Details</h4>
                </div>
                <div class="talent-details-modal-body modal-body clearfix">
                  <div class="loader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>
                  <div class="manage-job">
                    <div class="columns-wrapper">
                    <div class="candidate-details-wrapper clearfix">
                      <div id="candaidate-details" class="candidate-details talent-details-wrapper">
                      Loading talent cv.
                      </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
          <!-- Modal END -->



      <?php endwhile; // End of the loop.
      } else {
        echo 'Access to this page is limited!';
        }?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
