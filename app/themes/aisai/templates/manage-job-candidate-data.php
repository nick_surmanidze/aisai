    <!-- ############# Candidate data start ################ -->
    <!-- Start of toolbar -->
    <div class="toolbar">
      <div class="btn-group" role="group" aria-label="...">
        <a href="#profile" class="btn btn-default">Profile</a>
        <a href="#chat" class="btn btn-default">Chat</a>

        <div class="btn-group" role="group">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Move to
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#">to Shortlisted</a></li>
            <li><a href="#">to Phone Screen</a></li>
            <li><a href="#">to Interview</a></li>
            <li><a href="#">to Offered</a></li>
            <li><a href="#">to Hired</a></li>
            <li><a href="#">to Rejected</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- End of toolbar -->

    <!-- Name and Tags - Begin -->
    <div class="white-panel basic-profile-panel">
      <h4 class="list-group-item-heading">Name Surname </h4>
      <p class="list-group-item-text">[job title] at [company name]</p>
      <span class="mail"><i class="fa fa-envelope-o"></i>mail@example.com</span>
      <span class="phone"><i class="fa fa-phone"></i>+123456789</span>

      <div class="tags-area">
        <div class="tags">
          <span class="tag">TAG CONTENT</span><span class="tag">TAG CONTENT for the second tag</span>
        </div>
        <button class="btn btn-sm btn-info">Add / Edit Tags</button>
      </div>
    </div>
    <!-- Name and Tags - END -->

    <!-- Messaging - Start -->
    <div class="white-panel chat-panel" id="chat">

      <div class="section-content">

        <!-- Start Conversation -->
        <div class="conversation">

          <div class="load-more">
            <span>Load Older Messages</span>
          </div>

          <div class="timeline-wrapper  col-md-12">

            <div class="item IN">
              <div class="timeline">
                <div class="circle"></div>
                <div class="line"></div>
              </div>
              <div class="item-content">
                <div class="panel panel-default">
                  <div class="panel-body">
                    message Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error ipsum, est praesentium voluptate, reiciendis distinctio! Culpa illo assumenda nobis tempora odio vel.
                  </div>
                </div>
              </div>
            </div>


            <div class="item out">
              <div class="timeline">
                <div class="circle"></div>
                <div class="line"></div>
              </div>
              <div class="item-content">
                <div class="panel panel-default">
                  <div class="panel-body">
                    message
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>

        <div class="editor">
          <textarea name="msg" id="msg-text" cols="30" rows="4"></textarea>
          <span class="chars-counter">chars left <span class="chars-num">1500</span></span>
          <span id="send-btn" class="btn btn-info btn-sm"><i class="fa fa-paper-plane-o"></i> send message</span>
        </div>

        <!-- End Conversation -->

      </div>
    </div>
    <!-- Messaging - END -->

    <!-- Profile - Begin -->
    <div class="white-panel full-profile-panel" id="profile">
      <div class="section-content">
        <!-- Summary - start -->
        <div class="subtitle col-md-12">
          <h2>Summary</h2>
        </div>
        <div class="summary col-md-12">
          <p>Hello World...</p>
        </div>
        <!-- Summary - end -->

        <div class="subtitle col-md-12">
          <h2>Career History and Key Achievements</h2>
        </div>

        <div class="career timeline-wrapper col-md-12">

          <div class="item">
            <div class="timeline ">
              <div class="circle"></div>
              <div class="line"></div>
            </div>
            <div class="item-content">
              <div class="panel panel-default">
                <div class="panel-body">

                  <h5 class="list-group-item-heading">
                    <span>Managing Director, </span>
                    <span>Advertising </span>
                    <br>
                  </h5>
                  <span>Some Company, </span>
                  <span>Mid-Sized Business </span>
                  <span> in the </span>
                  <span>Alternative Energy </span>
                  <span> sector.</span> <br>
                  <small>
                    <span>2015 </span><span> to </span>
                    <span>date. </span><br>
                    <span><pre>Some description</pre></span>
                  </small>

                </div>
              </div>
            </div>
          </div>

        </div>


        <div class="subtitle col-md-12">
          <h2>Education and Skills</h2>
        </div>

        <div class="education timeline-wrapper col-md-12">

          <div class="item">
            <div class="timeline">
              <div class="circle"></div>
              <div class="line"></div>
            </div>
            <div class="item-content">
              <div class="panel panel-default">
                <div class="panel-body">
                  <h5 class="list-group-item-heading">
                    <span>Aalto University</span>
                  </h5>
                  <span>Business and management</span>
                  <span>(Bachelors Degree)</span><br>
                  <span>Grade Attained: First-Class Honours</span><br>
                  <small>
                    <span>Awarded in 2012</span>
                  </small>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="sub-subtitle col-md-12">
          <h2>Specialist and Technical Skills</h2>
        </div>

        <div class="skills col-md-12">
          <ul class="list-group">
            <li class="list-group-item">Skill One</li>
            <li class="list-group-item">Skill Two</li>
            <li class="list-group-item">Skill Three</li>
          </ul>
        </div>
      </div>
    </div>
    <!-- Profile - END -->
    <!-- ############# Candidate data end ################ -->

