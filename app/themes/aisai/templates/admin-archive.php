<?php
/**
 * Template Name: Job Management (dashboard)
 * Custom template.
 */
get_header();


function get_active_menu_item($current, $root) {

  if(isset($_GET['status'])) {

    $status = $_GET['status'];
    if($status == $current) {
      return 'active';
    }

  } else {
    if($root == true) {
      return 'active';
    }

  }

}


// make a request and get list of jobs.
// ativate search by: title, employer, description
// Sorty by: date, title
// show: all, live, pending, archive
// load more: by default load 5 items maximum and then load more 5 items on button click

global $api;
$user_id = aisai::user_logged_in();

$status = 'all';

if(isset($_GET['status'])) {

  if($_GET['status'] == 'live') {
      $status = 'live';
  }

  if($_GET['status'] == 'archive') {
      $status = 'archive';
  }

  if($_GET['status'] == 'pending') {
      $status = 'pending';
  }

}

  $job_list = $api->sendRequest(array(
  'action'       => 'read',
  'controller'   => 'job',
  'id'           => '',
  'recruiter_id' => 0, // indicate 0 for showing all jobs and disregard the author
  'multiple'     => true, //****
  'query_string' => '', // search in title
  'limit'        => 5, //
  'offset'       => 0, //
  'sort_by'      => 'date', // date, title
  'order'        => 'desc',
  'status'       => $status, //all, live, pending, archive
  ));




$jobs = $job_list->jobs;
$total = $job_list->total;



  $pipeline = array(
    'shortlisted_label'  => 'Shortlisted',
    'phone_screen_label' => 'Phone Screen',
    'interview_label'    => 'Interview',
    'offer_label'        => 'Offer',
    'hired_label'        => 'Hired',
    'rejected_label'     => 'Rejected',
    );

?>

<script>var loadLimit = 5;</script>

  <div id="primary" class="content-area"  ng-controller="createSearchController">
    <main id="main" class="site-main" role="main">
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper job-management" id="admin-jobs">
          <div class="middle-large-section clearfix">
            <div class="menu-wrapper">
              <div class="menu">
                <div class="list-group">
                  <a href="/admin-archive" data-status='all' class="list-group-item <?php echo get_active_menu_item('all', true);?>">Job Management</a>
                  <a href="/admin-archive?status=live" data-status='live' class="list-group-item sub-item <?php echo get_active_menu_item('live', false);?>"><i class="fa fa-caret-right"></i> Live</a>
                  <a href="/admin-archive?status=pending" data-status='pending' class="list-group-item sub-item <?php echo get_active_menu_item('pending', false);?>"><i class="fa fa-caret-right"></i> Pending</a>
                  <a href="/admin-archive?status=archive" data-status='archive' class="list-group-item sub-item <?php echo get_active_menu_item('archive', false);?>"><i class="fa fa-caret-right"></i> Archive</a>
                </div>
              </div>
            </div>

            <div class="right-content clearfix">
              <div class="content">
                <div class="toolbar">
                  <div class="sort">

                    <div class="btn-group btn-group-sm" role="group">
                      <button type="button" class="btn btn-default label-item"><strong>Sort By:</strong></button>
                      <button type="button" class="btn btn-default sorter" data-factor="title"><span>Title</span></button>
                      <button type="button" class="btn btn-default sorter" data-factor="date"><span>Date</span></button>
                    </div>

                  </div>
                  <div class="search">

                    <div class="input-group  btn-group-sm">
                      <input type="text" class="form-control"  id="search-job-input" placeholder="Search for...">
                      <span class="input-group-btn">
                        <button class="btn btn-success" id="search-job-button" type="button">Search  <i class="fa fa-search"></i></button>
                      </span>
                    </div>

                  </div>
                </div>

                <div class="cards-wrapper">
                  <div class="loader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>
                  <div class="cards">
                  <?php
                  if(count($jobs) > 0) {

                  foreach($jobs as $job) { ?>

                  <!-- Job Card - START -->
                    <div class="job-card">
                      <div class="title-row">
                        <span class="title"><?php echo  $job->job_title; ?></span><span class="company-name"><?php echo  $job->company; ?></span>
                        <div class="btn-wrapper">
                          <a href="/admin-manage-job?id=<?php echo  $job->id; ?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Edit / Approve</a>
                        </div>

                      </div>
                      <div class="columns">

                          <div class="col-1-6">
                            <div class="line title"><?php echo $pipeline['shortlisted_label']; ?></div>
                            <div class="line value"><?php echo $job->pipeline->shortlisted; ?></div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title"><?php echo $pipeline['phone_screen_label']; ?></div>
                            <div class="line value"><?php echo $job->pipeline->phone_screen; ?></div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title"><?php echo $pipeline['interview_label']; ?></div>
                            <div class="line value"><?php echo $job->pipeline->interview; ?></div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title"><?php echo $pipeline['offer_label']; ?></div>
                            <div class="line value"><?php echo $job->pipeline->offer; ?></div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title"><?php echo $pipeline['hired_label']; ?></div>
                            <div class="line value"><?php echo $job->pipeline->hired; ?></div>
                          </div>

                          <div class="col-1-6">
                            <div class="line title"><?php echo $pipeline['rejected_label']; ?></div>
                            <div class="line value"><?php echo $job->pipeline->rejected; ?></div>
                          </div>

                      </div>
                      <div class="footer-line">

                      <span class="created-on">Created on: <?php echo $job->date_created; ?></span>

                      <div class="status-wrapper">
                        <span class="title">Status: </span>
                        <span class="status"><?php echo  $job->job_status; ?></span>
                      </div>

                      </div>

                    </div>
                  <!-- Job Card - END -->


                  <?php } // endforeach
                  } else { // if job count is 0


                    if($status == 'all') {

                      echo '<div class="no-jobs-found">You have no jobs</div>';

                    } elseif($status == 'pending') {

                      echo '<div class="no-jobs-found">You have no pending jobs</div>';

                    } elseif($status == 'live') {

                      echo '<div class="no-jobs-found">You have no live jobs</div>';

                    } elseif($status == 'archive') {

                      echo '<div class="no-jobs-found">You have no archived jobs</div>';

                    }


                    } ?>

                  </div>

                </div>


                <div class="load-more-btn-wrapper">
                  <span class='counter'><span class='loaded'><?php echo count($jobs); ?></span> out of <span class='total'><?php echo $total; ?></span></span>
                  <?php if(count($jobs) < $total) { ?>
                      <button class="btn btn-sm btn-success" id="load-more-button">Load More ...</button>
                  <?php } ?>

                </div>

                <!-- Modal Begin -->
                <div class="modal fade edit_job_details"  tabindex="-1" role="dialog" aria-labelledby="edit_job_details">
                  <div class="modal-dialog  modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="edit_job_details">Edit Job Details</h4>
                      </div>
                      <div class="modal-body">
                      <div class="loader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>
                      <div class="updated">Job Details Updated!</div>
                        <summernote config="summernoteOptions"><p><span style="font-weight: bold;color: #808080;">Job Description</span></p><p><br></p><p><br></p><p><br></p><p><span style="font-weight: bold;color: #808080;">Employer Description</span></p><p><br></p><p><br></p><p><br></p><p><span style="font-weight: bold;color: #808080;">Job Benefits</span></p><p><br></p><p><br></p></summernote>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="update_job_details">Save changes</button>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- Modal END -->

              </div>
            </div>
          </div>

        </div>
      <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
