<?php
/**
 * Template Name: Inbox
 * Custom template.
 */
get_header();


$user_id = aisai::user_logged_in();

/**
 * We have three scenarios:
 * inbox  - returns all incoming mail,
 * sent   - all outgoing,
 * Unread - all new
 */

//decide which function to use
$section = 'inbox';
#$output = get_messages($from, $to, $query, $subject, $read, $limit, $offset);

if(isset($_GET['show'])) {

  if($_GET['show'] == 'sent') {
    // we are in 'sent'
    $section = 'sent';

    $output = aisai::get_messages($user_id, 0, '', '', 1, 10, 0);

  } elseif($_GET['show'] == 'unread') {
    // we're in 'unread'
    $section = 'unread';
    $output = aisai::get_messages(0, $user_id, '', '', 0, 10, 0);

  } elseif($_GET['show'] == 'inbox') {
    // we're in 'inbox'
    $section = 'inbox';
    $output = aisai::get_messages(0, $user_id, '', '', 1, 10, 0);

  }

} else {
  // we are in the inbox
  $section = 'inbox';
  $output = aisai::get_messages(0, $user_id, '', '', 1, 10, 0);

}
$total = 0;
if($output->total > 0 ) {
$total = $output->total;
}

?>
<script type="text/javascript"> var section = "<?php echo $section; ?>";</script>
  <div id="primary" class="content-area" ng-controller="createSearchController">
    <main id="main" class="site-main" role="main">
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="content-wrapper inbox inbox-content">
          <div class="middle-large-section clearfix">
            <div class="menu-wrapper">
              <div class="menu">
                <div class="list-group">
                  <a href="<?php get_home_url();?>/dashboard" class="list-group-item">Job Management</a>
                  <a href="<?php get_home_url();?>/inbox?show=inbox" class="list-group-item <?php is_current_item('show', 'inbox'); ?>">Inbox
                    <?php $new = aisai::get_number_of_new_messages();
                    if($new > 0) { ?>
                      <span class="badge badge-red"><?php echo $new; ?></span>
                    <?php } ?>

                  </a>
                  <a href="<?php get_home_url();?>/inbox?show=sent" class="list-group-item sub-item <?php is_current_item('show', 'sent'); ?>"><i class="fa fa-caret-right"></i> Sent</a>
                  <a href="<?php get_home_url();?>/inbox?show=unread" class="list-group-item sub-item <?php is_current_item('show', 'unread'); ?>"><i class="fa fa-caret-right"></i> Unread

                    <?php
                    if($new > 0) { ?>
                      <span class="badge badge-blue"><?php echo $new; ?></span>
                    <?php } ?>
                    </a>

                  <?php if(can_use_talent_pool()) { ?>
                  <a href="/talent-pool" class="list-group-item">Talent Pool</a>
                  <?php } ?>
                </div>
              </div>
            </div>

            <div class="right-content clearfix">
              <div class="content">
                <div class="toolbar">
                  <div class="sort">

                    <div class="btn-group btn-group-sm" role="group">
                      <button type="button" class="btn btn-default label-item"><strong>Sort By:</strong></button>
                      <button type="button" class="btn btn-default sorter" data-factor="talent"><span>Talent</span></button>
                      <button type="button" class="btn btn-default sorter" data-factor="subject"><span>Job</span></button>
                      <button type="button" class="btn btn-default sorter" data-factor="date"><span>Date</span></button>
                    </div>

                  </div>
                  <div class="search">

                    <div class="input-group  btn-group-sm">
                      <input type="text" class="form-control" id="querystring" placeholder="Search by subject..">
                      <span class="input-group-btn">
                        <button class="btn btn-success" type="button" id="search-messages"><span class='search-button-label'>Search </span><i class="fa fa-search"></i></button>
                      </span>
                    </div>

                  </div>
                </div>

                <div class="cards-wrapper">
                <div class="loader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>
                  <div class="list-group">




                        <?php

                        $number = (array) $output->messages;
                        if (count($number) > 0) {

                          if($section == 'sent') {

                          foreach((array) $output->messages as $message) { ?>

                    <a href="<?php get_home_url(); ?>/conversation?from-user=<?php echo $message->from_user_id; ?>&amp;to-user=<?php echo $message->to_user_id; ?>&amp;subject=<?php echo $message->message_subject; ?>" class="list-group-item">
                      <h4 class="list-group-item-heading"><span class="direction">TO:</span><?php echo $message->sent_to; ?> / <span class="subject"><?php echo $message->message_subject; ?></span></h4>
                      <span class="date"><?php echo $message->message_sent_date; ?></span>
                      <p class="list-group-item-text"><?php echo $message->message_body; ?></p>
                    </a>



                         <?php } // endforeach



                          } else {




                          foreach((array) $output->messages as $message) { ?>


                          <a href="<?php get_home_url(); ?>/conversation?from-user=<?php echo $message->from_user_id; ?>&amp;to-user=<?php echo $message->to_user_id; ?>&amp;subject=<?php echo $message->message_subject; ?>" class="list-group-item">
                          <h4 class="list-group-item-heading"><span class="direction">FROM:</span><?php echo $message->sent_from; ?> / <span class="subject"><?php echo $message->message_subject; ?></span>
                              <?php if($message->message_read == 0) { ?>
                                  <span class="label label-danger">New</span>
                              <?php } ?>
                          </h4>
                          <span class="date"><?php echo $message->message_sent_date; ?></span>
                          <p class="list-group-item-text"><?php echo $message->message_body; ?></p>
                          </a>


                         <?php } // endforeach

                        }


                        } else { ?>

                        <div class="no-messages">There are no messages!</div>

                        <? } ?>




                  </div>


                </div>



                <div class="load-more-btn-wrapper">
                  <span class='counter'><span class='loaded'><?php echo count((array) $output->messages); ?></span> out of <span class='total'><?php echo $total; ?></span></span>
                  <?php if(count((array) $output->messages) < $total) { ?>
                      <button class="btn btn-sm btn-success" id="load-more-conversations">Load More ...</button>
                  <?php } ?>

                </div>


              </div>
            </div>
          </div>

        </div>
      <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
