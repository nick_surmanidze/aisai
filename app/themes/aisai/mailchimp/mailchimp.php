<?php

// Mailchimp api class
require_once('MCAPI.class.php');

// Mailchimp options page
require_once('mailchimp-options.php');


function subscribe_to_mailchimp($email = 'default@opengo.com', $name = 'John Doe', $list_id = 0) {

	// get api key and list id
		$api_k = sanitize_text_field(get_option("og_mailchimp_api_key"));
	  $mc_api = new MCAPI($api_k);

	  $name = sanitize_text_field($name);
	  $email = $email;

	  $merge_vars = Array(
	      'EMAIL' => $email,
	      'FNAME' => $name
	  );
	  $subscribed = $mc_api->listSubscribe($list_id, $email, $merge_vars, 'html', false);
	  return  $subscribed;

}


?>
