/**
 * APP
 *
 * AISAI front end.
 *
 * DEV -  Nick Surmanidze
 */

(function($) {
"use strict";

var aisaiApp = angular.module('aisaiApp', ['auto-value', 'summernote']);

// Create Search


aisaiApp.controller('createSearchController', function ($scope, $timeout, $compile) {



  $scope.summernoteOptions = {
    height: 260,
    toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['view', ['fullscreen']],
        ]
  };

  // set initial value of radio button to gbp
  $scope.currency = 'gbp';
  $scope.testCount = 0;

  $scope.updateTestCount = function() {

 setTimeout(function () {
        $scope.$apply(function () {
            $scope.testCount = $(document).find('.test-node').length;
        });
    }, 1);

  }
  $scope.updateTestCount();


  function isOnlyText(val) {
      if(val !== undefined) {
      var matches = val.match(/^[a-z\d\-_\s\`\/]+$/i);
      if (matches != null) {
          return true;
      } else {
        return false;
      }
    }
  }

  $scope.is_alpha = function(val) {
      return isOnlyText(val);
  }

  $scope.add = function() {

    if($scope.testCount < 10 ) {
      $('.test-node-container').append($compile('<div class="test-node"> <input type="text" class="form-control capability" placeholder="Enter Capability Required"  value=""> <select class="multiselect-single" style="visibility:hidden;" title="Level Required"> <option value="">Level Required</option> <option value="4">Expert</option> <option value="3"class="">Proficient</option> <option value="2">Competent</option> <option value="1">Beginner</option> </select> <button class="btn btn-danger remove-node"><i class="fa fa-times"></i> Remove</button> <div class="error-message">Please enter capability value and indicate required level.</div></div>')($scope));
      $('.multiselect-single').multiselect('refresh');
      $scope.updateTestCount();
    }
  }

  // remove node
  $('.test-node-container').on('click', '.remove-node' , function() {
    $(this).parent().remove();
    $scope.updateTestCount();
  });


  $scope.create = function() {

    $scope.data = {};

    $scope.searchFields = $scope.getSearchFields();
    $scope.caps = $scope.getCapabilities();
    $scope.details = $scope.getJobDetails();

    if(!($scope.searchFields == false) && !($scope.caps == false) && !($scope.details == false)) {

      $scope.data.searchFields = $scope.searchFields;
      $scope.data.capabilities = $scope.caps;
      $scope.data.details = $scope.details;

      // go log yourself
      console.log($scope.data);


      // start of ajax wrapper
      $(".item-preloader").show();
      $(".item-failed-msg").hide();
      $(".item-success-msg").hide();
      $.ajax({
          data: ({
            action : 'ai_create_search',
            params: $scope.data
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {
           $(".item-preloader").hide();
            console.log(msg);
            if(msg == 'failed') {
              //show failed msg
               jQuery('.job-was-not-created-modal').modal('show');
            } else {
              //show success msg
              jQuery('.job-was-created-modal').modal('show');




            }
      });
      // end of ajax wrapper





    } else {
      // if($scope.getSearchFields() == false) {
      //   console.log('search fields false');
      // }
      // if($scope.getCapabilities() == false) {
      //   console.log('caps false');
      // }

      // if($scope.getJobDetails() == false) {
      //   console.log('details false');
      // }
      console.log('if you do not correct the errors than the skies will go down.');
    }


  }


  $scope.getSearchFields = function() {

    // create search fields object. it will contain all the data from the search fields
    $scope.searchFileds = {};

    $scope.searchFileds.idealIndustry      = $scope.ideal_industry;
    $scope.searchFileds.idealSeniority     = $scope.ideal_seniority;
    $scope.searchFileds.idealJobFunction   = $scope.ideal_job_function;
    $scope.searchFileds.idealLocation      = $scope.ideal_job_location;
    $scope.searchFileds.idealPay           = $scope.ideal_pay;
    $scope.searchFileds.idealCompanyType   = $scope.ideal_company_type;
    $scope.searchFileds.idealContract      = $scope.ideal_contract;

    $scope.searchFileds.industryExperience = $scope.industry_experience;
    $scope.searchFileds.roleExperience     = $scope.role_experience;
    $scope.searchFileds.seniority          = $scope.seniority;
    $scope.searchFileds.currentPay         = $scope.current_pay;
    $scope.searchFileds.contract           = $scope.contract;
    $scope.searchFileds.currency           = $scope.currency;

    $scope.searchFileds.qualificationType  = $scope.qualification_type;
    $scope.searchFileds.courseName         = $scope.course_name;
    $scope.searchFileds.gradeAttained      = $scope.grade_attained;

    return $scope.searchFileds;
  }



  // if job details are validated correctly than return them or return false
  $scope.getJobDetails = function() {
    $scope.empty_title = false;
    $scope.empty_employer = false;
    $scope.empty_description = false;

    $scope.jobDetails = {};

    if($scope.validateJobDetails() == true) {

      $scope.jobDetails.title = $scope.job_title;
      $scope.jobDetails.employer = $scope.employer;
      $scope.jobDetails.job_description = $scope.job_description;

      return $scope.jobDetails;

    } else {
      return false;
    }

  }

  // function will return true or false
  $scope.validateJobDetails = function() {

    // set variable that will be returned. Initially true
    $scope.detailsValid = true;

    // validate job title
    if($scope.job_title.length < 1 || !$scope.is_alpha($scope.job_title)) {
      // invalid
      $scope.empty_title = true;
      $scope.detailsValid = false;
    }

    // validate employer
    if($scope.employer.length < 1 || !$scope.is_alpha($scope.employer)) {
      // invalid
      $scope.empty_employer = true;
      $scope.detailsValid = false;
    }

    // validate description
    if($scope.job_description.length < 500) {
      // invalid
       $scope.empty_description = true;
       $scope.detailsValid = false;
    }

    return $scope.detailsValid;
  }




// if capabilities are entered correctly, than return them or return false
$scope.getCapabilities = function() {

  $scope.capabilities = [];

  //hide all error messages
  $(".test-node").each(function(){
      $(this).find('.error-message').hide();
  });

if ($(document).find('.test-node').length > 0) {

  if($scope.validateCapabilities() == true) {

    $(".test-node").each(function(){
       $scope.capabilities.push([$(this).find('.capability').val(),$(this).find('.multiselect-single').val()]);

    });
    return $scope.capabilities;
  } else {
    // $scope.validateCapabilities();
    return false;
  }


} else {
  // this is in case we do not have capabilities indicated.
  return true;
}


}


// shows validation message or returns false
$scope.validateCapabilities = function() {
  $scope.result = true;

if($(document).find('.test-node').length > 0 ) {

  $(".test-node").each(function(){
    $scope.result = true;

    if(!$(this).find('.capability').val() || !$(this).find('.multiselect-single').val()) {
      $(this).find('.error-message').show();
      $scope.result = false;
    }

  });

}

  return $scope.result;
}


});





aisaiApp.controller('membershipSettingsController', function ($scope, $timeout, $compile) {


$scope.userCount = 0;

$scope.updateUserCount = function() {

  $timeout( function() {
    $scope.userCount = $(document).find('.user-node').length;
  }, 0);

}

$scope.updateUserCount();

$scope.add = function() {

  if($scope.userCount < 2 ) {
    $('.user-node-container').append($compile(' <div class="user-node"> <input type="text" class="form-control user-email" placeholder="user email address" value=""> <button class="btn btn-danger remove-node"><i class="fa fa-times"></i> Remove</button> </div>')($scope));
    $('.multiselect-single').multiselect('refresh');
    $scope.updateUserCount();
  }

}


$('.user-node-container').on('click', '.remove-node' , function() {
  $(this).parent().remove();
  $scope.updateUserCount();
});

});


// aisaiApp.controller('jobArchiveController', function ($scope, $timeout, $compile) {
// $scope.something = 'nothing';
// });

///////////////////////////////////////////////////////////////////
// Job Sorting, search, filter


function get_job_search_params() {

  var params = {};

  params.status = 'all';
  params.sort_by = 'date';
  params.order = 'desc';
  params.query = '';

  if($('.list-group .active').length > 0) {
    params.status = $('.list-group .active').data('status');
  }

  // now sort order and sort by

  if($('.sorter.asc').length > 0 || $('.sorter.desc').length > 0) {
    // we have sorting activated
    if($('.sorter.asc').length > 0) {
      params.order = 'asc';
      params.sort_by = $('.sorter.asc').data('factor');
    }

    if($('.sorter.desc').length > 0) {
      params.order = 'desc';
      params.sort_by = $('.sorter.desc').data('factor');
    }
 }

    params.query = $('#search-job-input').val();

  return params;

}

// output search results
$('#user-jobs #search-job-button').click(function(){
  var params = {};
  params = get_job_search_params();
  params.limit = parseInt($('.counter .loaded').html());
  if(params.limit < 5) {
    params.limit = 5;
  }
  params.offset = 0;
      // start of ajax wrapper
      $(".cards-wrapper .loader").show();
console.log(get_job_search_params());
      $.ajax({
          data: ({
            action : 'ai_output_jobs',
            params: params
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {
           $(".cards-wrapper .loader").hide();
           var output = JSON.parse(msg);
           console.log(output);
            $(".cards-wrapper .cards").html(output.data);
            $('.counter .loaded').html(params.limit);
            $('.counter .total').html(output.total);
            if(params.limit >= output.total) {
              $('.counter .loaded').html(output.total);
              $('#load-more-button').hide();
            } else {
              $('.counter .loaded').html(params.limit);
              $('#load-more-button').show();
            }

      });
      // end of ajax wrapper
});


// output sort results
$('#user-jobs .sorter').click(function(){


if($(this).hasClass("asc")) {

    $(".sorter").each(function(index, value) {
    $(this).removeClass("asc").removeClass("desc");

  });

  $(this).removeClass("asc").addClass("desc");

} else if ($(this).hasClass("desc")) {

    $(".sorter").each(function(index, value) {
    $(this).removeClass("asc").removeClass("desc");

  });

  $(this).removeClass("asc").removeClass("desc");

} else {

    $(".sorter").each(function(index, value) {
    $(this).removeClass("asc").removeClass("desc");

  });

  $(this).addClass("asc");
}



  var params = {};
  params = get_job_search_params();
  params.limit = parseInt($('.counter .loaded').html());
  params.offset = 0;


      // start of ajax wrapper
      $("#user-jobs .cards-wrapper .loader").show();
      console.log(get_job_search_params());

      $.ajax({
          data: ({
            action : 'ai_output_jobs',
            params: params
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {
           $(".cards-wrapper .loader").hide();
           var output = JSON.parse(msg);
           console.log(output);
            $(".cards-wrapper .cards").html(output.data);
      });
      // end of ajax wrapper
});


$('#user-jobs #load-more-button').click(function() {

  var params = {};
  params = get_job_search_params();
  params.limit = parseInt($('.counter .loaded').html()) + 5;
  params.offset = 0;


      // start of ajax wrapper
      $(".cards-wrapper .loader").show();
      console.log(get_job_search_params());

      $.ajax({
          data: ({
            action : 'ai_output_jobs',
            params: params
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {
           $(".cards-wrapper .loader").hide();
           var output = JSON.parse(msg);
           console.log(output);
            $(".cards-wrapper .cards").html(output.data);

            if(params.limit >= output.total) {
              $('.counter .loaded').html(output.total);
              $('#load-more-button').hide();
            } else {
              $('.counter .loaded').html(params.limit);
              $('#load-more-button').show();
            }

      });
      // end of ajax wrapper

});


//////////////////////////////////
// The same but for admin panel
//////////////////////////////////





// output search results
$('#admin-jobs #search-job-button').click(function(){
  var params = {};
  params = get_job_search_params();
  params.limit = parseInt($('.counter .loaded').html());
  params.offset = 0;
      // start of ajax wrapper
      $(".cards-wrapper .loader").show();
      console.log(get_job_search_params());
      $.ajax({
          data: ({
            action : 'ai_output_jobs_admin',
            params: params
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {
           $(".cards-wrapper .loader").hide();
           var output = JSON.parse(msg);
           console.log(output);
            $(".cards-wrapper .cards").html(output.data);
            $('.counter .loaded').html(params.limit);
            $('.counter .total').html(output.total);
            if(params.limit >= output.total) {
              $('.counter .loaded').html(output.total);
              $('#load-more-button').hide();
            } else {
              $('.counter .loaded').html(params.limit);
              $('#load-more-button').show();
            }

      });
      // end of ajax wrapper
});


// output sort results
$('#admin-jobs .sorter').click(function(){


if($(this).hasClass("asc")) {

    $(".sorter").each(function(index, value) {
    $(this).removeClass("asc").removeClass("desc");

  });

  $(this).removeClass("asc").addClass("desc");

} else if ($(this).hasClass("desc")) {

    $(".sorter").each(function(index, value) {
    $(this).removeClass("asc").removeClass("desc");

  });

  $(this).removeClass("asc").removeClass("desc");

} else {

    $(".sorter").each(function(index, value) {
    $(this).removeClass("asc").removeClass("desc");

  });

  $(this).addClass("asc");
}



  var params = {};
  params = get_job_search_params();
  params.limit = 5;
  params.offset = 0;


      // start of ajax wrapper
      $("#admin-jobs .cards-wrapper .loader").show();
      console.log(get_job_search_params());

      $.ajax({
          data: ({
            action : 'ai_output_jobs_admin',
            params: params
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {
           $(".cards-wrapper .loader").hide();
           var output = JSON.parse(msg);
           console.log(output);
            $(".cards-wrapper .cards").html(output.data);
      });
      // end of ajax wrapper
});


$('#admin-jobs #load-more-button').click(function() {

  var params = {};
  params = get_job_search_params();
  params.limit = parseInt($('.counter .loaded').html()) + 5;
  params.offset = 0;


      // start of ajax wrapper
      $(".cards-wrapper .loader").show();
      console.log(get_job_search_params());

      $.ajax({
          data: ({
            action : 'ai_output_jobs_admin',
            params: params
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {
           $(".cards-wrapper .loader").hide();
           var output = JSON.parse(msg);
           console.log(output);
            $(".cards-wrapper .cards").html(output.data);

            if(params.limit >= output.total) {
              $('.counter .loaded').html(output.total);
              $('#load-more-button').hide();
            } else {
              $('.counter .loaded').html(params.limit);
              $('#load-more-button').show();
            }

      });
      // end of ajax wrapper

});


// Job Sorting, search, filter
///////////////////////////////////////////////////////////////////




})( jQuery );
