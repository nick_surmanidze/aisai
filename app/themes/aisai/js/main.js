/**
 * main.js
 *
 * Main javascript file. Aisai.
 *
 *
 */


/*
 * Summernote PasteClean
 *
 * This is a plugin for Summernote (www.summernote.org) WYSIWYG editor.
 * It will clean up the content your editors may paste in for unknown sources
 * into your CMS. It strips Word special characters, style attributes, script
 * tags, and other annoyances so that the content can be clean HTML with
 * no unknown hitchhiking scripts or styles.
 *
 * @author Jason Byrne, FloSports <jason.byrne@flosports.tv>
 *
 */

(function (factory) {
  /* global define */
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else {
    // Browser globals: jQuery
    factory(window.jQuery);
  }
}(function ($) {

    var badTags = [
            'font',
            'style',
            'embed',
            'param',
            'script',
            'html',
            'body',
            'head',
            'meta',
            'title',
            'link',
            'iframe',
            'applet',
            'noframes',
            'noscript',
            'form',
            'input',
            'select',
            'option',
            'std',
            'xml:',
            'st1:',
            'o:',
            'w:',
            'v:'
        ],
        badAttributes = [
            'style',
            'start',
            'charset'
        ];

    $.summernote.addPlugin({
        name: 'pasteClean',
        init: function(layoutInfo) {

            var $note = layoutInfo.holder();

            $note.on('summernote.paste', function(e, evt) {
                evt.preventDefault();
                // Capture pasted data
                var text = evt.originalEvent.clipboardData.getData('text/plain'),
                    html = evt.originalEvent.clipboardData.getData('text/html');
                // Clean up html input
                if (html) {
                    // Regular expressions
                    var tagStripper         = new RegExp('<[ /]*(' + badTags.join('|') + ')[^>]*>', 'gi'),
                        attributeStripper   = new RegExp(' (' + badAttributes.join('|') + ')(="[^"]*"|=\'[^\']*\'|=[^ ]+)?', 'gi'),
                        commentStripper     = new RegExp('<!--(.*)-->', 'g');

                    // clean it up
                    html = html.toString()
                        // Remove comments
                        .replace(commentStripper, '')
                        // Remove unwanted tags
                        .replace(tagStripper, '')
                        // remove unwanted attributes
                        .replace(attributeStripper, ' ')
                        // remove Word classes
                        .replace(/( class=(")?Mso[a-zA-Z]+(")?)/g, ' ')
                        // remove whitespace (space and tabs) before tags
                        .replace(/[\t ]+\</g, "<")
                        // remove whitespace between tags
                        .replace(/\>[\t ]+\</g, "><")
                        // remove whitespace after tags
                        .replace(/\>[\t ]+$/g, ">")
                        // smart single quotes and apostrophe
                        .replace(/[\u2018\u2019\u201A]/g, "'")
                        // smart double quotes
                        .replace(/[\u201C\u201D\u201E]/g, '"')
                        // ellipsis
                        .replace(/\u2026/g, '...')
                        // dashes
                        .replace(/[\u2013\u2014]/g, '-');
                }
                // Do the paste
                var $dom = $('<div class="pasted"/>').html(html || text);
                $note.summernote('insertNode', $dom[0]);
                return false;
            });

        }
    });
}));





(function($) {
"use strict";

var ogApp = {};


function isOnlyText(val) {
    if(val !== undefined) {
    var matches = val.match(/^[a-z\d\-_\s\.\,\'\`\"\(\)\/]+$/i);
    if (matches != null) {
        return true;
    } else {
      return false;
    }
  }
}

function isAlphanumeric(val) {
    if(val !== undefined) {
    var matches = val.match(/^[a-z\d\-_\s\.\,\(\)\/]+$/i);
    if (matches != null) {
        return true;
    } else {
      return false;
    }
  }
}


function isLinkedinProfile(val) {
    if(val !== undefined) {
    var matches = val.match(/(ftp|http|https):\/\/?((www|\w\w)\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/);
    if (matches != null) {
        return true;
    } else {
      return false;
    }
  }
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
};


function isPhoneNumber(number) {
  var pattern = new RegExp(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im);
    return pattern.test(number);
};




$(document).ready(function() {

  // Init multiselect
  if($('.multiselect').length > 0) {
      $('.multiselect').multiselect({
        maxHeight:200,
        buttonText: function(options, select) {
                var count = select.find(':selected').length;
                if(count > 0) {
                  return select.attr('title') + ' ('+count+')';
                } else {
                  return select.attr('title') + ' (any)';
                }
            }

          });
  }

if($('.multiselect-single').length > 0) {

$('.multiselect-single').multiselect({
    on: {
        change: function(option, checked) {
            var values = [];
            $('.multiselect-single option').each(function() {
                if ($(this).val() !== option.val()) {
                    values.push($(this).val());
                }
            });

            $('.multiselect-single').multiselect('deselect', values);
        }
    }
});

}



  // Init multiselect with filter
  if($('.multiselect-filter').length > 0) {
      $('.multiselect-filter').multiselect({maxHeight:200, enableFiltering: true, enableCaseInsensitiveFiltering: true,filterPlaceholder: 'Search'});
  }

  // Init multiselect with three possible options
  if($('.multiselect-limit-three').length > 0) {

    $('.multiselect-limit-three').each(function(){

      var thisselect = $(this);

      thisselect.multiselect({
        buttonText: function(options, select) {
          var count = select.find(':selected').length;
          if(count > 0) {
            return select.attr('title') + ' ('+count+')';
          } else {
            return select.attr('title') + ' (any)';
          }
        },
        maxHeight:200,
        numberDisplayed:true,
        templates: {
          ul: '<ul class="multiselect-container dropdown-menu limit-three-ms"></ul>',
        },
        onChange: function(option, checked) {

            // Get selected options.
            var selectedOptions = thisselect.find('option:selected');

            if (selectedOptions.length >= 3) {
                // Disable all other checkboxes.
                var nonSelectedOptions = thisselect.find('option').filter(function() {
                  return !$(this).is(':selected');
                });

                // var dropdown = thisselect.siblings('.multiselect-container');
                nonSelectedOptions.each(function() {
                  var input = $('.limit-three-ms input[value="' + $(this).val() + '"]');
                  input.prop('disabled', true);
                  input.parent('li').addClass('disabled');
                });
            }
            else {
                // Enable all checkboxes.
                thisselect.find('option').each(function() {
                  var input = $('.limit-three-ms input[value="' + $(this).val() + '"]');
                  input.prop('disabled', false);
                  input.parent('li').addClass('disabled');
                });
            }
          }
      });
    });

  }




// Change Filter State
//-----------------------------------



//-----------------------------------
// Trigger for sorting



function get_filter_results(page) {

  $(".action-button .ajax-spinner").show();

  var sortfactor = '';
  var direction = '';

  if(page) {
    var curPage = page;
  } else {
    curPage = 1;
  }


  $(".sorter span").each(function(index, value) {

    if($(this).hasClass("asc")) {

      sortfactor = $(this).attr('data-factor');
      direction = "ASC";

    } else if ($(this).hasClass("desc")) {

      sortfactor = $(this).attr('data-factor');
      direction = "DESC";

    }

  });


}

// Trigger for sorting
//-----------------------------------


//-----------------------------------
// Load More Messages - Inbox

$(document).on('click', '#load-more-conversations', function(){

  //preloader show
  $('.inbox-content .loader').show();
  //increase load limit by 5
  var params = {};
  params.limit = parseInt($('.counter .loaded').html()) + 5;
  if( isNaN(params.limit)) {
    params.limit = 10;
  }
  params.section = section;
  params.query = '';
  params.order = 'DESC';
  params.sort_by = 'date';

  if($('.inbox #querystring').length > 0) {
    params.query = $('.inbox #querystring').val();
  }

    if($('.sorter.asc').length > 0 || $('.sorter.desc').length > 0) {
      // we have sorting activated
      if($('.sorter.asc').length > 0) {
        params.order = 'asc';
        params.sort_by = $('.sorter.asc').data('factor');
      }

      if($('.sorter.desc').length > 0) {
        params.order = 'desc';
        params.sort_by = $('.sorter.desc').data('factor');
      }
   }

  //send ajax request
    $.ajax({
        data: ({
          action : 'og_load_more_conversations',
          params: params,
          }),
          type: 'POST',
          async: true,
          url: aiAjax,
          })
    // On done return response
    .done(function( msg ) {
        var output = JSON.parse(msg);
        $('.inbox-content .cards-wrapper .list-group').html(output.data);
        $('.inbox-content .loader').hide();

        if(params.limit >= output.total) {
          $('.counter .loaded').html(output.total);
          $('#load-more-conversations').hide();
        } else {
          $('.counter .loaded').html(params.limit);
          $('#load-more-conversations').show();
        }
        console.log(params);
        console.log(msg);
    });


});


// Load More Messages - Inbox
//-----------------------------------



// output sort results
$('.inbox .sorter').click(function(){


    if($(this).hasClass("asc")) {

        $(".sorter").each(function(index, value) {
        $(this).removeClass("asc").removeClass("desc");

      });

      $(this).removeClass("asc").addClass("desc");

    } else if ($(this).hasClass("desc")) {

        $(".sorter").each(function(index, value) {
        $(this).removeClass("asc").removeClass("desc");

      });

      $(this).removeClass("asc").removeClass("desc");

    } else {

        $(".sorter").each(function(index, value) {
        $(this).removeClass("asc").removeClass("desc");

      });

      $(this).addClass("asc");
    }

    // now output the results
  //preloader show
  $('.inbox-content .loader').show();
  //increase load limit by 5
  var params = {};
  params.limit = parseInt($('.counter .loaded').html()) + 0;
  if( isNaN(params.limit)) {
    params.limit = 10;
  }
  params.section = section;
  params.query = '';
  params.order = 'desc';
  params.sort_by = 'date';

  if($('.inbox #querystring').length > 0) {
    params.query = $('.inbox #querystring').val();
  }

    if($('.sorter.asc').length > 0 || $('.sorter.desc').length > 0) {
      // we have sorting activated
      if($('.sorter.asc').length > 0) {
        params.order = 'asc';
        params.sort_by = $('.sorter.asc').data('factor');
      }

      if($('.sorter.desc').length > 0) {
        params.order = 'desc';
        params.sort_by = $('.sorter.desc').data('factor');
      }
   }

  //send ajax request
    $.ajax({
        data: ({
          action : 'og_load_more_conversations',
          params: params,
          }),
          type: 'POST',
          async: true,
          url: aiAjax,
          })
    // On done return response
    .done(function( msg ) {
        var output = JSON.parse(msg);
        $('.inbox-content .cards-wrapper .list-group').html(output.data);
        $('.inbox-content .loader').hide();

        if(params.limit >= output.total) {
          $('.counter .loaded').html(output.total);
          $('#load-more-conversations').hide();
        } else {
          $('.counter .loaded').html(params.limit);
          $('#load-more-conversations').show();
        }
        console.log(params);
        console.log(msg);
    });


 });

$('#search-messages').click(function(){

  //preloader show
  $('.inbox-content .loader').show();
  //increase load limit by 5
  var params = {};
  params.limit = parseInt($('.counter .loaded').html()) + 5;
  if( isNaN(params.limit)) {
    params.limit = 10;
  }
  params.section = section;
  params.query = '';
  params.order = 'DESC';
  params.sort_by = 'date';

  if($('.inbox #querystring').length > 0) {
    params.query = $('.inbox #querystring').val();
  }

    if($('.sorter.asc').length > 0 || $('.sorter.desc').length > 0) {
      // we have sorting activated
      if($('.sorter.asc').length > 0) {
        params.order = 'asc';
        params.sort_by = $('.sorter.asc').data('factor');
      }

      if($('.sorter.desc').length > 0) {
        params.order = 'desc';
        params.sort_by = $('.sorter.desc').data('factor');
      }
   }

  //send ajax request
    $.ajax({
        data: ({
          action : 'og_load_more_conversations',
          params: params,
          }),
          type: 'POST',
          async: true,
          url: aiAjax,
          })
    // On done return response
    .done(function( msg ) {
        var output = JSON.parse(msg);
        $('.inbox-content .cards-wrapper .list-group').html(output.data);
        $('.inbox-content .loader').hide();

        if(params.limit >= output.total) {
          $('.counter .loaded').html(output.total);
          $('#load-more-conversations').hide();
        } else {
          $('.counter .loaded').html(params.limit);
          $('#load-more-conversations').show();
        }
        console.log(params);
        console.log(msg);
    });

});



// update my info (settings page)

$('#update_my_info').click(function(){


  var is_valid = true;

  $('.error-message').hide();

  if(($('#my_info #name').val().length < 1) || $('#my_info #name').val().length > 40 || !isAlphanumeric($('#my_info #name').val())) {
    is_valid = false;
    $('#my_info #name').parent().find('.error-message').show();
  }

  if(($('#my_info #surname').val().length < 1) || $('#my_info #surname').val().length > 40 || !isAlphanumeric($('#my_info #surname').val())) {
    is_valid = false;
    $('#my_info #surname').parent().find('.error-message').show();
  }

  if(($('#my_info #display_name').val().length < 1) || $('#my_info #display_name').val().length > 40 || !isAlphanumeric($('#my_info #display_name').val())) {
    is_valid = false;
    $('#my_info #display_name').parent().find('.error-message').show();
  }

  if(($('#my_info #employer_name').val().length < 1) || $('#my_info #employer_name').val().length > 40 || !isAlphanumeric($('#my_info #employer_name').val())) {
    is_valid = false;
    $('#my_info #employer_name').parent().find('.error-message').show();
  }

  if(($('#my_info #linkedin').val().length > 0) && !isLinkedinProfile($('#my_info #linkedin').val())) {

    is_valid = false;
    $('#my_info #linkedin').parent().find('.error-message').show();

  }


  if(is_valid == true) {

    // get the values if validation was successful

    var data = {};

    data.name          = $('#my_info #name').val();
    data.surname       = $('#my_info #surname').val();
    data.display_name  = $('#my_info #display_name').val();
    data.employer_name = $('#my_info #employer_name').val();
    data.linkedin      = $('#my_info #linkedin').val();
    data.recruiter_email_frequency      = $('#my_info #email-frequency').val();

  $('.item-preloader').show();
    //send ajax request
      $.ajax({
          data: ({
            action : 'ai_update_info_settings',
            params: data,
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {
        $('.item-preloader').hide();
        location.reload();
          console.log(data);
          console.log(msg);
      });


  }

});



// update my info (settings page)

$('#update_pipeline').click(function(){

  var is_valid = true;

  $('.error-message').hide();

  // if(($('#rec_pipeline #shortlisted_label').val().length < 1) || $('#rec_pipeline #shortlisted_label').val().length > 15 || !isAlphanumeric($('#rec_pipeline #shortlisted_label').val())) {
  //   is_valid = false;
  //   $('#rec_pipeline #shortlisted_label').parent().find('.error-message').show();
  // }


  if(($('#rec_pipeline #phone_screen_label').val().length < 1) || $('#rec_pipeline #phone_screen_label').val().length > 15 || !isAlphanumeric($('#rec_pipeline #phone_screen_label').val())) {
    is_valid = false;
    $('#rec_pipeline #phone_screen_label').parent().find('.error-message').show();
  }


  if(($('#rec_pipeline #interview_label').val().length < 1) || $('#rec_pipeline #interview_label').val().length > 15 || !isAlphanumeric($('#rec_pipeline #interview_label').val())) {
    is_valid = false;
    $('#rec_pipeline #interview_label').parent().find('.error-message').show();
  }


  if(($('#rec_pipeline #offer_label').val().length < 1) || $('#rec_pipeline #offer_label').val().length > 15 || !isAlphanumeric($('#rec_pipeline #offer_label').val())) {
    is_valid = false;
    $('#rec_pipeline #offer_label').parent().find('.error-message').show();
  }


  if(($('#rec_pipeline #hired_label').val().length < 1) || $('#rec_pipeline #hired_label').val().length > 15 || !isAlphanumeric($('#rec_pipeline #hired_label').val())) {
    is_valid = false;
    $('#rec_pipeline #hired_label').parent().find('.error-message').show();
  }

  // if(($('#rec_pipeline #rejected_label').val().length < 1) || $('#rec_pipeline #rejected_label').val().length > 15 || !isAlphanumeric($('#rec_pipeline #rejected_label').val())) {
  //   is_valid = false;
  //   $('#rec_pipeline #rejected_label').parent().find('.error-message').show();
  // }



  if(is_valid == true) {

    // get the values if validation was successful

    var data = {};

    data.shortlisted_label  = "Shortlisted";
    data.phone_screen_label = $('#rec_pipeline #phone_screen_label').val();
    data.interview_label    = $('#rec_pipeline #interview_label').val();
    data.offer_label        = $('#rec_pipeline #offer_label').val();
    data.hired_label        = $('#rec_pipeline #hired_label').val();
    data.rejected_label     = "Rejected";

    $('.item-preloader').show();
    //send ajax request
      $.ajax({
          data: ({
            action : 'ai_update_rec_settings',
            params: data,
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {
        $('.item-preloader').hide();
        location.reload();
          console.log(data);
          console.log(msg);
      });



  }

});


$(document).on('click', '#edit_job_details_trigger', function(){

  var job_id = $(this).attr('data-id');

  if(job_id > 0) {
    // job id will be also checked on the back end
    $('.edit_job_details').modal('show');
    $('.modal-content .updated').hide();
    $('.modal').find('.loader').show();
    $('#update_job_details').attr('data-job-id', job_id);
    var data = {};

    data.id = job_id;

        $.ajax({
            data: ({
            action : 'ai_get_job_details',
            params: data,
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {
        $('.item-preloader').hide();
        var output = JSON.parse(msg);
          console.log(data);
          console.log(output.job.job_description);
          var stripped = output.job.job_description.replace(/\0/g, '0').replace(/\\(.)/g, "$1");
          $('.modal').find('.note-editable').html(stripped);
          $('.modal').find('.loader').hide();
      });
  }

});


$(document).on('click', '#update_job_details', function(){

  var data = {};
  data.text = $('.summernote').code();
  data.job_id = $('#update_job_details').attr('data-job-id');

      $('.modal').find('.loader').show();
     $('.modal-content .updated').hide();
        $.ajax({
            data: ({
            action : 'ai_update_job_details',
            params: data,
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {
          console.log(msg);
          $('.modal').find('.loader').hide();
          $('.modal-content .updated').show();
          setTimeout(function() {
            $('.edit_job_details').modal('hide'); }
            , 1300);

      });


});



$(document).on('click', '#move_to_live', function(){

  var data = {};
  data.status = 'pending';
  data.job_id = $(this).attr('data-id');

     $('.loader.status-update').css('display', 'inline');
        $.ajax({
            data: ({
            action : 'ai_update_job_status',
            params: data,
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {

          console.log(data);
          $('.status-wrapper .status').html('pending');
          $('.title-row #move_to_live').remove();
          $('.title-row .btn-wrapper').append('<button class="btn btn-xs btn-success" data-id="'+ data.job_id +'" id="move_to_archive"><i class="fa fa-archive"></i> Move to Archive</button>');
          $('.loader.status-update').css('display', 'none');

      });


});


$(document).on('click', '#move_to_archive', function(){

  var data = {};
  data.status = 'archive';
  data.job_id = $(this).attr('data-id');
  //data.can_make_live = $(this).parent().attr('data-can');
  data.can_make_live = 1;

     $('.loader.status-update').css('display', 'inline');
        $.ajax({
            data: ({
            action : 'ai_update_job_status',
            params: data,
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {
          console.log(data);
          $('.status-wrapper .status').html('archive');
          $('.title-row #move_to_archive').remove();
          if(data.can_make_live == 1) {

            $('.title-row .btn-wrapper').append('<button class="btn btn-xs btn-success" data-id="'+ data.job_id +'" id="move_to_live"><i class="fa fa-rocket"></i> Make Live</button>');

          } else {

            $('.title-row .btn-wrapper').append('<button class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="left" title="You have reached your live job limit under this plan."><i class="fa fa-star-o"></i> Upgrade</button>');
          }

          $('.loader.status-update').css('display', 'none');

      });

});





//-----------------------------------
// Load More Candidates - Manage job

$(document).on('click', '#load-more-candidates', function(){

  var button = $(this);
  //preloader show
  $('.load-more-button-wrapper .loader').show();

  //increase load limit by 5
  var params = {};

  params.live_loaded = $(this).attr('data-live-loaded');
  params.rejected_loaded = $(this).attr('data-rejected-loaded');

  params.live_total = $(this).attr('data-live-total');
  params.rejected_total = $(this).attr('data-rejected-total');

  //default in case something goes wrong
  params.limit = 10;

  // get the limit parameter
  if( parseInt(params.live_loaded) >= parseInt(params.rejected_loaded) ) {
    params.limit = parseInt(params.live_loaded) + 5;
  } else {
    params.limit = parseInt(params.rejected_loaded) + 5;
  }

  // One more step to secure everything from crashing
  if( isNaN(params.limit)) {
    params.limit = 10;
  }

  // getting job id from edit job details trigger button
  params.job_id = $('#edit_job_details_trigger').attr('data-id');

  //send ajax request
    $.ajax({
        data: ({
          action : 'ai_load_more_candidates',
          params: params,
          }),
          type: 'POST',
          async: true,
          url: aiAjax,
          })

    // On done return response
    .done(function( msg ) {

        var output = JSON.parse(msg);
        $('.load-more-button-wrapper .loader').hide();

        if(parseInt(output.live_loaded) > 0) {
            $('#live-list .mCSB_container').html(output.live_html);
        }
        if(parseInt(output.rejected_loaded) > 0) {
           $('#live-list .mCSB_container').html(output.live_html);
        }

        button.attr('data-live-loaded', output.live_loaded);
        button.attr('data-rejected-loaded', output.rejected_loaded);


        if((parseInt(output.live_loaded) >= parseInt(output.live_total)) && (parseInt(output.rejected_loaded) >= parseInt(output.rejected_total))) {
          button.hide();
        }
        console.log(params);
        console.log(output);
    });


});


// Load More Candidates - job management
//-----------------------------------




//------------------------------------
// load candidate data

$(document).on('click', '.candidate-card', function(e){

  // prevent default action for link click
  e.preventDefault();
  $('.candidate-card').removeClass('active');
  $(this).addClass('active');
  var data = {};

  data.user_id    = $(this).attr('data-user');
  data.profile_id = $(this).attr('data-profile');
  data.job_id     = $('#edit_job_details_trigger').attr('data-id');

 $('.candidate-details-wrapper .loader').show();
 $('.candidate-details-wrapper .candidate-placeholder').hide();

  $.ajax({
    data: ({
    action : 'ai_load_candidate_data',
    params: data,
    }),
    type: 'POST',
    async: true,
    url: aiAjax,
    })
  // On done return response
  .done(function( msg ) {
      console.log(data);
      var output = JSON.parse(msg);
      console.log(output.raw);
      $('.candidate-details-wrapper .loader').hide();
      $('.candidate-details-wrapper #candaidate-details').html(output.html);

      var d = $(document).find('.conversation');
      d.scrollTop(d.prop("scrollHeight"));

  });

});

// load candidate data
//------------------------------------



//------------------------------------
// Move candidate through pipeline


$(document).on('click', '.move-through-pipeline>li>a', function(e){

  // prevent default action for link click
  e.preventDefault();

  var data = {};

  data.job_id        = $('#edit_job_details_trigger').attr('data-id');
  data.profile_id    = $('.move-through-pipeline').attr('data-profile-id');
  data.pipeline_step = $(this).attr('data-next');

  data.previous_step = $('.toolbar .candidate_pipeline_step').attr('data-step');

  data.pipeline_alias_new = $(this).html();

  $('.toolbar .candidate_pipeline_step').html(data.pipeline_alias_new);
  $('.toolbar .candidate_pipeline_step').attr('data-step', data.pipeline_step);

  $(this).parent().prevAll().remove();
  $(this).parent().remove();

  if(data.pipeline_step == 'rejected') {

    var number_of_live = $('#live .candidate-card').length;


    $('#rejected .list-group .mCSB_container').append($('#live .candidate-card.active'));

    if(number_of_live == 1) {
      $('#live .candidate-card.active').remove();
      if($('#rejected .candidate-placeholder').length > 0) {
        $('#rejected .candidate-placeholder').remove();
      }
      $('#live .list-group .mCSB_container').append('<div class="candidate-placeholder">You have 0 live candidates.</div>');
    }



    $('.toolbar .candidate_pipeline_step').addClass('rejected-red');
    $('.move-through-pipeline').parent('.btn-group').remove();


  }

  // find the cell moved from

  data.prev_cell_class = '.' + data.previous_step + '_cell';
  data.prev_cell_class_value = $('.job-card').find(data.prev_cell_class).find('.value').text();
  data.prev_cell_class_value = parseInt(data.prev_cell_class_value) - 1;
  $('.job-card').find(data.prev_cell_class).find('.value').text(data.prev_cell_class_value);

  //find cell moved to

  data.next_cell_class = '.' + data.pipeline_step + '_cell';
  data.next_cell_class_value = $('.job-card').find(data.next_cell_class).find('.value').text();
  data.next_cell_class_value = parseInt(data.next_cell_class_value) + 1;
  $('.job-card').find(data.next_cell_class).find('.value').text(data.next_cell_class_value);

  // make all the changes here

  $.ajax({
    data: ({
    action : 'ai_move_through_pipeline',
    params: data,
    }),
    type: 'POST',
    async: true,
    url: aiAjax,
    })
  // On done return response
  .done(function( msg ) {
      console.log(data);
      console.log(msg);
  });
});



// Move candidate through pipeline
//------------------------------------



//------------------------------------
// edit candidate tags
$(document).on('click', '#edit-candidate-tags', function(){

  var pool_id = $(this).attr('data-pool-id');
  var profile_id = $('.candidate-card.active').attr('data-profile');
  $('.modal-content .updated').hide();
  // clean up the form
  $('.edit_candidate_tags').find('#tag-one').val('');
  $('.edit_candidate_tags').find('#tag-two').val('');
  $('.edit_candidate_tags').find('#tag-three').val('');

  if(profile_id > 0) {

    $('#update_tags').attr('data-profile-id', profile_id);

    if(pool_id > 0) {

      // then we need to load the tags from the pool
      $('.edit_candidate_tags').modal('show');
      $('#update_tags').attr('data-pool-id', pool_id);

      // tags are already loaded into the ui so just parse the ui.

      var tags = {};
      tags.one = $(document).find('.tags').children('.tag').eq(0).text();
      tags.two = $(document).find('.tags').children('.tag').eq(1).text();
      tags.three = $(document).find('.tags').children('.tag').eq(2).text();



      $('.edit_candidate_tags').find('#tag-one').val(tags.one);
      $('.edit_candidate_tags').find('#tag-two').val(tags.two);
      $('.edit_candidate_tags').find('#tag-three').val(tags.three);

      $('.modal').find('.loader').hide();
      console.log(tags);

    } else {

      // then we need to create a pool. no need to load the tags. just open the form
      $('.edit_candidate_tags').modal('show');
      $('.modal').find('.loader').hide();
      $('#update_tags').attr('data-pool-id', pool_id);
    }

  }

});


// now create or update talent pool

$(document).on('click', '#update_tags', function(){

  var data = {};
  data.profile_id = $(this).attr('data-profile-id');
  data.pool_id = $(this).attr('data-pool-id');

  data.tag_one = $('.edit_candidate_tags').find('#tag-one').val();
  data.tag_two = $('.edit_candidate_tags').find('#tag-two').val();
  data.tag_three = $('.edit_candidate_tags').find('#tag-three').val();




  if(data.profile_id > 0) {

    if(data.pool_id > 0) {

      // update pool
      data.action = 'update';

    } else {

      // create pool
      data.action = 'create';
    }

  $('.modal').find('.loader').show();
  $('.modal-content .updated').hide();

        $.ajax({
            data: ({
            action : 'ai_update_candidate_tags',
            params: data,
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {

          var output = JSON.parse(msg);
          console.log(output.id);

          $(document).find('#edit-candidate-tags').attr('data-pool-id', output.id);
          // populate tags

          data.tagsHtml = '';

          if(data.tag_one.replace(/\s/g, '').length > 0) {

              data.tagsHtml = data.tagsHtml + "<span class='tag'>"+data.tag_one+"</span>";
          }

          if(data.tag_two.replace(/\s/g, '').length > 0) {

              data.tagsHtml = data.tagsHtml + "<span class='tag'>"+data.tag_two+"</span>";
          }

          if(data.tag_three.replace(/\s/g, '').length > 0) {

              data.tagsHtml = data.tagsHtml + "<span class='tag'>"+data.tag_three+"</span>";
          }

          $(document).find('.tags').html(data.tagsHtml);

          console.log(msg);

          $('.modal').find('.loader').hide();
          $('.modal-content .updated').show();

          setTimeout(function() {
            $('.edit_candidate_tags').modal('hide'); }
            , 1300);


      });




  } else {console.log('failed');}


});


//----------------------------------------------------
// messaging on job management page

  var maxchars = 1500;

  $(document).on('keyup', '#msg-text',function () {
      var tlength = $(this).val().length;
      $(this).val($(this).val().substring(0, maxchars));
      var tlength = $(this).val().length;
      var remain = maxchars - parseInt(tlength);
      $(document).find('.chars-num').text(remain);

  });


function scroll_to_bottom() {

  var d = $(document).find('.conversation');
  d.scrollTop(d.prop("scrollHeight"));
}




function escapeHtml(text) {
    'use strict';
    return text.replace(/[\"&<>]/g, function (a) {
        return { '"': '&quot;', '&': '&amp;', '<': '&lt;', '>': '&gt;' }[a];
    });

}

function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}



$(document).on('click', '#send-btn', function() {

  if(escapeHtml($('#msg-text').val()).length > 0) {

    var sender_name = $(document).find("#send-btn").attr('data-sender-name');


    $('.conversation .timeline-wrapper')
    .append("<div class='item out'><div class='timeline'><div class='circle'></div><div class='line'></div></div><div class='item-content'><div class='panel panel-default'><div class='panel-body'><b>"+sender_name + "  (sending...)</b><br>"+ nl2br (escapeHtml($('#msg-text').val())) +"</div></div></div></div>");

    $('.conversation  .timeline-wrapper .no-messages').remove();
    var message = nl2br (escapeHtml($('#msg-text').val()));

    $('#msg-text').val("");
    $('.chars-num').text("1500");
    scroll_to_bottom();

    // make ajax request
    var data = {};

    data.recipient = $(document).find("#send-btn").attr('data-recipient');
    data.subject = $('#job-title').text();
    data.body = message;
    console.log(data);

    $.ajax({
        data: ({
          action : 'og_send_message',
          params: data,
          }),
          type: 'POST',
          async: true,
          url: aiAjax,
          })
    // On done return response
    .done(function( msg ) {
          console.log(msg);
          var number_of_items = $('.conversation .timeline-wrapper .item').length;
          var sdata = {};

          sdata.recipient = data.recipient;
          sdata.subject = data.subject;
          sdata.limit = number_of_items;
          sdata.offset = 0;

          console.log(sdata);

          $.ajax({
            data: ({
              action : 'og_get_conversation',
              params: sdata,
              }),
              type: 'POST',
              async: true,
              url: aiAjax,
            })
            // On done return response
            .done(function( msg ) {
              var output = JSON.parse(msg);
              console.log(msg);
              $('.conversation .timeline-wrapper').html(output.data);
            });
    });
  }
});



$(document).on('click', '#load-older-msgs', function() {

          var sdata = {};

          sdata.recipient = $(document).find("#send-btn").attr('data-recipient');
          sdata.subject = $('#job-title').text();;
          sdata.limit = ($('.conversation .timeline-wrapper .item').length) + 10;
          sdata.offset = 0;

          console.log(sdata);
          $.ajax({
                data: ({
                  action : 'og_get_conversation',
                  params: sdata,
                  }),
                  type: 'POST',
                  async: true,
                  url: aiAjax,
                  })
            // On done return response
            .done(function( msg ) {
              var output = JSON.parse(msg);

                  console.log(msg);

                  $('.conversation  .timeline-wrapper').html(output.data);

                  if(output.total > $('.conversation  .timeline-wrapper .item').length) {
                    $("#load-older-msgs").show();
                  } else {
                    $("#load-older-msgs").hide();
                  }
            });

  });


// messaging on job management page
//----------------------------------------------------




//----------------------------------------------------
// load more talent pool items on talent pool page




$(document).on('click', '#load-more-pool', function(){

  //preloader show
  $('.cards-wrapper .loader').show();

  //increase load limit by 5
  var params = {};
  params.limit = parseInt($('.counter .loaded').html()) + 5;
  if( isNaN(params.limit)) {
    params.limit = 15;
  }
  params.query = '';
  params.order = 'DESC';
  params.sort_by = 'id';

  if($('#search-pool').length > 0) {
    params.query = $('#search-pool').val();
  }

    if($('.sorter.asc').length > 0 || $('.sorter.desc').length > 0) {
      // we have sorting activated
      if($('.sorter.asc').length > 0) {
        params.order = 'asc';
        params.sort_by = $('.sorter.asc').data('factor');
      }

      if($('.sorter.desc').length > 0) {
        params.order = 'desc';
        params.sort_by = $('.sorter.desc').data('factor');
      }
   }

  //send ajax request
    $.ajax({
        data: ({
          action : 'ai_load_more_pool',
          params: params,
          }),
          type: 'POST',
          async: true,
          url: aiAjax,
          })
    // On done return response
    .done(function( msg ) {

        var output = JSON.parse(msg);
        $('.talent-pool .cards-wrapper .list-group').html(output.html);
        $('.talent-pool .loader').hide();

        if(output.current >= output.total) {
          $('.counter .loaded').html(output.total);
          $('#load-more-pool').hide();
        } else {
          $('.counter .loaded').html(params.limit);
          $('#load-more-pool').show();
        }
        console.log(params);
        console.log(output);
    });


});









// output sort results
$('.talent-pool .sorter').click(function(){


    if($(this).hasClass("asc")) {

        $(".sorter").each(function(index, value) {
        $(this).removeClass("asc").removeClass("desc");

      });

      $(this).removeClass("asc").addClass("desc");

    } else if ($(this).hasClass("desc")) {

        $(".sorter").each(function(index, value) {
        $(this).removeClass("asc").removeClass("desc");

      });

      $(this).removeClass("asc").removeClass("desc");

    } else {

        $(".sorter").each(function(index, value) {
        $(this).removeClass("asc").removeClass("desc");

      });

      $(this).addClass("asc");
    }

    // now output the results
  //preloader show
  $('.cards-wrapper .loader').show();

  //increase load limit by 5
  var params = {};
  params.limit = parseInt($('.counter .loaded').html());
  if( isNaN(params.limit)) {
    params.limit = 10;
  }
  params.query = '';
  params.order = 'DESC';
  params.sort_by = 'id';

  if($('#search-pool').length > 0) {
    params.query = $('#search-pool').val();
  }

    if($('.sorter.asc').length > 0 || $('.sorter.desc').length > 0) {
      // we have sorting activated
      if($('.sorter.asc').length > 0) {
        params.order = 'asc';
        params.sort_by = $('.sorter.asc').data('factor');
      }

      if($('.sorter.desc').length > 0) {
        params.order = 'desc';
        params.sort_by = $('.sorter.desc').data('factor');
      }
   }

  //send ajax request
    $.ajax({
        data: ({
          action : 'ai_load_more_pool',
          params: params,
          }),
          type: 'POST',
          async: true,
          url: aiAjax,
          })
    // On done return response
    .done(function( msg ) {

        var output = JSON.parse(msg);
        $('.talent-pool .cards-wrapper .list-group').html(output.html);
        $('.talent-pool .loader').hide();

        if(output.current >= output.total) {
          $('.counter .loaded').html(output.total);
          $('#load-more-pool').hide();
        } else {
          $('.counter .loaded').html(params.limit);
          $('#load-more-pool').show();
        }
        console.log(params);
        console.log(output);
    });


 });






$('#search-pool-trigger').click(function(){

  //preloader show
  $('.cards-wrapper .loader').show();

  //increase load limit by 5
  var params = {};
  params.limit = parseInt($('.counter .loaded').html());

  if(params.limit < 10) {
    params.limit = 10;
  }
  if( isNaN(params.limit)) {
    params.limit = 10;
  }
  params.query = '';
  params.order = 'DESC';
  params.sort_by = 'id';

  if($('#search-pool').length > 0) {
    params.query = $('#search-pool').val();
  }

    if($('.sorter.asc').length > 0 || $('.sorter.desc').length > 0) {
      // we have sorting activated
      if($('.sorter.asc').length > 0) {
        params.order = 'asc';
        params.sort_by = $('.sorter.asc').data('factor');
      }

      if($('.sorter.desc').length > 0) {
        params.order = 'desc';
        params.sort_by = $('.sorter.desc').data('factor');
      }
   }

  //send ajax request
    $.ajax({
        data: ({
          action : 'ai_load_more_pool',
          params: params,
          }),
          type: 'POST',
          async: true,
          url: aiAjax,
          })
    // On done return response
    .done(function( msg ) {

        var output = JSON.parse(msg);
        $('.talent-pool .cards-wrapper .list-group').html(output.html);
        $('.talent-pool .loader').hide();

        if(output.current >= output.total) {
          $('.counter .loaded').html(output.total);
          $('#load-more-pool').hide();
        } else {
          $('.counter .loaded').html(params.limit);
          $('#load-more-pool').show();
        }
        console.log(params);
        console.log(output);
    });

});




// load more talent pool items on talent pool page
//-----------------------------------------------------




//-----------------------------------------------------
// Edit talent pool tags


$(document).on('click', '.edit-pool-tags', function(){

  var tagTrigger = $(this);

  var pool_id = $(this).attr('data-pool-id');
  var profile_id = $(this).attr('data-profile-id');
  var triggerClass = '.' + $(this).attr('data-pool-slug');

  $('#update_pool_tags').attr('data-trigger-class', $(this).attr('data-pool-slug'));

  $('.modal-content .updated').hide();
  // clean up the form
  $('.edit_candidate_tags').find('#tag-one').val('');
  $('.edit_candidate_tags').find('#tag-two').val('');
  $('.edit_candidate_tags').find('#tag-three').val('');

  if(profile_id > 0) {

    $('#update_pool_tags').attr('data-profile-id', profile_id);

    if(pool_id > 0) {

      // then we need to load the tags from the pool
      $('.edit_candidate_tags').modal('show');
      $('#update_pool_tags').attr('data-pool-id', pool_id);

      // tags are already loaded into the ui so just parse the ui.

      var tags = {};

      tags.one = $(document).find(triggerClass).find('.tags-container').children('.tag').eq(0).text();
      tags.two = $(document).find(triggerClass).find('.tags-container').children('.tag').eq(1).text();
      tags.three = $(document).find(triggerClass).find('.tags-container').children('.tag').eq(2).text();


      $('.edit_candidate_tags').find('#tag-one').val(tags.one);
      $('.edit_candidate_tags').find('#tag-two').val(tags.two);
      $('.edit_candidate_tags').find('#tag-three').val(tags.three);

      $('.modal').find('.loader').hide();

      console.log(tags);

    } else {

      console.log('failed to load modal correctly!');

    }

  }

});


// now create or update talent pool

$(document).on('click', '#update_pool_tags', function(){

  var data = {};
  data.profile_id = $(this).attr('data-profile-id');
  data.pool_id = $(this).attr('data-pool-id');

  data.tag_one = $('.edit_candidate_tags').find('#tag-one').val();
  data.tag_two = $('.edit_candidate_tags').find('#tag-two').val();
  data.tag_three = $('.edit_candidate_tags').find('#tag-three').val();


  if(data.profile_id > 0) {

    if(data.pool_id > 0) {

      // update pool
      data.action = 'update';

    } else {

      // create pool
      data.action = 'create';
    }

  $('.modal').find('.loader').show();
  $('.modal-content .updated').hide();

        $.ajax({
            data: ({
            action : 'ai_update_candidate_tags',
            params: data,
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {

          var output = JSON.parse(msg);
          console.log(output.id);

          //$(document).find('#edit-candidate-tags').attr('data-pool-id', output.id);
          // populate tags

          data.tagsHtml = '';

          if(data.tag_one.replace(/\s/g, '').length > 0) {

              data.tagsHtml = data.tagsHtml + "<span class='tag'>"+data.tag_one+"</span>";
          }

          if(data.tag_two.replace(/\s/g, '').length > 0) {

              data.tagsHtml = data.tagsHtml + "<span class='tag'>"+data.tag_two+"</span>";
          }

          if(data.tag_three.replace(/\s/g, '').length > 0) {

              data.tagsHtml = data.tagsHtml + "<span class='tag'>"+data.tag_three+"</span>";
          }
          var triggerClass = "."+$('#update_pool_tags').attr('data-trigger-class');
          $(document).find(triggerClass).find('.tags-container').html(data.tagsHtml);

          console.log(msg);

          $('.modal').find('.loader').hide();
          $('.modal-content .updated').show();

          setTimeout(function() {
            $('.edit_candidate_tags').modal('hide'); }
            , 1300);


      });

  } else {console.log('failed');}


});




$(document).on('click', '.remove-from-pool', function(){

  var data = {};
  data.profile_id = $(this).parent().find('#update_pool_tags').attr('data-profile-id');
  data.pool_id = $(this).parent().find('#update_pool_tags').attr('data-pool-id');

  if(data.profile_id > 0) {

    if(data.pool_id > 0) {

      // update pool
      data.action = 'update';

    } else {

      // create pool
      data.action = 'create';
    }

  $('.modal').find('.loader').show();
  $('.modal-content .updated').hide();

        $.ajax({
            data: ({
            action : 'ai_remove_candidate_from_pool',
            params: data,
            }),
            type: 'POST',
            async: true,
            url: aiAjax,
            })
      // On done return response
      .done(function( msg ) {

          var output = JSON.parse(msg);
          console.log(output);
          var triggerClass = "."+$('#update_pool_tags').attr('data-trigger-class');
          $(document).find(triggerClass).remove();
          $('.counter .loaded').html(parseInt($('.counter .loaded').html())-1);
          $('.counter .total').html(parseInt($('.counter .total').html())-1);
          console.log(msg);
          $('.modal').find('.loader').hide();
          $('.edit_candidate_tags').modal('hide');
      });




  } else {console.log('failed');}


});


// Edit talent pool tags
//-----------------------------------------------------







//-----------------------------------------------------
// Load talent profile in talent pool

$(document).on('click', '.show-pool-profile', function(){


  $('.load-talent-profile').modal('show');
  $('.talent-details-modal-body').css('min-height', '500px');

  $('.load-talent-profile .talent-details-wrapper').html('');

  var data = {};
  data.profile_id = $(this).attr('data-profile-id');
  console.log(data);

  $.ajax({
    data: ({
    action : 'ai_load_profile_from_pool',
    params: data,
    }),
    type: 'POST',
    async: true,
    url: aiAjax,
    })
    .done(function( msg ) {

        var output = JSON.parse(msg);
        console.log(output);

        console.log(msg);
        $('.load-talent-profile .talent-details-wrapper').html(output.html);
        $('.modal').find('.loader').hide();
    });

});



// Load talent profile in talent pool
//-----------------------------------------------------




//-----------------------------------------------------
// Load talent profile on admin page

$(document).on('click', '.admin-view-profile-button', function(){


  $('.load-talent-profile').modal('show');
  $('.talent-details-modal-body').css('min-height', '500px');

  $('.load-talent-profile .talent-details-wrapper').html('');

  var data = {};
  data.profile_id = $(this).attr('data-profile-id');
  console.log(data);

  $.ajax({
    data: ({
    action : 'ai_load_profile_from_pool',
    params: data,
    }),
    type: 'POST',
    async: true,
    url: aiAjax,
    })
    .done(function( msg ) {

        var output = JSON.parse(msg);
        console.log(output);

        console.log(msg);
        $('.load-talent-profile .talent-details-wrapper').html(output.html);
        $('.modal').find('.loader').hide();
    });

});



// Load talent profile on admin page
//-----------------------------------------------------



//-----------------------------------------------------
// Update job from admin page

$(document).on('click', '#make_job_live', function(){



  // everything is double checked on the back end
  var data = {};
  data.job_id = $('.title-row').attr('data-job-id');
  data.job_title = $('#job-title').val();
  data.company = $('#company').val();
  data.job_description = $('#job-description').code();
  data.capabilities = [];
  data.job_status = $('#status-update').val();
    $(".test-node").each(function(){
       data.capabilities.push([$(this).find('.capability').val(),$(this).find('.capability').data('capability-id')]);
    });
  console.log(data);

  $.ajax({
    data: ({
    action : 'ai_moderate_job',
    params: data,
    }),
    type: 'POST',
    async: true,
    url: aiAjax,
    })
    .done(function( msg ) {
        console.log(msg);
        location.reload();
    });

});




// Update job from admin page
//-----------------------------------------------------



//-----------------------------------------------------
// Leave shared account

$(document).on('click', '.shared-accounts .leave-account', function(){

  // everything is double checked on the back end
  var data = {};
  data.account_id = $(this).attr('data-account-id');

  $(this).parent().find('.item-preloader').show();

  console.log(data);

  $.ajax({
    data: ({
    action : 'ai_leave_account',
    params: data,
    }),
    type: 'POST',
    async: true,
    url: aiAjax,
    })
    .done(function( msg ) {
        console.log(msg);
        $(this).parent().find('.item-preloader').hide();
        location.reload();
    });

});

// Leave shared account
//-----------------------------------------------------



//-----------------------------------------------------
// Create / Update my account and add users to account.

$(document).on('click', '#update_my_account', function(){

  // everything is double-checked on the back end
  var data = {};
  var is_valid = true;

  $('.email-error').remove();
  $('.error-message').hide();

  data.can_see_pool       =  $("input[type='radio'][name='can-view-pool']:checked").val();
  data.can_change_status  =  $("input[type='radio'][name='can-change-status']:checked").val();
  data.can_create_new     =  $("input[type='radio'][name='can-create-new']:checked").val();
  data.can_change_details =  $("input[type='radio'][name='can-change-details']:checked").val();

  data.user_emails = [];

  $("input[type='text'].user-email").each(function(){

      if(isValidEmailAddress($(this).val())) {

        data.user_emails.push($(this).val());

      } else {
        is_valid = false;
        $(this).parent().append('<div class="error-message email-error" style="display:block!important">please enter a valid email address</div>');

      }

  });

  if( isAlphanumeric($('#account_name').val()) && ($('#account_name').val().length > 2) && ($('#account_name').val().length < 21)) {

    data.account_title = $('#account_name').val();

  } else {
    is_valid = false;
    $('#account_name').parent().find('.error-message').show();
  }


  if(is_valid == true) {

    var preloader = $(this).parent().find('.item-preloader');
    preloader.show();

    console.log(data);

    $.ajax({
      data: ({
      action : 'ai_update_my_account',
      params: data,
      }),
      type: 'POST',
      async: true,
      url: aiAjax,
      })
      .done(function( msg ) {

        console.log(msg);

        preloader.hide();

        location.reload();

      });

  }


});

// Create / Update my account and add users to account.
//-----------------------------------------------------







//-----------------------------------------------------
// Update plan


// enable button only when new settings do not match current settings

// when switching between recurring and one off
$(document).on('change', 'input:radio[name="plan_type"]', function (event) {


   var data = {};
    data.change = '';

    data.plan_before = $('#plan-switch').attr('data-current');
    data.plan_after =  $('#plan-switch input:radio:checked').val();

    data.type_before = $('#plan-type').attr('data-current');
    data.type_after = $('#plan-type input:radio:checked').val();

    // identify change
    if(data.plan_before == 'smart' && (data.plan_after == 'genius' || data.plan_after == 'genius_plus')) {
      data.change = 'upgrade';
    } else if(data.plan_before == 'genius' && data.plan_after == 'genius_plus') {
      data.change = 'upgrade';
    } else if(data.plan_before == 'genius' && data.plan_after == 'smart') {
      data.change = 'downgrade';
    } else if(data.plan_before == 'genius_plus' && (data.plan_after == 'smart' || data.plan_after == 'genius')) {
        data.change = 'downgrade';
    } else {
      data.change = 'none';
    }


  if((data.type_before == data.type_after) && (data.plan_before == data.plan_after)) {
    $('#update-membership').attr("disabled", true);
  } else {
    $('#update-membership').attr("disabled", false);
  }

    console.log(data);
});

// when switching between plans
$(document).on('change', 'input:radio[name="plan_slug"]', function (event) {

    $('#plan-type #recurring').click();

    $('.next-plan-span .plan').removeClass('smart').removeClass('genius').removeClass('genus-plus');
    if($('#plan-switch input:radio:checked').val() == 'smart') {
      $('.next-plan-span .plan').addClass('smart');
      $('.next-plan-span .plan').html('Smart');
    }
    if($('#plan-switch input:radio:checked').val() == 'genius') {
      $('.next-plan-span .plan').addClass('genius');
      $('.next-plan-span .plan').html('Genius');
    }
    if($('#plan-switch input:radio:checked').val() == 'genius_plus') {
      $('.next-plan-span .plan').addClass('genius-plus');
      $('.next-plan-span .plan').html('Genius Plus');
    }
    //check if we need to show the plan type
    if($(this).val() == 'smart') {
      $('.plan-type-selector').hide();
    } else {
      $('.plan-type-selector').show();
    }

   var data = {};
    data.change = '';

    data.plan_before = $('#plan-switch').attr('data-current');
    data.plan_after =  $('#plan-switch input:radio:checked').val();

    data.type_before = $('#plan-type').attr('data-current');
    data.type_after = $('#plan-type input:radio:checked').val();

    // identify change
    if(data.plan_before == 'smart' && (data.plan_after == 'genius' || data.plan_after == 'genius_plus')) {
      data.change = 'upgrade';
    } else if(data.plan_before == 'genius' && data.plan_after == 'genius_plus') {
      data.change = 'upgrade';
    } else if(data.plan_before == 'genius' && data.plan_after == 'smart') {
      data.change = 'downgrade';
    } else if(data.plan_before == 'genius_plus' && (data.plan_after == 'smart' || data.plan_after == 'genius')) {
        data.change = 'downgrade';
    } else {
      data.change = 'none';
    }


  if((data.type_before == data.type_after) && (data.plan_before == data.plan_after)) {
    $('#update-membership').attr("disabled", true);
  } else {
    $('#update-membership').attr("disabled", false);
  }

    console.log(data);
});



$(document).on('click', '#update-membership', function(){

// check if we need to disable active jobs

     var data = {};
    data.change = '';

    data.plan_before = $('#plan-switch').attr('data-current');
    data.plan_after =  $('#plan-switch input:radio:checked').val();

    data.type_before = $('#plan-type').attr('data-current');
    data.type_after = $('#plan-type input:radio:checked').val();

    // identify change
    if(data.plan_before == 'smart' && (data.plan_after == 'genius' || data.plan_after == 'genius_plus')) {
      data.change = 'upgrade';
    } else if(data.plan_before == 'genius' && data.plan_after == 'genius_plus') {
      data.change = 'upgrade';
    } else if(data.plan_before == 'genius' && data.plan_after == 'smart') {
      data.change = 'downgrade';
    } else if(data.plan_before == 'genius_plus' && (data.plan_after == 'smart' || data.plan_after == 'genius')) {
        data.change = 'downgrade';
    } else {
      data.change = 'none';
    }

  console.log(data);

  var urlParams = $.param( data );

  location.href = '/payment-details/?' + urlParams;

});

// Update plan
//-----------------------------------------------------




//-----------------------------------------------------
// Detele user account


$(document).on('click', '#delete_my_account', function(){

    var data = {};

    $('.deleting-data-preloader').show();
    $.ajax({
    data: ({
    action : 'ai_delete_user_account',
    params: data,
    }),
    type: 'POST',
    async: true,
    url: aiAjax,
    })
    .done(function( msg ) {
        console.log(msg);
        $('.deleting-data-preloader').hide();
        location.href = '/goodbye';
    });



});

// Detele user account
//-----------------------------------------------------




}); // document ready..
})( jQuery );
