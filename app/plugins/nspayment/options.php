<?php
// This is an admin page..
/**
 * I will need several options
 */



// Add Menu Pages and submenu pages. 

function setup_nspayment_admin_menus() {
    

  	add_menu_page('Payment Options', //$page_title is the title of the page to be added
			'Payment Options', // $menu_title is the title shown in the menu (often a shorter version of $page_title
			'manage_options', //$capability is the minimum capability required from a user in order to have access to this menu.
			'payment-options', // $menu_slug is a unique identifier for the menu being created
			'payment_settings'); //$function is the name of a function that is called to handle (and render) this menu page
  
}
 
// This tells WordPress to call the function named "setup_nsauth_admin_menus"
// when it's time to create the menu pages.
add_action("admin_menu", "setup_nspayment_admin_menus");


//Add function generating output of the page.

function payment_settings() {

	// Check that the user is allowed to update options
	if (!current_user_can('manage_options')) {
	    wp_die('You do not have sufficient permissions to access this page.');
	}


$genius_plan_id = get_option("nspay_genius_plan_id");

$genius_single_price = get_option("nspay_genius_single_price");

$genius_plus_plan_id = get_option("nspay_genius_plus_plan_id");

$genius_plus_single_price = get_option("nspay_genius_plus_single_price");



$merchant_id = get_option("nspay_merchant_id");

$public_key = get_option("nspay_public_key");

$private_key = get_option("nspay_private_key");


?>
<div class="wrap">
    <?php screen_icon('themes'); ?> <h2>Opengo Auth</h2>
 
    <form method="POST" action="">
        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <label for="genius_plan_id">
                        Genius Plan ID:
                    </label>
                </th>

                <td>
                    <input type="text" name="genius_plan_id" value="<?php echo $genius_plan_id;?>" size="100" />
                </td>

            </tr>    






            <tr valign="top">
                <th scope="row">
                    <label for="genius_single_price">
                        Genius One-Off Price (xx.xx):
                    </label>
                </th>

                <td>
                    <input type="text" name="genius_single_price" value="<?php echo $genius_single_price;?>" size="100" />
                </td>

            </tr>  


            <tr valign="top">
                <th scope="row">
                    <label for="genius_plus_plan_id">
                        Genius Plus Plan ID:
                    </label>
                </th>

                <td>
                    <input type="text" name="genius_plus_plan_id" value="<?php echo $genius_plus_plan_id;?>" size="100" />
                </td>

            </tr>  

<hr>



            <tr valign="top">
                <th scope="row">
                    <label for="genius_plus_single_price">
                        Genius Plus One-Off Price (xx.xx):
                    </label>
                </th>

                <td>
                    <input type="text" name="genius_plus_single_price" value="<?php echo $genius_plus_single_price;?>" size="100" />
                </td>

            </tr>  



            <tr valign="top">
                <th scope="row">
                    <label for="merchant_id">
                        Braintree Merchant ID:
                    </label>
                </th>

                <td>
                    <input type="text" name="merchant_id" value="<?php echo $merchant_id;?>" size="100" />
                </td>

            </tr>  


            <tr valign="top">
                <th scope="row">
                    <label for="public_key">
                        Braintree Public Key:
                    </label>
                </th>

                <td>
                    <input type="text" name="public_key" value="<?php echo $public_key;?>" size="100" />
                </td>

            </tr>  



            <tr valign="top">
                <th scope="row">
                    <label for="private_key">
                        Braintree Private Key:
                    </label>
                </th>

                <td>
                    <input type="text" name="private_key" value="<?php echo $private_key;?>" size="100" />
                </td>

            </tr>  





        </table>
 

        <input type="hidden" name="nspay_update_settings" value="Y" /> 
        <input type="submit" name="submit" id="submit" class="button button-primary" value="save"/> 
    </form>
     
 
</div>


<?php
}

// Fetching $_POST request with data and updating settings

if (isset($_POST["nspay_update_settings"])) {
    
    $genius_plan_id = esc_attr($_POST["genius_plan_id"]);   
	update_option("nspay_genius_plan_id", $genius_plan_id);


	$genius_single_price = esc_attr($_POST["genius_single_price"]);   
	update_option("nspay_genius_single_price", $genius_single_price);


	$genius_plus_plan_id = esc_attr($_POST["genius_plus_plan_id"]);   
	update_option("nspay_genius_plus_plan_id", $genius_plus_plan_id);




	$genius_plus_single_price = esc_attr($_POST["genius_plus_single_price"]);   
	update_option("nspay_genius_plus_single_price", $genius_plus_single_price);

	$merchant_id = esc_attr($_POST["merchant_id"]);   
	update_option("nspay_merchant_id", $merchant_id);


	$public_key = esc_attr($_POST["public_key"]);   
	update_option("nspay_public_key", $public_key);

	$private_key = esc_attr($_POST["private_key"]);   
	update_option("nspay_private_key", $private_key);


}