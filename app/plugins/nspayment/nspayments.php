<?php
/*
Plugin Name: nsPayments
Plugin URI: http://everycode.net
Description: Payments via braintree
Author: Nick Surmanidze
Version: 1.0 
Author URI: http://everycode.net
*/



// Load admin page
require_once( dirname( __FILE__ ). '/options.php' );

// Load ApiCaller
if ( !class_exists( 'ApiCaller' ) && file_exists( dirname( __FILE__ ) . '/lib/apicaller.class.php' ) ) {
    require_once( dirname( __FILE__ ) . '/lib/apicaller.class.php' );
}

// Load environment
require_once( dirname( __FILE__ ). '/_environment.php' );

// Load payment functions class
require_once( dirname( __FILE__ ). '/_nspay.class.php' );


?>
