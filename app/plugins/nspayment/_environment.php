<?php
require_once(dirname( __FILE__ ) . '/lib/Braintree.php');
Braintree_Configuration::environment('sandbox');
Braintree_Configuration::merchantId(esc_attr($_POST["merchant_id"]));
Braintree_Configuration::publicKey(esc_attr($_POST["public_key"]));
Braintree_Configuration::privateKey(esc_attr($_POST["private_key"]));
?>