<?php

@session_start();
require_once(__DIR__ . "/_config.php");

if(isset($_SESSION['nsauth'])) {

$_SESSION['nsauth'] = null;
	unset($_SESSION['nsauth']);	
} 

unset($_COOKIE);

header("location:" . NSAUTH_LOGOUT_URL);

exit; 