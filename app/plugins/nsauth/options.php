<?php
// This is an admin page..
/**
 * I will need several options
 */

# redirects  1. Login  2. Logout  3. Registration

# Linkedin   1. App ID   2. Secret

# Facebook   1. App ID   2. Secret


// Let's begin.




// Add Menu Pages and submenu pages. 

function setup_nsauth_admin_menus() {
    

  	add_menu_page('NSAuth Options', //$page_title is the title of the page to be added
			'nsAuth Options', // $menu_title is the title shown in the menu (often a shorter version of $page_title
			'manage_options', //$capability is the minimum capability required from a user in order to have access to this menu.
			'nsauth-options', // $menu_slug is a unique identifier for the menu being created
			'nsauth_settings'); //$function is the name of a function that is called to handle (and render) this menu page
  

	// add_submenu_page('nsauth-options', //$parent_slug is a unique identifier for the top menu page to which this submenu is added as a child.
	// 		'Front Page Elements', //$page_title is the title of the page to be added
	// 		'Front Page', // $menu_title is the title shown in the menu (often a shorter version of $page_title
	// 		'manage_options', //$capability is the minimum capability required from a user in order to have access to this menu.
	// 		'front-page-elements', // $menu_slug is a unique identifier for the menu being created
	// 		'nsauth_settings'); //$function is the name of a function that is called to handle (and render) this menu page

//parent slug can be:

// Dashboard: index.php
// Posts: edit.php
// Media: upload.php
// Links: link-manager.php
// Pages: edit.php?post_type=page
// Comments: edit-comments.php
// Appearance: themes.php
// Plugins: plugins.php
// Users: users.php
// Tools: tools.php
// Settings: options-general.php



}
 
// This tells WordPress to call the function named "setup_nsauth_admin_menus"
// when it's time to create the menu pages.
add_action("admin_menu", "setup_nsauth_admin_menus");




//Add function generating output of the page.

function nsauth_settings() {

	// Check that the user is allowed to update options
	if (!current_user_can('manage_options')) {
	    wp_die('You do not have sufficient permissions to access this page.');
	}



$nsauth_login_redirect = get_option("nsauth_login_redirect");

$nsauth_logout_redirect = get_option("nsauth_logout_redirect");

$nsauth_registration_redirect = get_option("nsauth_registration_redirect");

$facebook_id = get_option("facebook_id");

$facebook_secret = get_option("facebook_secret");

$linkedin_id = get_option("linkedin_id");

$linkedin_secret = get_option("linkedin_secret");


$og_app_id = get_option("og_app_id");

$og_app_key = get_option("og_app_key");

$og_api_url = get_option("og_api_url");

?>
<div class="wrap">
    <?php screen_icon('themes'); ?> <h2>Opengo Auth</h2>
 
    <form method="POST" action="">
        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <label for="nsauth_login_redirect">
                        Login Redirect URL:
                    </label>
                </th>

                <td>
                    <input type="text" name="nsauth_login_redirect" value="<?php echo $nsauth_login_redirect;?>" size="100" />
                </td>

            </tr>    






            <tr valign="top">
                <th scope="row">
                    <label for="nsauth_logout_redirect">
                        Logout Redirect URL:
                    </label>
                </th>

                <td>
                    <input type="text" name="nsauth_logout_redirect" value="<?php echo $nsauth_logout_redirect;?>" size="100" />
                </td>

            </tr>  


            <tr valign="top">
                <th scope="row">
                    <label for="nsauth_registration_redirect">
                        Registration Redirect URL:
                    </label>
                </th>

                <td>
                    <input type="text" name="nsauth_registration_redirect" value="<?php echo $nsauth_registration_redirect;?>" size="100" />
                </td>

            </tr>  

<hr>



            <tr valign="top">
                <th scope="row">
                    <label for="facebook_id">
                        Facebook App ID:
                    </label>
                </th>

                <td>
                    <input type="text" name="facebook_id" value="<?php echo $facebook_id;?>" size="100" />
                </td>

            </tr>  



            <tr valign="top">
                <th scope="row">
                    <label for="facebook_secret">
                        Facebook Secret:
                    </label>
                </th>

                <td>
                    <input type="text" name="facebook_secret" value="<?php echo $facebook_secret;?>" size="100" />
                </td>

            </tr>  


            <tr valign="top">
                <th scope="row">
                    <label for="linkedin_id">
                        Linkedin App ID:
                    </label>
                </th>

                <td>
                    <input type="text" name="linkedin_id" value="<?php echo $linkedin_id;?>" size="100" />
                </td>

            </tr>  



            <tr valign="top">
                <th scope="row">
                    <label for="linkedin_secret">
                        Linkedin Secret:
                    </label>
                </th>

                <td>
                    <input type="text" name="linkedin_secret" value="<?php echo $linkedin_secret;?>" size="100" />
                </td>

            </tr>  


<hr>



            <tr valign="top">
                <th scope="row">
                    <label for="og_app_id">
                        Opengo APP ID:
                    </label>
                </th>

                <td>
                    <input type="text" name="og_app_id" value="<?php echo $og_app_id;?>" size="100" />
                </td>

            </tr> 

            <tr valign="top">
                <th scope="row">
                    <label for="og_app_key">
                        Opengo APP ID:
                    </label>
                </th>

                <td>
                    <input type="text" name="og_app_key" value="<?php echo $og_app_key;?>" size="100" />
                </td>

            </tr> 


            <tr valign="top">
                <th scope="row">
                    <label for="og_api_url">
                        Opengo API URL:
                    </label>
                </th>

                <td>
                    <input type="text" name="og_api_url" value="<?php echo $og_api_url?>" size="100" />
                </td>

            </tr>             


        </table>
 

        <input type="hidden" name="nsauth_update_settings" value="Y" /> 
        <input type="submit" name="submit" id="submit" class="button button-primary" value="save"/> 
    </form>
     
 
</div>


<?php
}

// Fetching $_POST request with data and updating settings

if (isset($_POST["nsauth_update_settings"])) {
    
    $nsauth_login_redirect = esc_attr($_POST["nsauth_login_redirect"]);   
	update_option("nsauth_login_redirect", $nsauth_login_redirect);


	$nsauth_logout_redirect = esc_attr($_POST["nsauth_logout_redirect"]);   
	update_option("nsauth_logout_redirect", $nsauth_logout_redirect);


	$nsauth_registration_redirect = esc_attr($_POST["nsauth_registration_redirect"]);   
	update_option("nsauth_registration_redirect", $nsauth_registration_redirect);




	$facebook_id = esc_attr($_POST["facebook_id"]);   
	update_option("facebook_id", $facebook_id);

	$facebook_secret = esc_attr($_POST["facebook_secret"]);   
	update_option("facebook_secret", $facebook_secret);


	$linkedin_id = esc_attr($_POST["linkedin_id"]);   
	update_option("linkedin_id", $linkedin_id);

	$linkedin_secret = esc_attr($_POST["linkedin_secret"]);   
	update_option("linkedin_secret", $linkedin_secret);



    $og_app_id = esc_attr($_POST["og_app_id"]);   
    update_option("og_app_id", $og_app_id);

    $og_app_key = esc_attr($_POST["og_app_key"]);   
    update_option("og_app_key", $og_app_key);

    $og_api_url = sanitize_text_field($_POST["og_api_url"]);   
    update_option("og_api_url", $og_api_url);

}