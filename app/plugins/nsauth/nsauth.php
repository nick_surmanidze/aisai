<?php
/*
Plugin Name: nsAuth
Plugin URI: http://everycode.net
Description: Social Auth module. Facebook and Linkedin
Author: Nick Surmanidze
Version: 1.0 beta
Author URI: http://everycode.net
*/



// Load admin page
include_once( dirname( __FILE__ ). '/options.php' );

// Load ApiCaller
if ( !class_exists( 'ApiCaller' ) && file_exists( dirname( __FILE__ ) . '/lib/apicaller.class.php' ) ) {
    require_once( dirname( __FILE__ ) . '/lib/apicaller.class.php' );
}

// Load configuration
include_once( dirname( __FILE__ ). '/_config.php' );



// Create shortcode
function nsauth() {

	$out = '';
	//Generate facebook login URL
	require_once(dirname( __FILE__ )."/lib/fb/SDK/facebook.php"); //include facebook SDK

	######### Facebook API Configuration ##########
	$appId = NSAUTH_FB_ID; //Facebook App ID
	$appSecret = NSAUTH_FB_SECRET; // Facebook App Secret
	$return_url = NSAUTH_FACEBOOK_CALLBACK_URL;  //return url (url to script)
	$homeurl = NSAUTH_FACEBOOK_CALLBACK_URL;  //return to home
	$fbPermissions = 'email';  //Required facebook permissions

	$facebook = new Facebook(array(
	  'appId'  => $appId,
	  'secret' => $appSecret

	));
	$loginUrl = $facebook->getLoginUrl(array('redirect_uri'=>$homeurl,'scope'=>$fbPermissions));
	
	if(isset($_SESSION['nsauth'])) {
		// If user is logged in that show logout link.
		$out = "<a href='".home_url()."/dashboard' class='nsauth-btn btn btn-primary'>Dashboard</a><br>";
		$out .= "<a href='".NSAUTH_ROOT_DIR."logout.php' class='nsauth-btn btn btn-danger'>Logout</a><br>";
	} else {
		$out = "<a href='".NSAUTH_ROOT_DIR."lib/li/auth.php' class='linkedin-login btn btn-default'><i class='fa fa-linkedin'></i>Login with LinkedIn</a><br>";
		$out .= "<a href='".$loginUrl."' class='facebook-login btn btn-default'><i class='fa fa-facebook'></i>Login with Facebook</a>";
    }

	return $out;

}

add_shortcode( 'nsauth-buttons', 'nsauth' );

/**
 * Returns logout url via using a shortcode [nsauth-logout-url]
 */
function logout_url() {
	$logout_url = NSAUTH_ROOT_DIR . "logout.php";
	return $logout_url;
}

add_shortcode( 'nsauth-logout-url', 'logout_url' );

/**
 * Check if the user is logged in
 */
function nsauth_logged_in() {
	if(isset($_SESSION['nsauth'])) {
		return true;
	} else {
		return false;
	}
}

/**
 * returns and array with user data
 */
function nsauth_get_user() {
	if(!isset($_SESSION['nsauth'])) {
	return false;
	} else {
		$user = (array) $_SESSION['nsauth'];
		return $user;
	}
}



?>
