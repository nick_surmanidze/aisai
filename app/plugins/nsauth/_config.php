<?php
//Letting the plugin use necessary resources
$location = $_SERVER['DOCUMENT_ROOT'];
require_once ($location . '/wpcore/wp-load.php');

// load configurations

// APP ID AND SECRET KEY  for OPENGO API SERVER
define('OG_APP_ID', get_option("og_app_id") );
define('OG_APP_KEY', get_option("og_app_key") );
define('OG_API_URL', get_option("og_api_url") );


// APP ID AND SECRET KEY 
define('NSAUTH_LI_ID', get_option("linkedin_id") );
define('NSAUTH_LI_SECRET', get_option("linkedin_secret") );

// APP ID AND SECRET KEY 
define('NSAUTH_FB_ID', get_option("facebook_id") );
define('NSAUTH_FB_SECRET', get_option("facebook_secret"));


define('NSAUTH_PATH', __DIR__ );
define('NSAUTH_ROOT_DIR', plugin_dir_url( __FILE__ ) );
// http://example.com
define('NSAUTH_APP_URL', get_site_url());


// Logout redirect
define('NSAUTH_LOGOUT_URL', get_option("nsauth_logout_redirect"));

// Login redirect
define('NSAUTH_LOGIN_URL', get_option("nsauth_login_redirect"));

// Registration redirect
define('NSAUTH_REG_URL', get_option("nsauth_registration_redirect"));

define('NSAUTH_BASE_URL', NSAUTH_APP_URL . '/' );

// CALLBACK URLs
define('NSAUTH_LINKEDIN_CALLBACK_URL', NSAUTH_ROOT_DIR . 'lib/li/auth.php' );
define('NSAUTH_FACEBOOK_CALLBACK_URL', NSAUTH_ROOT_DIR . 'lib/fb/auth.php'  );

