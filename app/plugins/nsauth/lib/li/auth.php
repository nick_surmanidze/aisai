<?php
@session_start();

include_once("../../_config.php");
include_once("LinkedIn/http.php");
include_once("LinkedIn/oauth_client.php");



$baseURL = NSAUTH_BASE_URL;
$callbackURL = NSAUTH_LINKEDIN_CALLBACK_URL;
$linkedinApiKey = NSAUTH_LI_ID;
$linkedinApiSecret = NSAUTH_LI_SECRET;
$linkedinScope = 'r_basicprofile r_emailaddress';


if (isset($_GET["oauth_problem"]) && $_GET["oauth_problem"] <> "") {
  // in case if user cancel the login. redirect back to home page.
  $_SESSION["err_msg"] = $_GET["oauth_problem"];
  header("location:index.php");
  exit;
}

$client = new oauth_client_class;

$client->debug = false;
$client->debug_http = true;
$client->redirect_uri = $callbackURL;

$client->client_id = $linkedinApiKey;
$application_line = __LINE__;
$client->client_secret = $linkedinApiSecret;

if (strlen($client->client_id) == 0 || strlen($client->client_secret) == 0)
  die('Please go to LinkedIn Apps page https://www.linkedin.com/secure/developer?newapp= , '.
   'create an application, and in the line '.$application_line.
   ' set the client_id to Consumer key and client_secret with Consumer secret. '.
   'The Callback URL must be '.$client->redirect_uri).' Make sure you enable the '.
'necessary permissions to execute the API calls your application needs.';

/* API permissions
 */

$client->scope = $linkedinScope;

if (($success = $client->Initialize())) {

  if (($success = $client->Process())) {

    if (strlen($client->authorization_error)) {

      $client->error = $client->authorization_error;

      $success = false;

    } elseif (strlen($client->access_token)) {

      $success = $client->CallAPI(
       'http://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,location,picture-url,public-profile-url,formatted-name)', 
       'GET', array(
        'format'=>'json'
        ), array('FailOnAccessError'=>true), $user);

    }
  }

  $success = $client->Finalize($success);
}
if ($client->exit) exit;


if ($success) {

 //  print_r($_COOKIE['jobreg']);

	// print_r($user);

  $username = $user->emailAddress;
  $email = $user->emailAddress;
  $displayName = $user->formattedName;

  if(strlen($email) > 3) {
        // Only if we retrieved an email.


    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);


    $result = $apicaller->sendRequest(array(

      'action'       => 'read',
      'controller'   => 'auth',
      'source'       =>'social',
      'login'        => $username,
      'pass'         => '',
      'email'        => $email,
      'display_name' => $displayName,
      'role'         => 'recruiter'

      ));

    $result = (array) $result;

    if(isset($result['action']) && ( $result['action'] == 'login' || $result['action'] == 'register' )) {

      session_start();

      $_SESSION['nsauth'] = (array) $result;


      if($result['action'] == 'login') {

       header("location:" . NSAUTH_LOGIN_URL);
       exit; 

     } elseif ($result['action'] == 'register') {

       header("location:" . NSAUTH_REG_URL);
       exit; 

     }



   }
   else
   {
      // if no email, than no fun... Redirecting to root
     header("location:" . NSAUTH_APP_URL);
     exit; 
   }



 } else {

  echo "Sorry, something went wrong. Could not authenticate user.";        

}


} else {

  // Redirect to clean cache notice.
  header("location:clean.php");

  exit;
//echo "<a href='clean.php'>fix</a> ";

}
// header("location:index.php");
// exit;
?>

