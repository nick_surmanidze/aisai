<?php

include_once("../../_config.php");

include_once(__DIR__."/SDK/facebook.php"); //include facebook SDK

######### Facebook API Configuration ##########
$appId = NSAUTH_FB_ID; //Facebook App ID
$appSecret = NSAUTH_FB_SECRET; // Facebook App Secret
$return_url = NSAUTH_FACEBOOK_CALLBACK_URL;  //return url (url to script)
$homeurl = NSAUTH_FACEBOOK_CALLBACK_URL;  //return to home
$fbPermissions = 'email';  //Required facebook permissions

//Call Facebook API
$facebook = new Facebook(array(
  'appId'  => $appId,
  'secret' => $appSecret

));

$fbuser = $facebook->getUser();
//destroy facebook session if user clicks reset



if($fbuser){
		
	$user_profile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,locale');

	if(!empty($user_profile)){



		  $username = $user_profile['email'];
		  $email = $user_profile['email'];
		  $displayName = $user_profile['first_name'] . ' ' . $user_profile['last_name'];

		  if(strlen($email) > 3) {
		        // Only if we retrieved an email.


		    $apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		    $result = $apicaller->sendRequest(array(

		      'action'       => 'read',
		      'controller'   => 'auth',
		      'source'       =>'social',
		      'login'        => $username,
		      'pass'         => '',
		      'email'        => $email,
		      'display_name' => $displayName,
		      'role'         => 'recruiter'

		      ));

		    $result = (array) $result;

			if(isset($result['action']) && ( $result['action'] == 'login' || $result['action'] == 'register' )) {

			      session_start();

			      $_SESSION['nsauth'] = (array) $result;


			      if($result['action'] == 'login') {

			       header("location:" . NSAUTH_LOGIN_URL);
			       exit; 

			     } elseif ($result['action'] == 'register') {

			       header("location:" . NSAUTH_REG_URL);
			       exit; 

		     }

			}

		   }
		   else
		   {
		      // if no email, than no fun... Redirecting to root
		     header("location:" . NSAUTH_APP_URL);
		     exit; 
		   }







	} else {

		die('Some problem occurred, please try again or contact website administration for support.');
	}
}
