<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'aisai');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'root');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'root');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A>:j@|0rAf:@U@moE@o+G|MgU|Xyr/ieb@VIL,L7_N]Z{|:Om#=Sf`R>D`BVjU=Q');
define('SECURE_AUTH_KEY',  'LyN<|m7-E*_)<B&g=?> GGL|Ai ^&`-|v-bs[_WY/*{sDxx8jhJSlZ@Z!<oUPiMK');
define('LOGGED_IN_KEY',    '73(-3RF+YwxjRw,sn:D K/.k7ZHC2T]t9SbF|lzWWVax{fgfwSl6UhVvrz/Gvx|h');
define('NONCE_KEY',        'cVybpy#=z1r_UEUN7%7&0j)I+0uU:!5D.T|0}H3%fID$g_nlo-f+0@)1P]-yEqSo');
define('AUTH_SALT',        'S&ihQ|yAgece+w(xqVHs^8/n[  707@w)39i#h;KxbBoy!mX%@@m_M: FiUPjkL;');
define('SECURE_AUTH_SALT', '+-If)nHz5>~I||[,O,@5Fm|,70+y[dR}EA]a|E.NMxxHnSpZ:-^|Zsy9Z]w~c72x');
define('LOGGED_IN_SALT',   'd0eo+>@U[],a?CyQxg-5k aZVpW.%.O~fF~u]:qCS4y!eK-|w)DptH.8T+=g!np)');
define('NONCE_SALT',       'o53.LjjfRDQhJ&JV3,Jb{~T6(}RZHTclSqvJszu$jY;HW]z}:3Ku)9-,-0-ve;4?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ai_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');


/**
 * Set custom paths
 *
 * These are required because wordpress is installed in a subdirectory.
 */
if (!defined('WP_SITEURL')) {
	define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wpcore');
}
if (!defined('WP_HOME')) {
	define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME'] . '');
}
if (!defined('WP_CONTENT_DIR')) {
	define('WP_CONTENT_DIR', dirname(__FILE__) . '/app');
}
if (!defined('WP_CONTENT_URL')) {
	define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/app');
}


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
